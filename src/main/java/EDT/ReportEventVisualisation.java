package EDT;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
//import com.sun.image.codec.jpeg.*;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Visualisation of episode event frequency. Displays information in a circle or
 * a grid
 * 
 * @author Denise de Vries
 * 
 */

public class ReportEventVisualisation extends JFrame implements ActionListener,
				ChangeListener, Constants{
	private JFrame frame;
	private JPanel pMain;
	private JPanel cPanel; // Control Panel
	private ViewPanel pDisplay;
	private JSpinner sMin;
	private JComboBox cmbColour;
	private JPanel pStatus;
	private Font font;
	private int[][] tallies;
	private long[] episodes;
	private String strEvent = null; // get string description of the episodes
	// and display on the panel
	private int cScale = DOW; // Default circle/column scale Day of Week
	private int rScale = DOM; // Default radial/row scale Day of Month
	private boolean cFlag = true; // Flag to recalculate tallies for change of
									// scale
	private boolean rFlag = true; // Flag to recalculate tallies for change of
									// scale
	private TitledBorder tbr;
	private Box sb;

	/**
	 * 
	 * @param e Event identifier
	 * @param eventTitle Descriptive title of event
	 */
	public ReportEventVisualisation(int e, String eventTitle){
		ArrayList<Integer> d = Store.getEvent(e).getRawIndexes();
		episodes = new long [d.size()]; 
		for (int i = 0; i < d.size(); i++) {
			episodes[i] = Store.getRawRecordTimeStamp(d.get(i));
		}
		java.awt.Image ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/edt.gif"));
		setIconImage((new ImageIcon(ic)).getImage());
		createAndShowGUI(eventTitle);
	}

	/**
	 * Creates user interface and display of event data
	 * 
	 * @param title  Descriptive title of event
	 */
	private void createAndShowGUI(String title){
		font = new Font("SansSerif", Font.PLAIN, 11);
		buildTallies();

		pMain = new JPanel(new BorderLayout());
		pMain.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

		cPanel = ctrlPanel();
		pStatus = statusPanel();
		pDisplay = new ViewPanel(24, 1, 7, 2, tallies, strEvent);

		pMain.add(pDisplay, BorderLayout.CENTER);
		pMain.add(cPanel, BorderLayout.WEST);
		pMain.add(pStatus, BorderLayout.SOUTH);
		setContentPane(pMain);

		Store.setVtStatusText("TemporalView");

		setTitle("Event Visualisation for " + title);
		setBounds(50, 50, 900, 800);
		setMinimumSize(new Dimension(900, 800));
		setResizable(true);
		setVisible(true);
	}

	/**
	 * Creates panel to display status of currently selected event
	 * 
	 * @return Status Panel
	 */
	private JPanel statusPanel(){
		JPanel sp = new JPanel(new BorderLayout(3, 3));
		sp.setPreferredSize(new Dimension(20, 30));
		sp.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		JTextField vtStatus = new JTextField();
		vtStatus.setBorder(new CompoundBorder(BorderFactory.createLoweredBevelBorder(), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
		vtStatus.setBackground(Color.white);
		vtStatus.setFont(font);
		vtStatus.setForeground(Color.BLUE);
		vtStatus.setEditable(false);
		sp.add(vtStatus, BorderLayout.CENTER);
		Store.setVtStatusField(vtStatus);
		return sp;
	}

	/**
	 * Creates Control Panel for selection of options to control display of
	 * visualisation Default settings are: View = Circle, Continuous colour,
	 * Scale: Radians= Hours, Circles = Days of Week, Minimum = 1
	 * 
	 * @return Control Panel
	 */
	private JPanel ctrlPanel(){
		JPanel pControl = new JPanel();

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		pControl.setLayout(gridBag);
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(5, 5, 5, 5); // t,l,b,r

		// ****************************************/
		JLabel lView = new JLabel("View");
		lView.setForeground(Color.BLUE);
		lView.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(lView, c);
		pControl.add(lView);

		JPanel pVO = viewOptions();
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		gridBag.setConstraints(pVO, c);
		pControl.add(pVO);

		pControl.add(Store.buildSeparator(c, gridBag));

		// ****************************************/
		JLabel lFile = new JLabel("Colour Scheme");
		lFile.setForeground(Color.BLUE);
		lFile.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lFile, c);
		pControl.add(lFile);

		String[] colOptions = {"Continuous Colour", "Distinct Colour", "Greyscale"};
		cmbColour = new JComboBox(colOptions);
		cmbColour.setFont(font);
		cmbColour.setEditable(false);
		Dimension pref = cmbColour.getPreferredSize();
		cmbColour.setMaximumSize(new Dimension(Integer.MAX_VALUE, pref.height));
		cmbColour.setSelectedIndex(0);
		cmbColour.setActionCommand("Colour");
		cmbColour.addActionListener(this);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(cmbColour, c);
		pControl.add(cmbColour);

		pControl.add(Store.buildSeparator(c, gridBag));

		// ****************************************/
		JLabel lSO = new JLabel("Scale");
		lSO.setForeground(Color.BLUE);
		lSO.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;

		gridBag.setConstraints(lSO, c);
		pControl.add(lSO);

		JPanel pSO = scaleOptions();
		c.anchor = GridBagConstraints.EAST;
		gridBag.setConstraints(pSO, c);
		pControl.add(pSO);

		pControl.add(Store.buildSeparator(c, gridBag));

		// ****************************************/
		JLabel lFO = new JLabel("Filter");
		lFO.setForeground(Color.BLUE);
		lFO.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lFO, c);
		pControl.add(lFO);

		JLabel lMin = new JLabel("Minimum");
		c.gridwidth = 2;
		c.anchor = GridBagConstraints.EAST;
		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(lMin, c);
		pControl.add(lMin);

		sMin = new JSpinner(new SpinnerNumberModel(1, 1, 100, 1));
		sMin.setBorder(BorderFactory.createLineBorder(new Color(120, 150, 190)));
		sMin.setPreferredSize(new Dimension(50, 22));
		sMin.addChangeListener(this);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(sMin, c);
		pControl.add(sMin);

		pControl.add(Store.buildSeparator(c, gridBag));

		// ****************************************/
		JButton jb = new JButton("Print");
		jb.setActionCommand("PRINT");
		jb.addActionListener(this);
		gridBag.setConstraints(jb, c);
		pControl.add(jb);

		JButton jp = new JButton(" Export to JPG ");
		jp.setActionCommand("JPG");
		jp.addActionListener(this);
		gridBag.setConstraints(jp, c);
		pControl.add(jp);

		JPanel pPad = new JPanel();
		c.weighty = 1;
		gridBag.setConstraints(pPad, c);
		pControl.add(pPad);

		return pControl;
	}

	private JPanel viewOptions(){
		JPanel vo = new JPanel();
		vo.setLayout(new BoxLayout(vo, BoxLayout.LINE_AXIS));
		vo.setMaximumSize(new Dimension(200, 100));

		ButtonGroup style = new ButtonGroup();
		JRadioButton rbCircle = new JRadioButton("Circle");
		rbCircle.setFont(font);
		style.add(rbCircle);
		rbCircle.setActionCommand("Circle");
		rbCircle.addActionListener(this);
		rbCircle.setSelected(true);
		JRadioButton rbGrid = new JRadioButton("Grid");
		rbGrid.setFont(font);
		style.add(rbGrid);
		rbGrid.setActionCommand("Grid");
		rbGrid.addActionListener(this);
		rbGrid.setSelected(false);
		vo.add(rbCircle);
		vo.add(rbGrid);
		return vo;
	}

	private JPanel scaleOptions(){
		String[] scale = new String[]{"Hours", "HRS", "Day of Week", "DOW", "Day of Month", "DOM", "Week of Year", "WOY", "Month of Year", "MOY"};
		JPanel so = new JPanel();
		so.setLayout(new BoxLayout(so, BoxLayout.LINE_AXIS));
		so.setMaximumSize(new Dimension(200, 200));
		sb = new Box(BoxLayout.LINE_AXIS);
		formatSB("Rad  Circ");

		Box br = new Box(BoxLayout.PAGE_AXIS);
		ButtonGroup bgRow = new ButtonGroup();
		for(int i = 0; i < 10; i++){
			Box bx = Box.createHorizontalBox();
			bx.setBorder(BorderFactory.createEmptyBorder());
			bx.setAlignmentX(SwingConstants.RIGHT);
			JLabel lbl = new JLabel(scale[i], SwingConstants.RIGHT);
			lbl.setFont(font);
			i++;
			JRadioButton rb = new JRadioButton();
			rb.setActionCommand("R" + scale[i]);
			rb.addActionListener(this);
			if(i == 1)
				rb.setSelected(true);
			bgRow.add(rb);
			bx.add(lbl);
			bx.add(rb);
			br.add(bx);
		}
		Box bc = Box.createVerticalBox();
		bc.setBorder(BorderFactory.createEmptyBorder());
		ButtonGroup bgCol = new ButtonGroup();
		for(int i = 0; i < 10; i++){
			Box bx = Box.createHorizontalBox();
			bx.setBorder(BorderFactory.createEmptyBorder());
			bx.setAlignmentX(SwingConstants.LEFT);
			i++;
			JRadioButton rb = new JRadioButton();
			rb.setActionCommand("C" + scale[i]);
			if(i == 3)
				rb.setSelected(true);
			rb.addActionListener(this);
			bgCol.add(rb);
			bx.add(rb);
			bc.add(bx);
		}
		sb.add(br);
		sb.add(bc);
		so.add(sb);
		return so;
	}

	/**
	 * Sets the label for scale options
	 * 
	 * @param title
	 *            Label for scale options
	 */
	private void formatSB(String title){
		sb.setFont(font);
		tbr = BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), title);
		tbr.setTitleJustification(TitledBorder.RIGHT);
		tbr.setTitleFont(font);
		sb.setBorder(tbr);
	}

	/**
	 * Creates PDF report of current event
	 * 
	 * @see com.lowagie.*
	 */

	private void printToFile(){
		JFileChooser chooser = new JFileChooser(".");
		chooser.setDialogType(JFileChooser.SAVE_DIALOG);
		chooser.setFileFilter(new FileFilterUtility("pdf", "Adobe PDF Files: "));
		int returnVal = chooser.showSaveDialog(this.frame);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			Document document = new Document(PageSize.A4, 20, 20, 50, 50);
			Phrase p = new Phrase("Episode Detection Tool Visualisation Report", new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 16));
			HeaderFooter header = new HeaderFooter(p, false);
			header.setBorder(0);
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
			HeaderFooter footer = new HeaderFooter(new Phrase("Page "), new Phrase(": (" + sdf.format(cal.getTime()) + ")"));
			footer.setAlignment(Element.ALIGN_CENTER);
			footer.setBorder(0);

			try{
				PdfWriter.getInstance(document, new FileOutputStream(chooser.getSelectedFile()));
				document.setHeader(header);
				document.setFooter(footer);
				document.open();
				com.lowagie.text.Font f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 12, com.lowagie.text.Font.BOLD);
				Paragraph par = new Paragraph(this.getTitle(), f);
				document.add(par);

				par = new Paragraph("Axis: " + getStatusString(rScale) + " VS " + getStatusString(cScale), f);
				par.setSpacingAfter(80);
				document.add(par);

				Dimension size = pDisplay.getSize();
				BufferedImage myImage = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
				Graphics2D g2 = myImage.createGraphics();
				pDisplay.paint(g2);

				com.lowagie.text.Image img = com.lowagie.text.Image.getInstance(myImage, null);
				img.setAlignment(com.lowagie.text.Image.ALIGN_MIDDLE);
				img.setBorder(com.lowagie.text.Image.BOX);
				img.setBorderWidth(3);
				img.scaleToFit(400, 400);
				document.add(img);

			}catch(DocumentException de){
				System.err.println(de.getMessage());
			}catch(IOException e){
				System.out.println("Visualisation: File currently in use, please close first");
			}
			document.close();
		}
	}

	private void SaveToJPG(){
		File gfxFile;
		JFileChooser chooser = new JFileChooser(".");
		chooser.setDialogType(JFileChooser.SAVE_DIALOG);
		FileFilterUtility filter = new FileFilterUtility();
		filter.addExtension("jpg");
		filter.setDescription("JPEG Files");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showSaveDialog(this.frame);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			gfxFile = chooser.getSelectedFile();
			saveComponentAsJPEG(pDisplay, gfxFile);
		}
	}

	private void saveComponentAsJPEG(Component gfxComponent, File outFile){
		Dimension size = gfxComponent.getSize();
		BufferedImage myImage = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = myImage.createGraphics();
		gfxComponent.paint(g2);
		try{
			OutputStream out = new FileOutputStream(outFile);
			ImageIO.write(myImage, "jpg", out);
//			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//			encoder.encode(myImage);
			out.close();
		}catch(Exception e){
			System.out.println(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae){
		if(ae.getActionCommand().equals("Grid")){
			formatSB("Row  Col");
			pDisplay.setStyle(GRID);
		}
		if(ae.getActionCommand().equals("Circle")){
			formatSB("Rad  Circ");
			pDisplay.setStyle(CIRCLE);
		}
		if(ae.getActionCommand().equals("PRINT")){
			printToFile();
		}
		if(ae.getActionCommand().equals("JPG")){
			SaveToJPG();
		}
		if(ae.getActionCommand().equals("Colour")){
			pDisplay.setColScheme(cmbColour.getSelectedIndex());
		}

		if(ae.getActionCommand().equals("RHRS")){
			rScale = HRS;
			rFlag = true;
		}
		if(ae.getActionCommand().equals("RDOW")){
			rScale = DOW;
			rFlag = true;
		}
		if(ae.getActionCommand().equals("RDOM")){
			rScale = DOM;
			rFlag = true;
		}
		if(ae.getActionCommand().equals("RWOY")){
			rScale = WOY;
			rFlag = true;
		}
		if(ae.getActionCommand().equals("RMOY")){
			rScale = MOY;
			rFlag = true;
		}
		if(ae.getActionCommand().equals("CHRS")){
			cScale = HRS;
			cFlag = true;
		}
		if(ae.getActionCommand().equals("CDOW")){
			cScale = DOW;
			cFlag = true;
		}
		if(ae.getActionCommand().equals("CDOM")){
			cScale = DOM;
			cFlag = true;
		}
		if(ae.getActionCommand().equals("CWOY")){
			cScale = WOY;
			cFlag = true;
		}
		if(ae.getActionCommand().equals("CMOY")){
			cScale = MOY;
			cFlag = true;
		}

		if(rFlag || cFlag){
			buildTallies();
			pDisplay.setTallies(tallies);
			if(rFlag){
				pDisplay.setSpokes(getScale(rScale), rScale);
				rFlag = false;
			}
			if(cFlag){
				pDisplay.setAnnuli(getScale(cScale), cScale);
				cFlag = false;
			}
		}
	}

	private String getStatusString(int b){
		switch (b){
		case HRS:
			return "Hours";
		case DOW:
			return "Day of Week";
		case DOM:
			return "Day of Month";

		case WOY:
			return "Week of Year";

		case MOY:
			return "Month";
		}
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	public void stateChanged(ChangeEvent ce){
		if(ce.getSource().equals(sMin)){
			pDisplay.setFilterMin(Integer.parseInt(sMin.getValue().toString()));
		}
	}

	private void buildTallies(){
		// check the options and count for rScale and cScale
		int r, c;
		r = getScale(rScale);
		c = getScale(cScale);
		int[][] counts = new int[r + 1][c + 1];
		GregorianCalendar cal = new GregorianCalendar();
		for(int i = 0; i < episodes.length; i++){
			cal.setTimeInMillis(episodes[i]);
			switch (rScale){
			case HRS:
				r = cal.get(Calendar.HOUR_OF_DAY);
				break;
			case DOW:
				r = cal.get(Calendar.DAY_OF_WEEK);
				break;
			case DOM:
				r = cal.get(Calendar.DAY_OF_MONTH);
				break;
			case WOY:
				r = cal.get(Calendar.WEEK_OF_YEAR);
				break;
			case MOY:
				r = cal.get(Calendar.MONTH) + 1;
				break;
			default:
				r = 0;
			}
			switch (cScale){
			case HRS:
				c = cal.get(Calendar.HOUR_OF_DAY);
				break;
			case DOW:
				c = cal.get(Calendar.DAY_OF_WEEK);
				break;
			case DOM:
				c = cal.get(Calendar.DAY_OF_MONTH);
				break;
			case WOY:
				c = cal.get(Calendar.WEEK_OF_YEAR);
				break;
			case MOY:
				c = cal.get(Calendar.MONTH) + 1;
				break;
			default:
				c = 0;
			}
			counts[r][c]++;
		}
		tallies = counts;
	}

	private int getScale(int scale){
		int r;
		switch (scale){
		case HRS:
			r = 24;
			break;
		case DOW:
			r = 7;
			break;
		case DOM:
			r = 31;
			break;
		case WOY:
			r = 53;
			break;
		case MOY:
			r = 12;
			break;
		default:
			r = 0;
		}
		return r;
	}

	/** ********************************* */

	/**
	 * Visualisation of event data
	 * 
	 * @author Denise de Vries
	 * @calledBy createAndShowGui 
	 * 
	 */
	class ViewPanel extends JPanel implements Constants{
		int radius = 20;
		int cy = 600; // offset to centre of circle
		int cx = 600; // offset to centre of circle
		float gx = 100; // offset to left of grid
		float gy = 100; // offset to top of grid
		int spokes; // number of rows/radials
		int annuli; // number of columns/circles
		float band; // width of each annulus
		int style; // 0=circle, 1 = grid
		int max, min;
		int filterMin = 1;
		int colScheme = 0;
		int rLabel;
		int cLabel;
		Font font;
		Point radial;
		String strEvent = null;
		ArrayList<Object> graphiclist;
		private Color[] colors;
		final Color VIOLET = new Color(128, 0, 128);
		final Color INDIGO = new Color(75, 0, 130);
		private Color rainbow[] = {Color.WHITE, VIOLET, INDIGO, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED};
		public int[][] tallies;

		/**
		 * 
		 * @param radials
		 *            number of row/radials
		 * @param rLabel
		 *            index to row/radial label text
		 * @param annuli
		 *            number of columns/circles
		 * @param cLabel
		 *            index to column/circle label text
		 */
		public ViewPanel(int radials, int rLabel, int annuli, int cLabel){
			this.spokes = radials;
			this.annuli = annuli;
			this.rLabel = rLabel;
			this.cLabel = cLabel;
			this.style = 0;
		}

		/**
		 * @param radials
		 *            number of row/radials
		 * @param rLabel
		 *            index to row/radial label text
		 * @param annuli
		 *            number of columns/circles
		 * @param cLabel
		 *            index to column/circle label text
		 * @param tallies
		 *            array of event frequencies
		 * @param strEvent
		 *            description of event
		 */
		public ViewPanel(int radials, int rLabel, int annuli, int cLabel,
						int[][] tallies, String strEvent){
			this.spokes = radials;
			this.annuli = annuli;
			this.rLabel = rLabel;
			this.cLabel = cLabel;
			this.style = 0;
			this.tallies = tallies;
			this.strEvent = strEvent;
			font = new Font("SansSerif", Font.PLAIN, 11);
			setBorder(new CompoundBorder(new CompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()), BorderFactory.createEmptyBorder(5, 5, 5, 5)));

			initColArray();
			addMouseListener(new vpMouseListener());
		}

		private Point txtLocation(int increment, int scale){
			Point result = new Point();
			int bearing = (increment * 360 / spokes);
			double angle = 2 * Math.PI * ((90 + 360 / (spokes * 2)) - bearing) / 360;
			int extra = (cy - radius) / 15;
			result.x = cx - radius / 2 + (int)(Math.cos(angle) * (scale + extra));
			result.y = cy - radius / 2 - (int)(Math.sin(angle) * (scale + extra));
			return result;
		}

		private void initColArray(){
			colors = new Color[256];
			switch (colScheme){
			case 0: // Yellow-Red
				for(int i = 0; i <= 255; i++){
					colors[255 - i] = new Color(255, i, 0);

				}
				break;
			case 1: // Distinct
				for(int i = 0; i <= 255; i++){
					int rx = i / 32 + 1;
					colors[i] = rainbow[rx];
				}
				break;
			case 2: // greyscale
				for(int i = 0; i <= 255; i++){
					colors[i] = new Color(255 - i, 255 - i, 255 - i);
				}
			}
		}

		private void initMaxTally(){
			max = 0;
			for(int r = 1; r < spokes; r++){
				for(int c = 1; c < annuli; c++){
					if(tallies[r][c] > max)
						max = tallies[r][c];
				}
			}
		}

		private void initMinTally(){
			min = Integer.MAX_VALUE;
			for(int r = 1; r < spokes; r++){
				for(int c = 1; c < annuli; c++){
					if(tallies[r][c] < min)
						min = tallies[r][c];
				}
			}
		}

		public void paintComponent(Graphics g){
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D)g;
			graphiclist = new ArrayList<Object>();
			initMaxTally();
			initMinTally();
			int gaugeR = 0;
			int gaugeL = 0;
			switch (style){
			case CIRCLE:
				cy = getHeight() / 2;
				cx = getWidth() / 2;
				band = (cx < cy ? ((getWidth() - radius) / annuli) : ((getHeight() - radius) / annuli));
				band = band * 2 / 3;
				int txt = (int)(radius / 2 + band * annuli / 2);
				gaugeR = (int)(cx + radius / 2 + band * annuli / 2);
				gaugeL = (int)(cx - (radius / 2 + band * annuli / 2));
				paintWedge(g);
				g.setColor(Color.black);
				for(int i = 1; i <= annuli; i++){
					g.drawLine(10, (int)(cy - radius - (band * i) / 2), 20, (int)(cy - radius - (band * i) / 2));
					g.drawLine(10, cy - radius, 10, (int)(cy - radius - (band * annuli) / 2));
					switch (cLabel){
					case DOW:
						g.drawString(days[i], 20, (int)(cy - radius - (band * (i - 1)) / 2));
						break;
					case MOY:
						g.drawString(months[i], 20, (int)(cy - radius - (band * (i - 1)) / 2));
						break;
					default:
						g.drawString("" + (annuli - annuli + i), 20, (int)(cy - radius - (band * (i - 1)) / 2));
					}
				}

				g.setColor(Color.black);
				g.drawLine(10, cy - radius, 20, cy - radius);
				for(int i = 1; i <= spokes; i++){
					radial = txtLocation(i, txt);
					switch (rLabel){
					case DOW:
						g.drawString(days[i], radial.x, radial.y);
						break;
					case MOY:
						g.drawString(months[i], radial.x, radial.y);
						break;
					default:
						g.drawString("" + i, radial.x, radial.y);
					}
				}
				break;
			case GRID:
				float gw = getWidth() * 3 / 4;
				float gh = getHeight() * 3 / 4;
				gx = (getWidth() - gw) / 2;
				gy = (getHeight() - gh) / 2;
				gaugeR = (int)gw;
				gaugeL = (int)gx;
				gw = gw / (annuli + 1);
				gh = gh / (spokes + 1);

				for(int r = 1; r < spokes + 1; r++){
					for(int c = 1; c < annuli + 1; c++){
						Rectangle2D rect = new Rectangle2D.Float(gx + ((c - 1) * gw), gy + ((r - 1) * gh), gw, gh);
						Oblong ob = new Oblong(rect, r, c);
						if(tallies[r][c] >= filterMin){
							int co = (int)((colors.length - 1) * ((double)tallies[r][c] / (double)max));
							g2.setColor(colors[co]);
						}else{
							g2.setColor(Color.WHITE);
						}
						g2.fill(ob.getOblong());
						g2.setColor(Color.BLACK);
						g2.draw(ob.getOblong());
						graphiclist.add(ob);
					}
				}

				for(int i = 1; i <= annuli; i++){
					int sx = (int)gx + (int)(gw / 2) + (int)((i - 1) * gw);
					font = new Font("SansSerif", Font.BOLD, 11);
					g2.setFont(font);
					switch (cLabel){
					case DOW:
						g.drawString(days[i], sx, (int)gy - 5);
						break;
					case MOY:
						g.drawString(months[i], sx, (int)gy - 5);
						break;
					case WOY:
						font = new Font("SansSerif", Font.BOLD, 10);
						AffineTransform at = font.getTransform();
						at.rotate(Math.toRadians(270));
						font = font.deriveFont(at);

						g2.setFont(font);
						sx = (int)gx + (int)((i) * gw);
						g2.drawString("" + i, sx, (int)gy - 6);
						break;
					default:
						g.drawString("" + i, sx, (int)gy - 5);
					}
				}

				font = new Font("SansSerif", Font.BOLD, 11);
				g.setFont(font);
				for(int i = 1; i <= spokes; i++){
					int sy = (int)gy + (int)(i * gh);
					int sx = (int)gx;
					switch (rLabel){
					case DOW:
						g.drawString(days[i], sx - 25, (int)(sy - gh / 2));
						break;
					case MOY:
						g.drawString(months[i], sx - 25, (int)(sy - gh / 2));
						break;
					case WOY:
						font = new Font("SansSerif", Font.BOLD, 10);
						g.setFont(font);
						g.drawString("" + i, sx - 15, sy - 5);
						break;
					default:
						g.drawString("" + i, sx - 15, sy - 5);
					}
				}
				break;
			}
			drawGauge(g, gaugeL, gaugeR);
		}

		private void drawGauge(Graphics g, int lx, int rx){
			int uy = 70;
			double cl = 0;
			int step = (rx - lx) / (max + 1);
			font = new Font("SansSerif", Font.BOLD, 11);
			g.setFont(font);
			for(int x = lx; x <= rx && cl <= max; x += step){
				int col = (int)((colors.length - 1) * (cl / max));
				if(col >= 1){
					g.setColor(colors[col]);
				}else{
					g.setColor(Color.WHITE);
				}
				for(int i = x; i < (x + step) && i < rx; i++){
					g.drawLine(i, uy - 44, i, uy - 38);
				}
				cl++;
			}
			g.setColor(Color.BLACK);
			g.drawString("0", lx, uy - 25);
			g.drawString("" + max, rx, uy - 25);
		}

		public void paintWedge(Graphics g){
			Graphics2D g2 = (Graphics2D)g;
			float a = (float)360 / spokes;
			for(int c = annuli; c > 0; c--){
				for(int r = spokes; r > 0; r--){
					float bearing = (r * (float)360 / spokes);
					Arc2D arc0 = new Arc2D.Float((cx - radius - (band * c) / 2), cy - radius - (band * c) / 2, radius + (band * c), radius + (band * c), 90 - bearing, a, Arc2D.OPEN);
					Arc2D arc1 = new Arc2D.Float(cx - radius - (band * (c - 1)) / 2, cy - radius - (band * (c - 1)) / 2, radius + (band * (c - 1)), radius + (band * (c - 1)), 90 - bearing, a, Arc2D.OPEN);
					Wedge w = new Wedge(arc0, arc1, r, c);

					if(tallies[r][c] >= filterMin){
						int co = (int)((colors.length - 1) * ((double)tallies[r][c] / (double)max));
						g2.setColor(colors[co]);
					}else{
						g2.setColor(Color.WHITE);
					}
					g2.fill(w.getWedge());
					g2.setColor(Color.BLACK);
					g2.draw(w.getWedge());
					graphiclist.add(w);
				}
			}
		}

		public void setAnnuli(int annuli, int cLabel){
			this.annuli = annuli;
			this.cLabel = cLabel;
			repaint();
		}

		public void setSpokes(int spokes, int rLabel){
			this.spokes = spokes;
			this.rLabel = rLabel;
			repaint();
		}

		public void setStyle(int style){
			this.style = style;
			repaint();
		}

		public void setTallies(int[][] tallies){
			this.tallies = tallies;
		}

		public void setColScheme(int colScheme){
			this.colScheme = colScheme;
			initColArray();
			repaint();
		}

		public void setFilterMin(int m){
			this.filterMin = m;
			repaint();
		}

		private void updateStatus(int r, int c){
			String strStatus = "";
			switch (rLabel){
			case HRS:
				strStatus = strStatus + "Hour = " + r;
				break;
			case DOW:
				strStatus = strStatus + "Day = " + statusDays[r];
				break;
			case DOM:
				strStatus = strStatus + "Day of month = " + r;
				break;
			case WOY:
				strStatus = strStatus + "Week of year = " + r;
				break;
			case MOY:
				strStatus = strStatus + "Month  = " + statusMonths[r];
				break;
			}
			strStatus = strStatus + ", ";
			switch (cLabel){
			case HRS:
				strStatus = strStatus + "Hour = " + c;
				break;
			case DOW:
				strStatus = strStatus + "Day = " + statusDays[c];
				break;
			case DOM:
				strStatus = strStatus + "Day of month = " + c;
				break;
			case WOY:
				strStatus = strStatus + "Week of year = " + c;
				break;
			case MOY:
				strStatus = strStatus + "Month  = " + statusMonths[c];
				break;
			}
			strStatus = strStatus + " : Count = " + tallies[r][c];
			Store.setVtStatusText(strStatus);
		}

		/** ************************************ */
		/**
		 * @author Denise de Vries
		 * 
		 */
		class vpMouseListener extends MouseAdapter{

			public vpMouseListener(){
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
			 */
			public void mouseClicked(MouseEvent ev){
				double xClick, yClick;
				boolean found;
				int row = 0;
				int col = 0;
				GeneralPath p;

				if(ev.getClickCount() == 1){
					xClick = ev.getX();
					yClick = ev.getY();
					found = false;
					for(int i = 0; i < graphiclist.size() && !found; i++){
						switch (style){
						case CIRCLE:
							Wedge w = (Wedge)graphiclist.get(i);
							p = w.getWedge();
							if(p.contains(xClick, yClick)){
								row = w.getR();
								col = w.getC();
								found = true;
							}
							break;
						case GRID:
							Oblong ob = (Oblong)graphiclist.get(i);
							p = ob.getOblong();
							if(p.contains(xClick, yClick)){
								row = ob.getR();
								col = ob.getC();
								found = true;
							}
							break;
						}
					}
					if(found)
						updateStatus(row, col);
				}
			}
		}

		/** ************************************ */
		/**
		 * @author Denise de Vries
		 * 
		 */
		class Oblong{
			GeneralPath gp;

			int r;

			int c;

			public Oblong(Rectangle2D rect, int r, int c){
				this.r = r;
				this.c = c;
				gp = new GeneralPath(rect);
			}

			public GeneralPath getOblong(){
				return gp;
			}

			public int getR(){
				return r;
			}

			public int getC(){
				return c;
			}
		}

		/** ************************************ */
		/**
		 * Draws wedge shape within concentric circles and radials
		 * 
		 * @author Denise de Vries
		 * 
		 */
		class Wedge{
			GeneralPath gp;

			int r;

			int c;

			/**
			 * @param arc0
			 *            inner arc of wedge
			 * @param arc1
			 *            outer arc of wedge
			 * @param r
			 *            radial
			 * @param c
			 *            circle
			 */
			public Wedge(Arc2D arc0, Arc2D arc1, int r, int c){
				this.r = r;
				this.c = c;
				Line2D l1 = new Line2D.Double(arc0.getStartPoint().getX(), arc0.getStartPoint().getY(), arc1.getStartPoint().getX(), arc1.getStartPoint().getY());
				Line2D l2 = new Line2D.Double(arc1.getEndPoint().getX(), arc1.getEndPoint().getY(), arc0.getEndPoint().getX(), arc0.getEndPoint().getY());

				gp = new GeneralPath(l1);
				gp.append(arc1, true);
				gp.append(l2, true);
				gp.append(arc0, false);
			}

			/**
			 * 
			 * @return general path of the currently selected wedge shape
			 */
			public GeneralPath getWedge(){
				return gp;
			}

			/**
			 * @return the radial of the currently selected wedge shape
			 */
			public int getR(){
				return r;
			}

			/**
			 * 
			 * @return the circular band of the currently selected wedge shape
			 */
			public int getC(){
				return c;
			}
		}
	}

}