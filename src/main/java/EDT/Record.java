package EDT;
/**
 * Facilitates result presentation by maintaining a mapping between timestamps and event identifiers.
 * Stored in a timestamp ordered array.
 * 
 * Two sets of events can be associated with a timestamp, regular events and 
 * ordered events (ordered actors). 
 * 
 * @author: Aaron Ceglar 
 */

public class Record {
	protected long timeStamp = 0;
	protected int event = 0;
	protected int orderedEvent = 0;

	/**
	 * @param t timestamp
	 * @param e event identifier
	 * @calledBy InitialiseEngine
	 */
	public Record(long t, int e){
		timeStamp = t;
		event = e;
	}

	/**
	 * @param e the alternative ordered event to associate with this timestamp
	 *  @calledBy Store
	 */
	public void setOrderedEvent(int e){
		orderedEvent =e;
	}

}
