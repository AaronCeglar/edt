package EDT;

import java.sql.*;
import java.util.*;

/**
 * Database connection using odbc driver for Microsoft Access. Connection
 * remains open until method dbClose() is called.
 * 
 * @author Denise de Vries
 */

public class DatabaseConnection{
	private Connection conn;
	private Statement stmt;
	private ResultSet results;
	private ResultSetMetaData rsmd;
	private ArrayList<String> alTables = null;

	/**
	 * Establish connection to MS Access database
	 * 
	 * @param dbName : the absolute path to the database
	 * @calledBy ValidateDataBaseInput
	 */

	public DatabaseConnection(String dbName){
		try{
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver").newInstance();
			conn = DriverManager.getConnection("jdbc:odbc:;DRIVER=Microsoft Access Driver (*.mdb);" + "DBQ=" + dbName + ";");
		}catch(SQLException se){
			System.out.println("Database connection SQL error " + se);
		}catch(Exception ex){
			System.out.println("Database connection error " + ex);
		}
		alTables = new ArrayList<String>();
	}

	/**
	 * Close database connection
	 * 
	 * @calledBy ValidateDataBaseInput
	 */
	public void dbClose(){
		try{
			conn.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Executes a SELECT query and returns the result set as an ArrayList of
	 * strings. Returns NULL on an SQL exception.
	 * 
	 * @param sql  SQL query string
	 * @calledBy ValidateDataBase Input
	 * @return ArrayList or Null
	 */
	public ArrayList<String> alQuery(String sql){
		ArrayList<String> al = new ArrayList<String>();
		StringBuffer sb = null;
		try{
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			results = stmt.executeQuery(sql);
			rsmd = results.getMetaData();
			int numOfCols = rsmd.getColumnCount();

			while(results.next()){
				sb = new StringBuffer();
				for(int i = 1; i <= numOfCols; i++){
					if(i != 1)	sb.append('\t');
					sb.append(results.getString(i));
				}
				al.add(sb.toString());
			}
			return al;
		}

		catch(SQLException e){
			System.err.println("SQL exception");
			System.err.println("SQLState " + e.getSQLState());
			System.err.println("Message " + e.getMessage());
			System.err.println("Code " + e.getErrorCode());
			return null;
		}
	}

	/**
	 * Retrieves database catalogue
	 * 
	 * @calledBy ValidateDataBaseInput
	 * 
	 * @return ArrayList of table names
	 */
	public ArrayList<String> getCatalogue(){
		try{
			DatabaseMetaData dbmd = conn.getMetaData();
			ResultSet trs = dbmd.getTables(null, null, null, new String[]{"TABLE"});

			while(trs.next()){
				String tableName = trs.getString("TABLE_NAME");
				alTables.add(tableName);
			}
		}catch(SQLException ex){
			System.out.println("getSchema error " + ex);
		}
		return alTables;
	}

	/**
	 * Retrieves table schema - column name and data type
	 * 
	 * @param tableName
	 *            Name of table
	 * @calledBy ValidateDataBaseInput
	 * @return Table schema
	 * @throws SQLException
	 */
	public String[][] getSchema(String tableName) throws SQLException{
		PreparedStatement ps = conn.prepareStatement("Select * from " + tableName + " where 1 = 0");
		ResultSetMetaData psmd = ps.getMetaData();
		int numOfCols = psmd.getColumnCount();
		DatabaseMetaData dbmd = conn.getMetaData();
		results = dbmd.getColumns(null, null, tableName, "%");
		int i = 0;
		String[][] colNames = new String[numOfCols][2];
		while(results.next()){
			colNames[i][0] = results.getString("COLUMN_NAME");
			colNames[i][1] = results.getString("TYPE_NAME");
			i++;
		}
		return colNames;
	}

}
