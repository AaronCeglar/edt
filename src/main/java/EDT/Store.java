package EDT;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import javax.swing.JSeparator;
import javax.swing.JTextField;

/**
 * Repository for those structures that remain constant between different analysis runs of the same file.
 * 
 * @author: Aaron Ceglar 
 */
public final class Store{
	
	private static JTextField tfStatus = null; // Status field in EDT interface.
	private static JTextField vtStatus = null; // Status bar in ReportEventVisualisation interface.	
	private static EpisodeDetection edt = null; // Main application object. 

	private static int timeStampColumn = 1; // Date column in data source.

	private static SimpleDateFormat currentDateFormat = new SimpleDateFormat(Constants.dateArrayRegex[0]); // Current date format.
	public static ArrayList<SimpleDateFormat> dateFormats = null; // Available date formats.

	private static ArrayList<String> dbData = null; // Populated if data source is a database.

	private static File fInput = new File("transactions.txt"); // File name of latest data source (can be database name).

	private static ArrayList<String> inputList = new ArrayList<String>(); // List of currently used data sources.

	private static DecimalFormat doubleFormat = new DecimalFormat("#0"); // Format for objects of type double.

	private static boolean orderedEventsBuilt = false; // Have ordered events (actors ordered within events been built).

	private static int inputType = 0; // Type of data source
	private static int eventCounter = 0; // Number of events.

//-----------Data structures------------

	private static String[] actorList = null; // List of actor names (id = array element).

	private static Event[] eventList = null; // List of events (id = array ewlement).
	private static HashMap<Event, Integer> eventMap = null; // event mapping (key = set of participating actors).

	private static Record[] recordList = null; // Array of raw records.
	private static long windowSpan = 0; // Span of analysis window. 

//--------------EDT Status Bar-------------	
	/**
	 * @param tf : Status bar widget.
	 * @calledBy EpisodeDetection.
	 */
	public static void setStatusField(JTextField tf){
		tfStatus = tf;
	}

	/**
	 * @param s : String to display.
	 * @calledBy AnalysisEngine AnalysisResults EpisodeDetection
	 */
	public static void setStatusText(String s){
		tfStatus.setText(s);
	}

//----------ReportEventVisualisation Status Bar----------

	/**
	 * @param tf :  Status bar widget.
	 * @calledBy ReportEventVisualisation
	 */
	public static void setVtStatusField(JTextField tf){
		vtStatus = tf;
	}

	/**
	 * @param s : String to display.
	 * @calledBy ReportEventVisualisation
	 */
	public static void setVtStatusText(String s){
		vtStatus.setText(s);
	}

//---------------Frame access------------	

	/**
	 * @param f : the EDT frame.
	 * @calledBy EpisodeDetection
	 */
	public static void setFrame(EpisodeDetection f){
		edt = f;
	}

	/**
	 * @return reference to the EDT frame.
	 * @calledBy AnalysisResults InitialiseEngine InputSourceDialog ReportTextDetail
	 */
	public static EpisodeDetection getFrame(){
		return edt;
	}

//---------Data source access---------	

	/**
	 * @return name of data source
	 * @calledBy EpisodeDetection InitialiseEngine
	 */
	public static File getFInput(){
		return fInput;
	}

	/**
	 * @param f : Data source name.
	 * @calledBy InputSourceDialog
	 */
	public static void setFInput(File f){
		fInput = f;
	}

	/**
	 * @return source data derived from given database.
	 * @calledBy InitialiseEngine
	 */
	public static ArrayList<String> getDbData(){
		return dbData;
	}

	/**
	 * @param d : The data set.
	 * @calledBy InitialiseDatabaseInput
	 */
	public static void setDbData(ArrayList<String> d){
		Store.dbData = d;
	}

	/**
	 * @return the first database record.
	 * @calledBy  ValidateInput
	 */
	public static String getFirstDbData(){
		return dbData.get(0);
	}

	/**
	 * @return The type of data source (File or Database).
	 * @calledBy InitialiseEngine ValidateInput
	 */
	public static int getInputType(){
		return inputType;
	}

	/**
	 * @param i : The type of data source specified.
	 * @calledBY InputDataSource
	 */
	public static void setInputType(int i){
		inputType = i;
	}

	/**
	 * @return The list of data sources opened for analysis.
	 * @calledBy AnalysisResults InitialiseEngine
	 */
	public static ArrayList<String> getInputList(){
		return inputList;
	}

	/**
	 * Clear the list of open data sources.
	 * @calledBy EpisodeDetection
	 */
	public static void clearInputList(){
		inputList.clear();
	}

	/**
	 * Append  file/ database name to list of opened datasources. 
	 * 
	 * @param s : File name
	 * @calledBy EpisodeDetection
	 */
	public static void addInputTitle(String n){
		inputList.add(n);
	}

	/**
	 * Empty data source list and then append latest data source.
	 * @calledBy InitialiseEngine
	 */
	public static void initialiseInputList(){
		inputList.clear();
		inputList.add(fInput.getName());
	}

//---------Dougble type format----------	
	/**
	 * @return double format
	 * @calledBy AnalysisResults 
	 */
	public static DecimalFormat getDoubleFormat(){
		return doubleFormat;
	}

//----------TimeStamp access------------
	
	/**
	 * @return the data source time stamp column.
	 * @calledBy InitialiseEngine
	 */
	public static int getTimeStampColumn(){
		return timeStampColumn;
	}

	/**
	 * @param tsc : time stamp column.
	 * @calledBy InitialiseDatabaseInput ValidateInput 
	 */
	public static void setTimeStampColumn(int tsc){
		timeStampColumn = tsc;
	}

//---------- event counter-------------- 
	
	/**
	 * @param i : current count.
	 * @calledBy InitialiseEngine
	 */
	public static void setEventCounter(int i){
		eventCounter = i;
	}

//--------------Date Format access---------
	
	/**
	 * @return the currently selected date format.
	 * @calledBy InitialiseEngine ReportTextDetail
	 */
	public static SimpleDateFormat getCurrentDateFormat(){
		return currentDateFormat;
	}

	/**
	 * @param e : the selected date format
	 * @calledBy DateFormatDialog EpisodeDetection ValidateInput
	 */
	public static void setCurrentDateFormat(SimpleDateFormat e){
		currentDateFormat = e;
	}

	/**
	 * @return the set of available date formats.
	 * @calledBy DateFormatDialog EpisodeDetection ValidateInput
	 */
	public static ArrayList<SimpleDateFormat> getDateFormats(){
		return dateFormats;
	}

	/**
	 * @param d : the set of date formats. 
	 * @calledBy DateFormatDialog EpisodeDetection
	 */
	public static void setDateFormats(ArrayList<SimpleDateFormat> d){
		dateFormats = d;
	}

//-------------Actor Storage-------------
	
	/**
	 * @return the set of actor symbols.
	 * @calledBy AnalysisResults InitialiseEngine ReportTextDetail
	 */
	public static String[] getArtifactSymbols(){
		return actorList;
	}

	/**
	 * @param as : the list of actor names. 
	 * @calledBy InitialiseEngine
	 */
	public static void setArtifactList(String[] a){
		actorList = a;
	}

	/**
	 * @param i : the requested actors identifier.
	 * @return the name associated with that actor id.
	 * @calledBy AnalysisResults Event InitialiseEngine
	 */
	public static String getArtifactSymbol(int i){
		return actorList[i];
	}

//-------------Raw record access----------
	
	/**
	 * @param r : the list of raw records.
	 * @calledBy InitialiseEngine
	 */
	public static void setRawRecords(Record[] r){
		recordList = r;
	}

	/**
	 * @return the number of raw records.
	 * @calledBy AnalysisEngine InitialseEngine ReportTextDetail
	 */
	public static int getRawLength(){
		return recordList.length;
	}

	/**
	 * @param i
	 * @return the event pertaining to a particular raw record.
	 * @calledBy InitialiseEngine ReportTextDetail
	 */
	public static int getRawRecordEvent(int i){
		return recordList[i].event;
	}

	/**
	 * @param i : the raw record index.
	 * @return The timestamp associated with a raw record.
	 * @calledBy AnalysisEngine InitialiseEngine ReportEventVisualisation ReportTextDetail
	 */
	public static Long getRawRecordTimeStamp(int i){
		return recordList[i].timeStamp;
	}

	/**
	 * @param b : return ordered/ or unordered event. 
	 * @param i : the index of the raw event to access.
	 * @return the event pertaining to the raw record.
	 * @calledBy AnalysisEngine
	 */
	public static int getRawRecordEvent(boolean b, int i){
		if(!b) return recordList[i].event;
		return recordList[i].orderedEvent;
	}

//--------------Window measure access--------------
	
	/**
	 * @return the number of analysis windows.
	 * @calledBy AbstractTreeNode
	 */
	public static long getWindowCount(){
		return recordList.length - windowSpan;
	}

	/**
	 * @param w : the analysis window span.
	 * @calledBy AnalysisEngine
	 */
	public static void setWindowSpan(long w){
		windowSpan = w;
	}

//-----------Event data access------------
	
	/**
	 * @param e :the event list.
	 * @calledBy InitialiseEngine
	 */
	public static void setEventList(Event[] e){
		eventList = e;
	}

	/**
	 * @param i : the index of the requested event.
	 * @return the event 
	 * @calledBy AbstractTreeNode AnalysisResults ReportEventVisualisation ReportTextDetail
	 */
	public static Event getEvent(int i){
		return eventList[i];
	}

	/**
	 * @param i : the event index.
	 * @return the actors associated with an event.
	 * @calledBy AnalysisEngine AnalysisResults InitialiseEngine ParallelTreeNode ReportTextDetail
	 */
	public static Integer[] getEventArtifacts(int i){
		return eventList[i].getActorArray();
	}


	/**
	 * @param e : the event map.
	 * @calledBy InitialiseEngine
	 */
	public static void setEventMap(HashMap<Event, Integer> e){
		eventMap = e;
	}

	/**
	 * @param i : the event index.
	 * @return : the time stamps associated with this event.
	 * @calledBy ReportTextDetail
	 */
	public static ArrayList<Integer> getEventTimeStamps(Integer i){
		return eventList[i].getRawIndexes();
	}

	/**
	 * @param ordered : get the event from the ordered or unordered set 
	 * @param i : event index
	 * @return the required event.
	 * @calledBy AnalysisEngine
	 */
	public static int getEvent(boolean ordered, int i){
		if(ordered)	return recordList[i].orderedEvent;
		return recordList[i].event;
	}

	/**
	 * If the ordered event set has not been built the do so.
	 * A new set of event identifiers associated with raw timestamps in which the actors have been sorted into a consistent ordering.
	 *  A,B is now the same as B,A
	 * 
	 * @calledBy AnalysisEngine
	 */
	public static void buildOrderedEvents(){
		if(orderedEventsBuilt) return;
		orderedEventsBuilt = true;
		
		for(int i = 0; i < recordList.length; i++){
			// Creates a new event of sorted actors.
			Event event = new Event(eventList[recordList[i].event].getActors(), eventCounter, i);

			// Test if its a new unique event.
			Integer currentEventId = eventMap.get(event);
			if(currentEventId == null){
				eventMap.put(event, eventCounter);
				currentEventId = eventCounter++;
			}
			recordList[i].setOrderedEvent(currentEventId);
		}

		// Populate eventlist and sort.
		eventList = eventMap.keySet().toArray(new Event[0]);
		Arrays.sort(eventList, new Comparator<Event>(){
			public int compare(Event a, Event b){
				return a.getId() - b.getId();
			}
		});
	}

	/**
	 *  A global method to facilitate construction of a separator.
	 *  Used in construction of various control panels. 
	 * 
	 * @param c : The current gridbag constraints.
	 * @param g : The grid bag layout object.
	 * @return
	 * @calledBy AnalysisResults InitialiseEngine ReportEventVisualisation
	 */
	public static JSeparator buildSeparator(GridBagConstraints c, GridBagLayout g){
		JSeparator sep = new JSeparator();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.WEST;
		g.setConstraints(sep, c);
		return sep;
	}

}
