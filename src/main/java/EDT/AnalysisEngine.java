package EDT;

import java.util.*;


import javax.swing.*;
import javax.swing.tree.*;

/**
 * Provides the EDT analysis algorithms for the detection of both parallel and hybrid episodes.
 * This analysis process returns a result presentation panel.
 * 
 * @author: Aaron Ceglar 
 */
public class AnalysisEngine{
	private int granularity = 0;// temporal granularity index (from constants)
	private int period = 0; // temporal granularity quantity
	private boolean orderActors = false; // are actors ordered within event
	private boolean orderEvents = false; // are events ordered within window
	private int [] actorFilter = null; // filter analysis on specific actors

	private double lift = 0; // lift heuristic
	private int support = 0;  // support heuristic
	private int depthConstraint = 0; //constrain analysis extent 

	private long windowSpan = 0; // width of analysis window

	private ParallelTreeNode dmtnParallelRoot = null; // root of parallel episode tree
	private HybridTreeNode dmtnHybridRoot = null; // root of hybrid episode tree
	private ArrayList<Episode> [] aParallelEpisodeWindows = null; // list of parallel episodes to be used in hybrid discovery

	/**
	 * Populate analysis specific parameters. 
	 * 
	 * @param s support heuristic
	 * @param l lift heuristic
	 * @param d depth constraint
	 * @param g granularity
	 * @param p period (quantitative granularity)
	 * @param a order actors
	 * @param w order events
	 * @param af actor filter
	 * 
	 * @calledBy InitialiseEngine
	 */
	public AnalysisEngine(int s, double l, int d, int g, int p, boolean a,	boolean w, int [] af){
		support = s;
		lift = l;
		depthConstraint = d;
		granularity = g;
		period = p;
		orderActors = a;
		orderEvents = w;
		actorFilter = af;
		if(actorFilter == null) actorFilter = new int [0];
		
		// the temporal span of an analysis window
		windowSpan = Constants.tempGranMillis[granularity]*period;
		Store.setWindowSpan(windowSpan); 
	}

	/**
	 * Launch Analysis 
	 * 
	 * @return result panel
	 * @calledBy InitialiseEngine
	 */
	public JPanel analyseDataset(){
		Store.setStatusText("analysing");
		dmtnHybridRoot = new HybridTreeNode(null);
		dmtnParallelRoot = new ParallelTreeNode(-1); // link top level nodes with hybrid
		aParallelEpisodeWindows = new ArrayList [Store.getRawLength()];
		
		// if orderActors ensure that the the required data structure exists.
		if(orderActors) Store.buildOrderedEvents();

		//build the parallel tree and then sort it 
		buildParallelTree(buildFirstParallel());
		dmtnParallelRoot.sort();
		
		// create first level of hybrid tree from parallel tree nodes
		Enumeration en = dmtnParallelRoot.depthFirstEnumeration();
		while(en.hasMoreElements()){
			ParallelTreeNode node = (ParallelTreeNode)en.nextElement();
			dmtnHybridRoot.add(new HybridTreeNode(node, node.temporalData));
		}

		//build the hybrid tree and then sort it
		buildHybridTree(buildFirstHybrid());
		dmtnHybridRoot.sort();

		Store.setStatusText("building report");
		return new AnalysisResults(support, lift, depthConstraint, granularity, period, 
			orderActors, orderEvents, actorFilter, dmtnParallelRoot, dmtnHybridRoot);
	}

	/**
	 * Build the first level of the parallel tree.
	 * - equivalent to valid episodes of event size 1.
	 * 
	 * If actor filter is active and no events within the analysis window that begins with
	 * this current event are valid, analysis moves to the next event only and cannot skip
	 * events as it is not known what events become exposed at the end of the window nor their
	 * validity.

	 * @return set of valid parallel episodes of length 1. As a hashtable :
	 * 		 key : starting index of the episode
	 * 		value: list of valid episodes belonging to this window   
	 */
	private Set<Map.Entry<Integer,ArrayList<Episode>>> buildFirstParallel(){
		// iterate over events. 
		for(int i=0;i<Store.getRawLength();i++){
			int symbol = Store.getRawRecordEvent(orderActors,i);
			long timeStamp = Store.getRawRecordTimeStamp(i);
			
			//if actor filter active and a valid actor does not exist within 
			// the analysis window starting with this event then get next event.
			if(actorFilter.length > 0 && ! windowHasFilter(i)) continue;
			
			// If valid, add to PTree, if already exists it increment the nodes count 
			if(! dmtnParallelRoot.hasChild(symbol, i, timeStamp)){
				dmtnParallelRoot.add(new ParallelTreeNode(symbol, i, timeStamp));
			}
		}
		// remove those tree nodes that do not meet the heuristic constraint.
		return dmtnParallelRoot.pruneTree(support, lift).entrySet();
	}
	
	/**
	 * Iterate over the events in the analysis window begining with the event "index".
	 * If a valid actor (according to the specfied filter) exists within this window
	 * then the window is valid and hence the event is valid.
	 * 
	 * *****************************************
	 * Optimisation 
	 *
	 * If the index =0 and the first valid episode is 6 this means that all windows up
	 * to and including 6 must be valid and this analysis is no longer required.
	 *   
	 * *****************************************
	 *   
	 * @param index : the the first event in current analysis window
	 * @return
	 */
	private boolean windowHasFilter(int index){
		long windowEndDate = Store.getRawRecordTimeStamp(index) + windowSpan;
		for(; index < Store.getRawLength(); index++){
			if(Store.getRawRecordTimeStamp(index) >= windowEndDate)	break;
			Integer[] eventSymbols = Store.getEventArtifacts(Store.getEvent(orderActors, index));
			// if a filter actor id is in this event's set of actor symbols return true 
			for(int actorId: actorFilter){
				for(int eventSymbol: eventSymbols){
					if(actorId - 1 == eventSymbol) return true;
				}
			}
		}
		return false;
	}

	/**
	 * Construct the parallel episode tree.
	 *  - A parallel episode is one in which the order of the participant events is not considered,
	 *    only the fact that this set of events occurred together (within the same window) 
	 *    at least X times (X specified by user heuristics). 
	 * 
	 * 	- Breadth wise tree construction. 
	 *  - A tree level is constructed from the valid nodes of the previous level. This resulting
	 *    set of candidate nodes are then validated with respect to the specified heuristics.
	 *    - Level 3 of the parallel tree holds episodes comprised of 3 events.
	 * 
	 * @param work : the set of valid, active level 1 nodes. As a hashtable :
	 * 	       	  key : starting index of the episode.
	 * 		     value: list of valid episodes belonging to this window. 
	 */
	private void buildParallelTree(Set<Map.Entry<Integer, ArrayList<Episode>>> work){
		int level = 1; // current tree level
		while(true){
			// iterate over the work set, for each work (or analysis) window
			Iterator<Map.Entry<Integer, ArrayList<Episode>>> workIterator = work.iterator();
			while(workIterator.hasNext()){
				Map.Entry<Integer, ArrayList<Episode>> workWindow = workIterator.next();
				int windowIndex = workWindow.getKey(); 
				ArrayList<Episode> windowEpisodes = workWindow.getValue();
				long windowEndDate = Store.getRawRecordTimeStamp(windowIndex) + windowSpan;

				// 2D arraylist of valid parallel episodes, stored by window index, facilitates hybrid detection
				if(aParallelEpisodeWindows[windowIndex] == null){
					aParallelEpisodeWindows[windowIndex] = new ArrayList<Episode>(windowEpisodes.size() + 1);
				}
				aParallelEpisodeWindows[windowIndex].addAll(windowEpisodes);

				// build new level episode candidates for this window
				HashSet<ParallelEpisodeSet> newEpisodeCandidates = new HashSet<ParallelEpisodeSet>();

				// iterate over the window 
				for(int j = windowIndex + 1; j < Store.getRawLength(); j++){
					if(Store.getRawRecordTimeStamp(j) >= windowEndDate)	break;

					// For each existing episode in this window, construct the set of candidates from
					// the existing episodes concatenated with each of the events in the window.
				    // only active leaves in workWindow, ensures event unique.
					Iterator<Episode> windowEpisode = windowEpisodes.iterator();
					while(windowEpisode.hasNext()){
						ParallelEpisodeSet candidate = new ParallelEpisodeSet(windowEpisode.next(), j);
						if(candidate.add(Store.getEvent(orderActors, j)))
							newEpisodeCandidates.add(candidate);
					}
				}

				// Insert candidates into parallel tree, if the episode already exists, as identified
				// by its participating events, increment it count and append to its temporal data.
				Iterator<ParallelEpisodeSet> candidateIterator = newEpisodeCandidates.iterator();
				while(candidateIterator.hasNext()){
					ParallelTreeNode currentNode = dmtnParallelRoot;
					ParallelEpisodeSet candidate = candidateIterator.next();
					Iterator<Integer> candidateElements = candidate.iterator();

					while(candidateElements.hasNext()){
						currentNode = currentNode.addChild(candidateElements.next());
						if(!currentNode.isActive())	break; // inactive tree branch, no insertion.
						if(!candidateElements.hasNext()){
							currentNode.appendTemporalInstance(windowIndex, candidate.startDate, candidate.endDate);
						}
					}
				}
			}
			// Prune tree to remove invalid nodes.
			work = dmtnParallelRoot.pruneTree(support, lift).entrySet();
			// Exit from parallel tree construction either when the root node is no longer active
			// or the depthconstraint has been reached.
			if(!dmtnParallelRoot.isActive()) break;
			if(depthConstraint < ++level) break;
		}
	}

	/**
	 * Builds the first two levels of the hybrid tree based upon the valid parallel episodes within
	 * each temporal window.
	 * Every parallel episode is concatenated with episodes from other windows within the window 
	 * temporal span. This candidate set is then appended to the hybrid tree and then pruned removing
	 * all nodes that don't meet the required user heuristics.
	 * 
	 * No filtering of actors required here as taken care of in validating the parallel nodes.
	 *  
	 * @return set of valid hybrid nodes to be extended
	 */
	private Set<Map.Entry<Integer, ArrayList<Episode>>> buildFirstHybrid(){
		// iterate over each window containing valid parallel episodes
		for(int windowIndex = 0; windowIndex < aParallelEpisodeWindows.length; windowIndex++){
			ArrayList<Episode> window = aParallelEpisodeWindows[windowIndex];
			if(window == null)continue;
	
			HashSet<HybridEpisodeSet> windowHybridCandidates = new HashSet<HybridEpisodeSet>();
			long windowEndDate = Store.getRawRecordTimeStamp(windowIndex) + windowSpan;

			// Concatenate each episode in window with episodes in subsequent windows until the  
			// comparator window start date occurs after windowEndDate. 
			for(Episode episode: window){
				for(int comparatorIndex = windowIndex + 1; comparatorIndex < Store.getRawLength(); comparatorIndex++){
					if(Store.getRawRecordTimeStamp(comparatorIndex) >= windowEndDate) break;

					// Get comparator episodes from the comparator window
					ArrayList<Episode> comparatorWindow = aParallelEpisodeWindows[comparatorIndex];
					if(comparatorWindow == null) continue;

					for(int j = 0; j < comparatorWindow.size(); j++){
						Episode comparatorEpisode = comparatorWindow.get(j);
						// Ensure temporal aspects valid
						// - The comparison episode cannot exceed the valid window bounds  
						// - The comparator episode must begin after the episode terminates
						if(comparatorEpisode.getEnd() > windowEndDate ||
						   episode.getEnd() > comparatorEpisode.getStart())	continue;

						// Append new concatenated episode to list of candidates. 
						// Ensure that the parallel events upon which they are based do
						// not share common event symbols.  
						// Only one hybrid episode of type will be entered per window.
						HybridEpisodeSet candidate = new HybridEpisodeSet(episode, comparatorEpisode.getEnd());
						if(candidate.add(comparatorEpisode)){
							windowHybridCandidates.add(candidate);
						}
					}
				}
			}

			// Insert candidates into hybrid tree.
			Iterator<HybridEpisodeSet> candidates = windowHybridCandidates.iterator();

			// First level hybrid creates the top two levels of the hybrid tree at once
			// Each candidate is inserted into the tree incrementing the count of the episode
			// and appending temporal data if it already exists
			while(candidates.hasNext()){
				AbstractTreeNode currentNode = dmtnHybridRoot;
				HybridEpisodeSet candidate = candidates.next();
				// iterate over the hybrid elements (e.g. participating parallel nodes) 
				Iterator<ParallelTreeNode> elements = candidate.iterator();
				while(elements.hasNext()){
					ParallelTreeNode element = elements.next();
					boolean found = false;
					
					//iterate over children of the current node 
					Enumeration<TreeNode> children = currentNode.children();
					while(children.hasMoreElements()){
						HybridTreeNode child = (HybridTreeNode) children.nextElement();
						if(child.symbolEquals(element)){
							currentNode = child;
							found = true;
							break;
						}
					}
					// if not found create new node and move to it
					if(!found){
						HybridTreeNode newNode = (new HybridTreeNode(element));
						currentNode.add(newNode);
						currentNode = newNode;
					}
					// if last element in candidate hybrid set, add to temporal data 
					if(!elements.hasNext()){
						currentNode.appendTemporalInstance(windowIndex, candidate.startDate, candidate.endDate);
					}else{
					}
				}
			}
		}
		// prune candidates, returning a constructed set of valid nodes  
		return dmtnHybridRoot.pruneTree(support, lift).entrySet();
	}
	

	/**
	 * Discover hybrid episodes through continual extension of the Hybrid Tree.
	 * Process terminates when either there are no active nodes left or the depth constraint is reached.
	 * 
	 * @param sWork : initial valid hybrid nodes 
	 */
	private void buildHybridTree( Set<Map.Entry<Integer, ArrayList<Episode>>> sWork){
		Set<Map.Entry<Integer, ArrayList<Episode>>> work = sWork;
		int level = 1;
		while(true){
			// Iterate over the set of windows in which valid hybrid episodes occur.
			Iterator<Map.Entry<Integer, ArrayList<Episode>>> windows = work.iterator();
			while(windows.hasNext()){
				Map.Entry<Integer, ArrayList<Episode>> window = windows.next();
				int windowIndex = window.getKey();
				ArrayList<Episode> hybridEpisodes = window.getValue();

				HashSet<HybridEpisodeSet> windowCandidates = new HashSet<HybridEpisodeSet>();
				long windowEndDate = Store.getRawRecordTimeStamp(windowIndex) + windowSpan;

				// Concatenate each episode in window with, episodes in subsequent windows until the  
				// comparator window start date occurs after windowEndDate. 
				for(int i = 0; i < hybridEpisodes.size(); i++){
					Episode episode = hybridEpisodes.get(i);
					for(int comparisonIndex = windowIndex + 1; comparisonIndex < Store.getRawLength(); comparisonIndex++){
						if(Store.getRawRecordTimeStamp(comparisonIndex) >= windowEndDate) break;
						ArrayList<Episode> comparatorWindow = aParallelEpisodeWindows[comparisonIndex];
						if(comparatorWindow == null) continue;

						/* Get comparator episodes from the comparator window to reate new candidates
						 * Check that the last two parallel episodes do not contain common
						 * elements, if they do the extension is not valid.
						 * eliminates 1->3->3
						 */
						 
						for(int j = 0; j < comparatorWindow.size(); j++){
							Episode comparisonEpisode = comparatorWindow.get(j);
							
							// Ensure temporal aspects valid
							// - The comparison episode cannot exceed the valid window bounds  
							// - The comparator episode must begin after the episode terminates
							if(comparisonEpisode.getEnd() > windowEndDate ||
							   episode.getEnd() > comparisonEpisode.getStart())continue;

							//	 validity check 
							if(!((HybridTreeNode)episode.getNode()).lastContains(comparisonEpisode.getNode())){
								// need to create a deep clone								
								HybridEpisodeSet candidate = new HybridEpisodeSet( episode.getStart(), comparisonEpisode.getEnd(),false);
								AbstractTreeNode hn = episode.getNode();
								TreeNode[] tn = hn.getPath();
								for(int f=1;f<tn.length;f++){
								  candidate.add(((HybridTreeNode)tn[f]).getSymbol());
								}
								candidate.add((ParallelTreeNode)comparisonEpisode.getNode());
					 	    	windowCandidates.add(candidate);
							}
						}
					}
				}

				// Insert candidates into hybrid tree.
				Iterator<HybridEpisodeSet> candidates = windowCandidates.iterator();
				while(candidates.hasNext()){
					AbstractTreeNode currentNode = dmtnHybridRoot;
					HybridEpisodeSet candidate = candidates.next();
					Iterator<ParallelTreeNode> elements = candidate.iterator();

					while(elements.hasNext()){
						ParallelTreeNode element = elements.next();
						//iterate over children of the current node 
						Enumeration<TreeNode> children = currentNode.children();
						boolean found = false;
						while(children.hasMoreElements()){
							HybridTreeNode child = (HybridTreeNode) children.nextElement();
							if(child.symbolEquals(element)){
								currentNode = child;
								found = true;
								break;
							}
						}
						// if not found create new node and move to it
						if(!found){
							HybridTreeNode newNode = (new HybridTreeNode(element));
							currentNode.add(newNode);
							currentNode = newNode;
						}
						if(!currentNode.isActive())	break;
						// if last element in candidate hybrid set add to temporal data 
						if(!elements.hasNext()){
							currentNode.appendTemporalInstance(windowIndex, candidate.startDate, candidate.endDate);
						}
					}
				}
			}
			// prune candidates, iterate a constructed set of valid nodes  if the root node is still 
			// active and depthConstraint has not been reached
			work = dmtnHybridRoot.pruneTree(support, lift).entrySet();
			if(!dmtnHybridRoot.isActive())break;
			if(depthConstraint < ++level) break;
		}
	}

/*------------Inner Classes-----------------------*/
	
	/**
	 * Maintains the participant set of a candidate parallel episode  
	 * 
	 * @author: Aaron Ceglar 
	 */
	class ParallelEpisodeSet extends TreeSet<Integer>{
		private long startDate = 0;
		private long endDate = 0;

		/**
		 * Constructs a new parallel episode from a given episode and an event symbol.
		 * The comparator orders events if specified by the user, else the symbols 
		 * are simply appended to the end. 
		 * Expand the episode temporal range to include the new event. 
		 * 
		 * @param e :
		 * @param event symbol
		 */
		public ParallelEpisodeSet(Episode e, int eventSymbol){
			super(new Comparator<Integer>(){
				public int compare(Integer a, Integer b){
					if(orderEvents)	return a - b;
					if(a == b)	return 0;
					return 1;
				}
			});
			startDate = e.getStart();
			endDate = e.getEnd();
			addAll(((ParallelTreeNode)e.getNode()).getPathSymbols());

			long time = Store.getRawRecordTimeStamp(eventSymbol);
			if(time < startDate) startDate = time; // should never occur
			if(time > endDate) endDate = time;
		}

		/* 
		 * @see java.util.AbstractSet#hashCode()
		 */
		public int hashCode(){
			Iterator<Integer> iter = this.iterator();
			int i = 1;
			while(iter.hasNext()) i *= iter.next();
			return i;
		}

		/* 
		 * @see java.util.AbstractSet#equals(java.lang.Object)
		 */
		public boolean equals(Object ts){
			if(ts == this) return true;
			if(!(ts instanceof ParallelEpisodeSet))	return false;

			ParallelEpisodeSet p = (ParallelEpisodeSet)ts;
			if(p.size() != size()) return false;

			Iterator<Integer> iterA = iterator();
			Iterator<Integer> iterB = p.iterator();

			while(iterA.hasNext()){
				if(iterA.next() != iterB.next()) return false;
			}
			return true;
		}
	}

	/**
	 * Maintains the participant set of a candidate hybrid episode  
	 * @author: Aaron Ceglar 
	 */
	class HybridEpisodeSet extends TreeSet<ParallelTreeNode>{
		private long startDate = 0;
		private long endDate = 0;

		/**
		 * Constructs a new hybrid episode, specifying its temporal range.
		 * The comparator ensures that during construction of the first level 
		 * if one parallel episode contains a event of the other it is invalid  e.g 1 -> 1
		 *  
		 * *********************************
		 * optimisation (more work needed)
		 * This is messy and requires attention.
		 * The point is that consecutive parallel episodes within a hybrid episode should
		 * not share an event consisting of the same artifacts (actors)
		 * It can be done a lot cleaner than at present.
		 * ********************************* 
	
		 * @param sDate
		 * @param eDate
		 * @param firstLevel : called from buildFirstHybridLevel
		 */
		public HybridEpisodeSet(long sDate, long eDate, final boolean firstLevel){
			super(new Comparator<ParallelTreeNode>(){
				public int compare(ParallelTreeNode a, ParallelTreeNode b){
					if(firstLevel && a.contains(b))	return 0;
					return 1;
				}
			});
			startDate = sDate;
			endDate = eDate;
		}

		/**
		 * First level constructor in which a deep clone of the hybrid tree is not required.
		 * ****************************
		 * work ?
		 * Can't I just keep a pointer to the exiting hybrid episode for which this is a candidate.
		 * Check it out later.
		 * ***************************
		 * @param e : parallel episode
		 * @param eDate : end date
		 */
		public HybridEpisodeSet(Episode e, long eDate){
			this(e.getStart(), eDate, true);
			add((ParallelTreeNode)e.getNode());
		}

		/**
		 * @param e : episode to append to hybrid set
		 * @return if addition was successful
		 */
		public boolean add(Episode e){
			return add((ParallelTreeNode)e.getNode());
		}

		/*
		 * @see java.util.AbstractSet#hashCode()
		 */
		public int hashCode(){
			Iterator iter = iterator();
			int i = 1;
			while(iter.hasNext()){
				i *= (iter.next()).hashCode();
			}	
			return i;
		}

		/* 
		 * @see java.util.AbstractSet#equals(java.lang.Object)
		 */
		public boolean equals(Object ts){
			if(ts == this) return true;
			if(!(ts instanceof HybridEpisodeSet)) return false;
			Iterator iterA = iterator();
			Iterator iterB = ((HybridEpisodeSet)ts).iterator();
			while(iterA.hasNext()){
				if(iterA.next().equals(iterB.next()))return true;
			}
			return true;
		}
	}
}	


