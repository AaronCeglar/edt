package EDT;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;

/**
 * Dialog enabling the specification of a dataset resource for analysis. 
 * At present accepted formats are:
 * 	- tab delimited (.csv, .txt, .dat)
 * 	- MS Access Database (.mdb)
 * 
 * Once selected the:
 * 	- file is validated.
 * 	- database source is initialised and then validated.
 * 
 * After validation the date format (regex) can be changed or a new suitable date format
 *  created (DateFormatDialog).The set of valid date formats are persistant (.df file).  
 *  
 * Once validated the user can continue with application launch "Ok" or terminate "Cancel".  
 *  
 * @author: Aaron Ceglar 
 */

public class InputSourceDialog extends JDialog implements ActionListener{
	private JPanel pMain = null;       // easiest way to get control of Dialog background color
	private JPanel pDynamic = null;    // dynamically populated panel

	private JTextField tbFile = null;
	private JButton bFile = null;
	private File fInput = null;

	private JButton bOk = null;
	private JButton bCancel = null;
	private JButton bDateFormat = null;

	private boolean isTableInitialised = false;   // database flag
	
	private int returnValue = JOptionPane.CANCEL_OPTION;

	/**
	 * Singleton method to facilitate returning a value. 
	 * Intstantiates a new ISDialog is none exists.
	 * 
	 * @calledBy EpisodeDetection
	 */
	public static int getValue(){
		InputSourceDialog dlg = new InputSourceDialog();
		return dlg.returnValue;
	}

	/**
	 * Constructs the Dialog Frame and positions it depending upon 
	 * the existance of the application frame.
	 */
	private InputSourceDialog(){
		super(Store.getFrame(), "EDT Dataset Resource Dialog", true);
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		setPreferredSize(new Dimension(250, 150));
		add(buildGUI());
		
		if(Store.getFrame() != null){
			Rectangle rec = Store.getFrame().getBounds();
			setLocation(((int)rec.getCenterX()) - (getSize().width / 2),
						((int)rec.getCenterY()) - (getSize().height / 2));
		}else{
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			setLocation((dim.width / 2) - 125, (dim.height / 2) - 75);
		}
		pack();
		addWindowListener(new WindowListenerISD());
		setResizable(false);
		setVisible(true);
	}

	/**
	 * populates the Dialog (main panel) with widgets. 
	 */  
	private JPanel buildGUI(){
		pMain = new JPanel();

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		pMain.setLayout(gridBag);
		c.insets = new Insets(5, 5, 5, 5); 
		c.weightx = 1;
		c.weighty = 0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.HORIZONTAL;

		JLabel lbFileDb = new JLabel("Specify the file or access database to use");
		gridBag.setConstraints(lbFileDb, c);
		pMain.add(lbFileDb);
		
		//------File widgets----------
		tbFile = new JTextField();
		tbFile.setEditable(false);
		tbFile.setBackground(Color.WHITE);
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(tbFile, c);
		pMain.add(tbFile);

		bFile = new JButton("...");
		bFile.addActionListener(this);
		c.weightx = 0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.EAST;
		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(bFile, c);
		pMain.add(bFile);
		
		//--------Dynamic panel-------------
		//populated during datasource initialisation and /or validation. 
		pDynamic = new JPanel();
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(pDynamic, c);
		pMain.add(pDynamic);
		
		//---------Action Buttons-------------
		bDateFormat = new JButton("Format Date");
		bDateFormat.addActionListener(this);
		bDateFormat.setVisible(false);
		c.weightx = 0;
		c.weighty = 0;
		c.gridwidth = 1;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		gridBag.setConstraints(bDateFormat, c);
		pMain.add(bDateFormat);

		bOk = new JButton("Ok");
		bOk.addActionListener(this);
		bOk.setEnabled(false);
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(bOk, c);
		pMain.add(bOk);

		bCancel = new JButton("Cancel");
		bCancel.addActionListener(this);
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(bCancel, c);
		pMain.add(bCancel);

		return pMain;
	}

	/**
	 * Enables and focuses the Ok button.
	 * @param b: boolean - enable the Ok Button
	 * @calledBy InitialiseDatabaseInput
	 * @calledBy ValidateInput
	 */
	public void setOkButton(boolean b){
		bOk.setEnabled(b);
		if(b)bOk.requestFocus();
	}
	
	/**
	 * Button Actions.
	 */
	public void actionPerformed(ActionEvent ae){
		//Select input source and then call relevant validation class.
		if(ae.getSource() == bFile){
			Rectangle rec = this.getBounds();
			JFileChooser fcFile = new JFileChooser(System.getProperty("user.dir"));
			fcFile.setDialogTitle("Select File");
			fcFile.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			String[] strFilter = {"csv", "dat", "mdb", "txt"};
			fcFile.setFileFilter(new FileFilterUtility(strFilter, "Data Files: "));
			if(fcFile.showOpenDialog(Store.getFrame()) == JFileChooser.APPROVE_OPTION){
				fInput = fcFile.getSelectedFile();
				tbFile.setText(fInput.getName());
				String[] st = (fInput.getName().split("[^a-zA-Z]"));
				String s = st[st.length - 1];
				pDynamic.removeAll();
				
				// initialise validation class, populate parameter panel. 
				if(s.equals("mdb")){
					isTableInitialised = true;
					bDateFormat.setVisible(false);
					Store.setInputType(Constants.DATABASE);
					// Database connection MUST HAVE absolute path not just filename.
					pDynamic.add(new InitialiseDataBaseInput(fInput.getAbsoluteFile().toString(), this));
				}else{
					Store.setInputType(Constants.FILE);
					pDynamic.add(new ValidateInput(fInput, this));
					bDateFormat.setVisible(true);
				}
				
				setPreferredSize(null);
				pack();
				Rectangle newRec = this.getBounds();
				setLocation(((int)rec.getCenterX()) - (newRec.width / 2), ((int)rec.getCenterY()) - (newRec.height / 2));
			}
		}
		/* If source is Database and it has just been initialised then Ok will instantiate
		 * a validateInput panel, else the dialog is disposed with return value of Ok.*/
		if(ae.getSource() == bOk){
			Rectangle rec = this.getBounds();
			returnValue = JOptionPane.OK_OPTION;
			if(isTableInitialised){
				((InitialiseDataBaseInput)pDynamic.getComponent(0)).dispose();
				isTableInitialised = false;
				pDynamic.removeAll();
				pDynamic.add(new ValidateInput(fInput, this));
				bDateFormat.setEnabled(true);
				bDateFormat.setVisible(true);
				setPreferredSize(null);
				pack();
				Rectangle newRec = this.getBounds();
				setLocation(((int)rec.getCenterX()) - (newRec.width / 2), ((int)rec.getCenterY()) - (newRec.height / 2));
				return;
			}
			removeAll();
			Store.setFInput(fInput);
			((ValidateInput)pDynamic.getComponent(0)).dispose();
			dispose();
		}
		// Dispose dialog with return value of Cancel. 
		if(ae.getSource() == bCancel){
			returnValue = JOptionPane.CANCEL_OPTION;
			dispose();
		}
		// Launch Date Format Dialog via ValidateInput. 
		if(ae.getSource() == bDateFormat){
			((ValidateInput)pDynamic.getComponent(0)).validateDate(this);
		}
	}

/*------------Inner Class-----------------------*/

	/**
	 * Set return value to cancel upon window close.
	 */
	class WindowListenerISD extends WindowAdapter{
		public void windowClosing(WindowEvent e){
			returnValue = JOptionPane.CANCEL_OPTION;
			dispose();
		}
	}

}
