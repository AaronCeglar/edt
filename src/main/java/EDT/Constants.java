package EDT;

import java.awt.Color;

/**
 * Set of constants used within EDT.
 * 
 * @author: Aaron Ceglar, Denise DeVries 
 */
public interface Constants{

	/**
	 * Granularity labels.
	 * 
	 * @calledBy AnalysisResults 
	 */
	public final String[] tempGranularities = {"Minute", "Hour", "Day", "Week", "Month", "Year"};
	
	/**
	 * Temporal extent of granularities.
	 * 
	 * @calledBy AnalysisEngine
	 */
	public final long[] tempGranMillis = {60000L, 3600000L, 86400000L, 604800000L, 2678400000L, 31536000000L};

	/**
	 * Column delimiter used in parsing source files.
	 * 
	 * @calledBy InitialiseEngine
	 */
	public final String columnRegex = "\t";

	/**
	 * Default selection of date formats. 
	 * Overridden by format.df file.
	 * 
	 * @calledBy EpisodeDetection
	 */
	public final String[] dateArrayRegex = {"dd/MM/yyyy", "MM/dd/yyyy", "yyyy-MM-dd HH:mm:SS", "dd-MM-yyyy HH:mm:SS", "dd-MM-yyyy SS:mm:HH"};

	/**
	 * Colour map for episode event presentation.
	 * 
	 *@calledBy  ReportTextDetail
	 */
	public final Color[] colourMap = {Color.black, Color.blue, Color.red, Color.cyan, Color.green, Color.lightGray, Color.magenta, Color.orange, Color.pink, Color.yellow, Color.gray, Color.darkGray};

	/**
	 * Type of data source
	 * 
	 *@calledBy InitialiseEngine InputSourceDialog ValidateInput  
	 */
	public final int FILE = 1;
	public final int DATABASE = 2;

	/**
	 * Representatives of granularity types.  
	 * 
	 * @calledBy ReportEventVisualisation
	 */
	public final int HRS = 1; // Hours.
	public final int DOW = 2; // Days of week.
	public final int DOM = 3; // Days of month.
	public final int WOY = 4; // Weeks of year.
	public final int MOY = 5; // Months of year.

	/**
	 * Type of visualisation  (Grid or Circular).
	 * 
	 * @calledBy ReportEventVisualisation
	 */
	public final int CIRCLE = 0;
	public final int GRID = 1;

	/**
	 * Textual (String) representation of temporal granularity.
	 * 
	 * @calledBy ReportEventVisualisation
	 */
	public final String[] months = {"nil", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	public final String[] statusMonths = {"nil", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	public final String[] days = {"nil", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
	public final String[] statusDays = {"nil", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
}
