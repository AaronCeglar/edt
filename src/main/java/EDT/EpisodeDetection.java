package EDT;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Main class of the Episode Detection Tool
 * 
 * Calls for valid data input, creates application interface and launches the initialise engine.
 * The simple menu does not use JMenu specifics. This was a conscious descision.      
 * @author: Aaron Ceglar
 */

public class EpisodeDetection extends JFrame implements ActionListener{
	private JPanel pMain = null;
	private JButton bOpen = null;  // menu button
	private JButton bAdd = null;   // menu button
	private JButton bPrint = null; // menu button
	private InitialiseEngine iEngine = null;

	/**
	 * EDT requires the entry of a valid data source (text file of Access Database)
	 * else application terminates. If valid the main application
	 * interface is constructed and launches the initialise Engine.
	 */
	public EpisodeDetection(){
		super("Episode Detection");
		loadGlobals();
		if(InputSourceDialog.getValue() == JOptionPane.OK_OPTION){
			java.awt.Image ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/edt.gif"));
			setIconImage((new ImageIcon(ic)).getImage());
			addWindowListener(new WindowListenerEDT());
			pMain = new JPanel(new BorderLayout());
			try{
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}catch(Exception ex){
				System.out.println("Episode Detection: invalid look and feel, default used");
			}
			Store.setFrame(this);
			Store.getFrame().initialiseInterface();

			setContentPane(pMain);
			setPreferredSize(new Dimension(900, 800));
			Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
			setLocation((screenDim.width / 2) - 450, (screenDim.height / 2) - 400);

			pack();
			setResizable(true);
			setVisible(true);
		}else{
			System.exit(0);
		}
	}

	/**
	 * Initialise the main interface
	 */
	public void initialiseInterface(){
		pMain.removeAll();
		iEngine = new InitialiseEngine();
		pMain.add(buildToolBar(), BorderLayout.NORTH);
		pMain.add(iEngine, BorderLayout.CENTER);
		pMain.add(buildStatusPanel(), BorderLayout.SOUTH);
		pMain.revalidate();
	}

	/**
	 * Construct a status panel for adhoc messages
	 * 
	 * @return Status Panel
	 */
	private JPanel buildStatusPanel(){
		JPanel pStatus = new JPanel(new BorderLayout(3, 3));
		pStatus.setPreferredSize(new Dimension(20, 30));
		pStatus.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

		JTextField tfStatus = new JTextField();
		tfStatus.setBackground(Color.WHITE);
		tfStatus.setFont(new Font("SansSerif", Font.PLAIN, 12));
		tfStatus.setForeground(Color.BLUE);
		tfStatus.setBorder(new CompoundBorder(BorderFactory.createLoweredBevelBorder(), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
		tfStatus.setEditable(false);
		pStatus.add(tfStatus, BorderLayout.CENTER);

		Store.setStatusField(tfStatus);
		Store.setStatusText("initialise");
		return pStatus;
	}

	/**
	 * Construct the toolBar panel   
	 * 
	 * @return toolBar
	 */
	private JToolBar buildToolBar(){
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setRollover(true);

		JPanel p = new JPanel();
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		p.setLayout(gridBag);
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(3, 3, 3, 3); // t,l,b,r
		c.weightx = 0;
		c.weighty = 0;

		bOpen = new JButton("open");
		bOpen.setContentAreaFilled(false);
		bOpen.addActionListener(this);
		gridBag.setConstraints(bOpen, c);
		p.add(bOpen);

		bAdd = new JButton("add");
		bAdd.setContentAreaFilled(false);
		bAdd.addActionListener(this);
		gridBag.setConstraints(bAdd, c);
		p.add(bAdd);

		bPrint = new JButton("print");
		bPrint.setContentAreaFilled(false);
		bPrint.addActionListener(this);
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(bPrint, c);
		p.add(bPrint);

		JPanel jPad = new JPanel();
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(jPad, c);
		p.add(jPad);

		toolBar.add(p);
		return toolBar;
	}

	/**
	 * Toolbar actions
	 */
	public void actionPerformed(ActionEvent ae){
		/*Open a new data source */
		if(ae.getSource() == bOpen){
			if(InputSourceDialog.getValue() == JOptionPane.OK_OPTION){
				Store.clearInputList();
				Store.addInputTitle(Store.getFInput().getName());
				initialiseInterface();
			}else{
				pMain.revalidate();
			}
		}
		/*Append a new dataset to the existing data store*/
		if(ae.getSource() == bAdd){
			if(InputSourceDialog.getValue() == JOptionPane.OK_OPTION){
				Store.addInputTitle(Store.getFInput().getName());
				iEngine.addDataset();
				pMain.revalidate();
			}else{
				pMain.revalidate();
			}
		}
		/*Print the active Tab*/
		if(ae.getSource() == bPrint){
			if(iEngine == null)	return;
			iEngine.printActiveTab();
		}
	}

	/**
	 * loads the persistent set of valid date formats. 
	 * If no persistent set exists, populate with default dateArrayRegex from Constants class 
	 */
	private void loadGlobals(){
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("df.edt"));
			Object st = ois.readObject();
			if(st instanceof ArrayList){
				Store.setDateFormats((ArrayList<SimpleDateFormat>)st);
			}
			ois.close();
		}catch(FileNotFoundException e){
			System.out.println("Load GLobals : File df.edt not found, loading defaults");

			ArrayList<SimpleDateFormat> s = new ArrayList<SimpleDateFormat>(Constants.dateArrayRegex.length);
			for(int i = 0; i < Constants.dateArrayRegex.length; i++){
				s.add(new SimpleDateFormat(Constants.dateArrayRegex[i]));
			}
			Store.setDateFormats(s);
			Store.setCurrentDateFormat(s.get(0));

		}catch(IOException e){
			System.out.println("Load GLobals : IOException");
		}catch(ClassNotFoundException e){
			System.out.println("Load GLobals : Class not found Exception");
		}
	}

	/**
	 * Save the set of valid date formats for persistance purposes
	 */
	private void saveGlobals(){
		try{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("df.edt"));
			oos.writeObject(Store.getDateFormats());
			oos.close();

		}catch(Exception e){
			System.out.println("Save Globals : Error writing fd.edt");
		}
	}

	/**
	 * main method
	 */
	public static void main(String[] args){
		new EpisodeDetection();
	}

/*------------Inner Class---------------*/

	/**
	 * Save globals upon exiting EDT application  
	 */
	class WindowListenerEDT extends WindowAdapter{
		public void windowClosing(WindowEvent e){
			saveGlobals();
			System.exit(0);
		}
	}

}
