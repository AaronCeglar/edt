package EDT;

import java.awt.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * A panel which appears part of the InputSourceDiualog in order to validate the required
 * time stamp feild. Given the selection of a feild from an exemplar datasource record the 
 * class searches for a valid date interpretation. If none exist the user is required
 * to create a valid timestamp (regular expression).
 * 
 *  The user is also able to instigate the DateFormatDialog, to select a format from a valid list
 *  of formats if the selewcted format is incorrect, or to create a new format. 
 *  
 * @author: Aaron Ceglar 
 */

public class ValidateInput extends JPanel implements ChangeListener{
	private InputSourceDialog parent = null; // owner.

	private String[] trans = null; // The exemplar string representation of the record.
	private File fPath = null; // DataSource file path.

	private JSpinner spTimeStamp = null; // TimeStamp feild selector.
	private JLabel lbSelectedTimeStampFeild = null; // Display currently selected timestamp feild.
	private int currentTimeStampColumn = 1; // Currently selected column.

	/**
	 * Instantiate Dialog
	 * 
	 * @param f Input file
	 * @param p Parent component
	 * @calledBy  InputSourceDialog
	 */
	public ValidateInput(File f, InputSourceDialog p){
		super();
		fPath = f;
		parent = p;
		buildGUI();
		setVisible(true);
	}

	/**
	 * Construct dialog widgets. Comprised of 
	 * - exemplar record display 
	 * - selected timestamp feild
	 */
	private void buildGUI(){
		removeAll();
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Validate Date Field", TitledBorder.CENTER, TitledBorder.TOP, new Font("Times", Font.PLAIN, 12), Color.BLUE));

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		setLayout(gridBag);

		c.insets = new Insets(5, 5, 5, 5);
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0;
		c.weighty = 0;

		//-------Example record--------
		JLabel lbExample = new JLabel("Example Record");
		gridBag.setConstraints(lbExample, c);
		add(lbExample);

		// get the example record
		if(Store.getInputType() == Constants.FILE){
			BufferedReader in;
			try{
				in = new BufferedReader(new FileReader(fPath));
				String line = in.readLine();
				trans = line.split(Constants.columnRegex);
				in.close();
			}catch(FileNotFoundException e){
				System.out.println("Validate Input: Input File not found");
			}catch(IOException e){
				System.out.println("Validate Input: Input File error");
			}
		}else{
			trans = Store.getFirstDbData().split("\t");
		}

		// initialisae the timeStamp column Date representation
		lbSelectedTimeStampFeild = new JLabel();
		lbSelectedTimeStampFeild.setHorizontalAlignment(SwingConstants.LEFT);

		// construct the example record display String
		JPanel pExample = new JPanel(new FlowLayout());

		// Get valid date format. If none exist then disable Ok button.
		// User is required to create valid date format within FormatDateDialog.
		if(!isValidDateField(trans[currentTimeStampColumn - 1])){
			pExample.add(new JLabel("No valid date formats exist"));
			parent.setOkButton(false);
		}else{
			JLabel lb = new JLabel("|");
			lb.setForeground(Color.RED);
			pExample.add(lb);

			for(int i = 0; i < trans.length; i++){
				JLabel lbx = new JLabel(trans[i]);
				lbx.setForeground(Color.BLUE);
				pExample.add(lbx);
				lb = new JLabel("|");
				lb.setForeground(Color.RED);
				pExample.add(lb);
			}
			parent.setOkButton(true);
		}

		JScrollPane scpTransaction = new JScrollPane(pExample);
		scpTransaction.setBorder(BorderFactory.createEmptyBorder());
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(scpTransaction, c);
		add(scpTransaction);

		//---------Timestamp column selector-------
		spTimeStamp = new JSpinner();
		spTimeStamp.addChangeListener(this);
		spTimeStamp.setModel(new SpinnerNumberModel(currentTimeStampColumn, 1, trans.length, 1));

		JLabel lbTimeStamp = new JLabel("TimeStamp Column:");
		c.gridwidth = 1;
		gridBag.setConstraints(lbTimeStamp, c);
		add(lbTimeStamp);

		gridBag.setConstraints(spTimeStamp, c);
		add(spTimeStamp);

		lbSelectedTimeStampFeild.setPreferredSize(new Dimension(140, 25));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(lbSelectedTimeStampFeild, c);
		add(lbSelectedTimeStampFeild);
	}

	/**
	 * Tests a given string against the currently known date formats. If a valid
	 * date format exists then the the exemplar date feild is set and true
	 * returned else the false is returned.
	 * 
	 * @param candidate  String to test for date validity.
	 * @return Valid date format exists.
	 */
	private boolean isValidDateField(String candidate){
		for(SimpleDateFormat dateFormat: Store.getDateFormats()){
			try{
				Date date = dateFormat.parse(candidate);
				lbSelectedTimeStampFeild.setText(dateFormat.format(date));
				Store.setCurrentDateFormat(dateFormat);
				return true;
			}catch(ParseException e){}
		}
		lbSelectedTimeStampFeild.setText("");
		return false;
	}

	/**
	 * Change Listener for spTimeStamp. If the spinner moves then the interface
	 * must be reconstructed given the newly selected date feild.
	 */
	public void stateChanged(ChangeEvent ce){
		currentTimeStampColumn = (Integer)(spTimeStamp.getValue());
		buildGUI();
		revalidate();
	}

	/**
	 * Instantiates the Date Format Dialog. reConstructs this dialolg upon return.
	 * 
	 * @calledBy InputSourceDialog.
	 * @param p Owner (parent) component.
	 */
	public void validateDate(JDialog p){
		new DateFormatDialog(p, trans[currentTimeStampColumn - 1]);
		isValidDateField(trans[currentTimeStampColumn - 1]);
		buildGUI();
		revalidate();
	}

	/**
	 * Upon dispose set the identity of the date feild in Store.
	 * Triggered upon parent dialog close.
	 * 
	 * @calledBy InputDataSource
	 */
	public void dispose(){
		Store.setTimeStampColumn((Integer)(spTimeStamp.getValue()));
	}

}
