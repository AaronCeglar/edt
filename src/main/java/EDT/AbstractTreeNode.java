package EDT;

import java.util.*;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Serves as overarching class of common methods for both the parallel and hybrid tree structures
 * constructede during analysis. As such contains shared methods used during analysis and presentation
 * along with a set of overridden meethods.
 * 
 * @author: Aaron Ceglar 
 *
 */
public class AbstractTreeNode extends DefaultMutableTreeNode {
	protected int count = 0; // number of instances of an episode relating to this node
	protected double lift = 0.0; // the episodes actual lift (measure of surprise)
	protected boolean active = true; // can the subtree roorted at this node still be extended
	protected boolean valid = false; // is the node valid with repect to user defined heuristics
	protected TreeSet<EpisodeInstance> temporalData = null; // the temporal instances associated with this episode 
	protected boolean isVisibleInTree = true; // is node visible in tree based presentation
	protected boolean isVisibleInList = true; // is node visible in list based presentation

	/**
	 * Constructor.
	 * 
	 * @calledBy HybridTeeNode, ParallelTreeNode
	 */
	public AbstractTreeNode(){
		super();
		temporalData = new TreeSet<EpisodeInstance>(new Comparator<EpisodeInstance>(){
			public int compare(EpisodeInstance arg0, EpisodeInstance arg1){
				return arg0.window - arg1.window;
			}
		});
	}

/*=================Analysis Methods===============*/	
	
	/**
	 * Append a new temporal instance (of episode) to the tree node. 
	 * 
	 * @param window
	 * @param sDate
	 * @param eDate
	 * @calledBy AnalysisEngine
	 */
	public void appendTemporalInstance(int window, long sDate, long eDate){
		count++;
		temporalData.add(new EpisodeInstance(sDate, eDate, window));
	}

	/**
	 * Launch recursive sort tree algorithm.
	 * 
	 * @calledBy AnalysisEngine
	 */
	public void sort(){
		sortTree(this);
	}

	/**
	 * Sort the tree into "count" order.
	 * 
	 * @param node
	 */
	private void sortTree(AbstractTreeNode node){
		if(node.getChildCount() <= 1) return;
		for(int j = 0; j < node.getChildCount(); j++){
			AbstractTreeNode child = (AbstractTreeNode)node.getChildAt(j);
			int i = 0;
			for(; i < j; i++){
				AbstractTreeNode testChild = (AbstractTreeNode)node.getChildAt(i);
				if(child.count > testChild.count) break;
			}
			node.insert(child, i);
		}
		for(int i = 0; i < node.getChildCount(); i++)
			sortTree(((AbstractTreeNode)node.getChildAt(i)));
	}

	/**
	 * After a tree level is constructed, it is pruned based upon used defined heuristics, either
	 * support of lift (see tech doc). The resulting set of valid leaf nodes are then used to 
	 * construct a table of valid episodes within each temporal window that are used as the
	 * basis for the next level of tree construction. This structure is returned.
	 * 
	 * @param support : presence heuristic.
	 * @param lift : surprise heuristic.
	 * @return set of valid episodes and the windows in which they occur. 
	 * @calledBy AnalysisEngine
	 */
	public Hashtable<Integer, ArrayList<Episode>> pruneTree(int support, double lift){
		Hashtable<Integer, ArrayList<Episode>> table = new Hashtable<Integer, ArrayList<Episode>>();
		if(lift == 0.0){
			pruningIteratorSupport(this, support, table);
		}else{
			pruningIteratorLift(this, support, lift, table);
		}
		return table;
	}

	/**
	 * Iterate over tree (recursive), remove nodes that don't meet the required support threshold.
	 * 
	 * Only "active" subtrees are visited, a tree becomes inactive when all its children are inactive
	 * or when it is a valid node (previously constructed) that has no children. The active
	 * flag signifies that this nodes subtree is eligable for further analysis. The "valid' flag 
	 * indicates if the node has met the heuristic constraints.    
	 * 
	 * @param node : current node in tree traversal 
	 * @param table : set of new valid episodes and the windows in which they occur
	 * @param support : heuristic
	 * @return 1: if the node has been removed, maintain correct traversal
	 * 		   0: if valid  	
	 */		   
	protected int pruningIteratorSupport(AbstractTreeNode node, int support,
					                     Hashtable<Integer, ArrayList<Episode>> table){
		if(!node.active) return 0; // if inactive then don't process node
		// i maintains proper increment give deletion of nodes
		for(int i = 0; i < node.getChildCount(); i++){
			i = i - pruningIteratorSupport(((AbstractTreeNode)node.getChildAt(i)), support, table);
		}

		// if valid then test for active status	(if new node not set to valid yet)
		if(node.valid){
			// if all the existing children are inactive then make this one inactive also
			if(node.getChildCount() == 0 || !node.hasActiveChild()) node.active = false;
		}else{
			// If new test against heuristic. If fail then remove else set to valid and add specific
			// temporal instances to table.
			if(node.count < support){
				node.removeFromParent();
				return 1;
			}else{
				node.valid = true;
				populateWorkTable(node, table);
			}
		}
		return 0;
	}
	
	/**
	 * Iterate over tree (recursive), remove nodes that don't meet the required support and lift
	 * thresholds.
	 * 
	 * Only "active" subtrees are visited, a tree becomes inactive when all its children are inactive
	 * or when it is a valid node (previously constructed) that has no children. The active
	 * flag signifies that this nodes subtree is eligable for further analysis. The "valid' flag 
	 * indicates if the node has met the heuristic constraints.    
	 * 
	 * @param node
	 * @param table
	 * @param minSupport
	 * @param minLift
	 * @return
	 */
	protected int pruningIteratorLift(AbstractTreeNode node, int minSupport, double minLift,
					                  Hashtable<Integer, ArrayList<Episode>> table){
		if(!node.active)return 0; // if inactive then don't process node
		for(int i = 0; i < node.getChildCount(); i++){
			i = i - pruningIteratorLift(((AbstractTreeNode)node.getChildAt(i)), minSupport, minLift, table);
		}
		// if valid then test for active status	(if new node not set to valid yet)
		if(node.valid){ 
			// if all the existing children are inactive then make this one inactive also
			if(node.getChildCount() == 0 || !node.hasActiveChild()) node.active = false;
		}else{
			AbstractTreeNode parent = (AbstractTreeNode)node.parent;
			// if top level node, cant't calculate lift. Validity based on support heuristic. 
			if(parent.isRoot() && node.count > minSupport){
				node.lift = 0;
				node.valid =true;
				populateWorkTable(node, table);
			// else calculate lift from parent frequency and  	
			}else{

				double recCount = Store.getWindowCount(); // number of windows
				double nodeOcc = node.count / recCount; // percentage of windows containing this pattern  
				double parentOcc = parent.count / recCount; // percentage of windows containing parent pattern   
				
				double eventOcc = 0.0; // occurance of event/parallelnode appended to parent to create new node
				if(node instanceof ParallelTreeNode){
					eventOcc = Store.getEvent((((ParallelTreeNode)node).getSymbol())).getCount() / recCount;
				}else{
					eventOcc = Store.getEvent((((HybridTreeNode)node).getSymbol()).count).getCount() / recCount;
				}

				//calculate lift 
				double li = nodeOcc / (parentOcc * eventOcc);
				
				// validate heuristic
				if(li > minLift && node.count > minSupport){
					node.lift = li;
					node.valid = true;
					populateWorkTable(node, table);
				}else{
					node.removeFromParent();
					return 1;
				}
			}
		}
		return 0;
	}

	/**
	 *  For each temporal instance of a valid node, create an Episode. If the window in which this
	 *  instance occurred already exists within the table, add the Episode to this window
	 *  episode data, else create a new bucket for that window in the table and append the episode 
	 *  to that.
	 * 
	 * @param node : from which the episodes are derived
	 * @param table : to store the window/event info for subsequent an analysis 
	 */
	private void populateWorkTable(AbstractTreeNode node, Hashtable<Integer, ArrayList<Episode>> table){
		for(EpisodeInstance td : node.temporalData){
			// create new episode.
		    Episode episode = node.createNewEpisodeData(td.start, td.end, node);
			ArrayList<Episode> validWindowEvents = table.get(td.window);
			if(validWindowEvents == null){
				// new valid window.
				ArrayList<Episode> newValidWindow = new ArrayList<Episode>();
				newValidWindow.add(episode);
				table.put(td.window, newValidWindow);
			}else{
				// valid window exists.
				validWindowEvents.add(episode);
			}
		}
	}

	/**
	 * Does this node have an active child in its subtree.
	 * 
	 * @return active status
	 */
	private boolean hasActiveChild(){
		Enumeration en = children();
		while(en.hasMoreElements()){
			if(((AbstractTreeNode)en.nextElement()).isActive())	return true;
		}
		return false;
	}

/*=================Presentation Methods===============*/	
			
	/**
	 * Set the visiblility of this node in the tree presentation.
	 * @param visibility
	 * @calledBy AnalysisResults
	 */
	public void setVisibleInTree(boolean visible){
		this.isVisibleInTree = visible;
	}

	/**
	 * Does this node have a tree visible child.
	 * 
	 * @return has visible child
	 * @calledBy AnalysisResults
	 */
	public boolean hasTreeVisibleChild(){
		Enumeration en = children();
		while(en.hasMoreElements()){
			if(((AbstractTreeNode)en.nextElement()).isVisibleInTree) return true;
		}
		return false;
	}

	/**
	 * Set the visiblility of this node in the tree presentation .
	 * 
	 * @param visible: T/F
	 * @calledBy AnalysisResults
	 */
	public void setVisibleInList(boolean visible){
		this.isVisibleInList = visible;
	}

	/**
	 * Is this node visiblw within the list.
	 * 
	 * @return visibility
	 * @calledBy AnalysisResults
	 */
	public boolean isVisibleInList(){
		return isVisibleInList;
	}

	/**
	 * Is the parent visibli in the list.
	 * 
	 * @return parents visibility
	 * @calledBy AnalysisResults
	 */
	public boolean isParentVisibleInList(){
		return ((AbstractTreeNode)parent).isVisibleInList;
	}
	
	/**
	 * @return the lift measure associated with this node
	 * @calledBy AnalysisResults
	 */
	public double getLift(){
		return lift;
	}
	
/*=================Overridden Methods===============*/	

	/** 
	 * Overridden in parallelTreeNode and hybridTreeNode.
	 */
	public String toString(){return "";	}
	
	/**
	 * Overridden in ParallelTreeNode and HybridTreeNode
	 */
	protected boolean containsCommonEvent(Object o){return false;}

	/**
	 * Get the details of the associated episode to print out in a list context.
	 * Overridden in ParallelTreeNode and HybridTreeNode
	 */
	public Integer[] contentsToPrintList(){return null;	}

	/**
	 * Get the details of the associated episode to print out in a tree context.
	 * Overridden in ParallelTreeNode and HybridTreeNode
	 */
	public Integer[] contentsToPrintTree(){return null;	}

	/**
	 * Get the set of event identifiers that comprise the associated episode.
	 * Overridden in ParallelTreeNode and HybridTreeNode
	 */
	public Integer[] getEvents(){return null;}
	
	/**
	 * @param sDate : start date
	 * @param eDate : end date
	 * @param node : episode node
	 * @return new Episode instance
	 */
	protected Episode createNewEpisodeData(long sDate, long eDate, AbstractTreeNode node){
		return new Episode(sDate, eDate, node, null);
	}

	/**
	 * @return active status
	 * @calledBy AnalysisEngine
	 */
	public boolean isActive(){
		return active;
	}

	/**
	 * @return multidimensional array containing the episode instance data
	 * @calledBy ReportTextDetail
	 */
	public long[][] getTemporalData(){
		int count = 0;
		long[][] a = new long[temporalData.size()][3];
		for(EpisodeInstance episode : temporalData){
			a[count][0] = episode.window;
			a[count][1] = episode.start;
			a[count++][2] = episode.end;
		}
		return a;
	}

	/**
	 * Print out a textual representation of the subtree rooted at this node.
	 * Used during development.
	 */
	public void pTree(){
		StringBuffer sb = new StringBuffer("\n");
		Enumeration e = preorderEnumeration();
		while(e.hasMoreElements()){
			AbstractTreeNode n = (AbstractTreeNode)e.nextElement();
			for(int i=0;i<n.getLevel();i++)sb.append("  ");
			sb.append(n.toString()+"\n");
		}
		System.out.println(sb.toString());
	}
	
/*--------------------Inner class-------------------*/

	/**
	 * Stores an instance of an episode:
	 *  - temporal range 
	 *  - analysis window
	 *  
	 *  ****************************
	 *  optimisation
	 *  
	 *  is this required?
	 *  can't we just create instances of episodes
	 *  *******************************
	 *  
	 * @author: Aaron Ceglar 
	 */
	class EpisodeInstance extends Object{
		public long start = 0;
		public long end = 0;
		public int window = 0;

		public EpisodeInstance(long s, long e, int w){
			start = s;
			end = e;
			window = w;
		}
	}
}
