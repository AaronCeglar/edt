package EDT;

import java.awt.*;
import java.awt.event.*;


import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ActionMapUIResource;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

/**
 * Standard Java does not come with a closeable tab pane. This class wraps a
 * BasicTabPaneUI extension that provides a closeable tab pane through the
 * addition of close widget within the Tab (next to the title). This widget is
 * active (red) upon the active tab and upon rollover of inactive tabs, turns
 * the inactive grey image to the active red. When red the tab can be delete by
 * clicking upon the widget.
 * 
 * Furthermore, specific to EDT the raw tab must always exist and hence does not
 * have said widget.
 * 
 * @author: Aaron Ceglar
 * @calledBy InitialiseEngine
 * 
 */
public class CloseTabPane extends JTabbedPane{

	/**
	 * wrapper constructor
	 */
	public CloseTabPane(){
		super();
		setUI(new CloseTabPaneUI());
	}
}

/*------------Inner Class-----------------------*/

/**
 * Extends BasicTabPaneUI to include a close widget upon each tab.
 * 
 * @author: Aaron Ceglar
 */
/**
 * @author: Aaron Ceglar 
 *
 */
class CloseTabPaneUI extends BasicTabbedPaneUI{
	protected ScrollableTabSupport tabScroller; // if scroll bars required in pane
	protected MyMouseMotionListener motionListener; // track roll over
	private Image imActive; // red active image
	private Image imInactive; // grey inactive image

	// widget state
	private static final int INACTIVE = 0;
	private static final int OVER = 1;
	private static final int PRESSED = 2;
	private static final int ACTIVE = 3;

	private int overTabIndex = -1;
	private int tabCount;
	private int closeWidgetStatus = INACTIVE; 

	/**
	 * Default constructor.
	 * Get widget images.
	 */
	public CloseTabPaneUI(){
		super();
		imActive = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/closeFocus.gif"));
		imInactive = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/close.gif"));
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#calculateTabWidth(int, int, java.awt.FontMetrics)
	 */
	protected int calculateTabWidth(int tabPlacement, int tabIndex,	FontMetrics metrics){
		if(tabIndex == 0){
			return super.calculateTabWidth(tabPlacement, tabIndex, metrics) + 8;
		}
		return super.calculateTabWidth(tabPlacement, tabIndex, metrics) + 23;
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#calculateTabHeight(int, int, int)
	 */
	protected int calculateTabHeight(int tabPlacement, int tabIndex,
					int fontHeight){
		return super.calculateTabHeight(tabPlacement, tabIndex, fontHeight) + 5;
	}

	/**
	 * Layout the tab label
	 * 
	 * @param tabPlacement : not used (hardcoded LEFT)
	 * @param metrics : Font information
	 * @param tabIndex : index of the tab
	 * @param title : tab title
	 * @param tabRect : tab bounding box
	 * @param iconRect : icon bounding box
	 * @param textRect : title bounding box
	 * @param isSelected : is tab active
	 */
	protected void layoutLabel(int tabPlacement, FontMetrics metrics,
					int tabIndex, String title, Rectangle tabRect,
					Rectangle iconRect, Rectangle textRect, boolean isSelected){

		textRect.x = textRect.y = iconRect.x = iconRect.y = 0;
		SwingUtilities.layoutCompoundLabel(tabPane, metrics, title, null, SwingUtilities.CENTER, 
						                   SwingUtilities.LEFT, SwingUtilities.CENTER, 
						                   SwingUtilities.CENTER, tabRect, iconRect, 
						                   textRect, textIconGap);
		textRect.x = tabRect.x + 8 + textIconGap;
	}

	/**
	 * Create a scroll button
	 * 
	 * @param direction : north, south, east or west button.
	 * @return new scroll button
	 */
	protected ScrollableTabButton createScrollableTabButton(int direction){
		return new ScrollableTabButton(direction);
	}


	/**
	 * set widget rollover behaviour	
	 *   
	 * @param x : mouse x-coordinate
	 * @param y : mouse y-coordinate
	 */
	private void setTabIcons(int x, int y){
		if(overTabIndex != (overTabIndex = getTabAtLocation(x, y))){
			tabScroller.tabPanel.repaint();
		}
		if(overTabIndex > 0 && overTabIndex != -1){
			int newCloseIndexStatus = INACTIVE;
			Rectangle closeRect = newCloseRect(rects[overTabIndex]);
			if(closeRect.contains(x, y))
				newCloseIndexStatus = ACTIVE;
			if(closeWidgetStatus != (closeWidgetStatus = newCloseIndexStatus))
				tabScroller.tabPanel.repaint();
		}
	}

	/**
	 * Get the extended tab title rectangle that encompasses widget as well as title
	 * 
	 * @param rect : initial tab title rectangle
	 * @return : new rect
	 */
	protected Rectangle newCloseRect(Rectangle rect){
		int dx = rect.x + rect.width;
		int dy = (rect.y + rect.height) / 2 - 6;
		return new Rectangle(dx - 21, dy, 16, 16);
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#createLayoutManager()
	 */
	protected LayoutManager createLayoutManager(){
		return new TabbedPaneScrollLayout();
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#installComponents()
	 */
	protected void installComponents(){
		if(tabScroller == null){
			tabScroller = new ScrollableTabSupport();
			tabPane.add(tabScroller.viewport);
			tabPane.add(tabScroller.scrollForwardButton);
			tabPane.add(tabScroller.scrollBackwardButton);
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#uninstallComponents()
	 */
	protected void uninstallComponents(){
		tabPane.remove(tabScroller.viewport);
		tabPane.remove(tabScroller.scrollForwardButton);
		tabPane.remove(tabScroller.scrollBackwardButton);
		tabScroller = null;
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#installListeners()
	 */
	protected void installListeners(){
		if((tabChangeListener = createChangeListener()) != null){
			tabPane.addChangeListener(tabChangeListener);
		}
		if((mouseListener = createMouseListener()) != null){
			tabScroller.tabPanel.addMouseListener(mouseListener);
		}
		if((motionListener = new MyMouseMotionListener()) != null){
			tabScroller.tabPanel.addMouseMotionListener(motionListener);
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#uninstallListeners()
	 */
	protected void uninstallListeners(){
		if(mouseListener != null){
			tabScroller.tabPanel.removeMouseListener(mouseListener);
			mouseListener = null;
		}
		if(motionListener != null){
			tabScroller.tabPanel.removeMouseMotionListener(motionListener);
			motionListener = null;
		}
		if(tabChangeListener != null){
			tabPane.removeChangeListener(tabChangeListener);
			tabChangeListener = null;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#createChangeListener()
	 */
	protected ChangeListener createChangeListener(){
		return new TabSelectionHandler();
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#createMouseListener()
	 */
	protected MouseListener createMouseListener(){
		return new CloseTabPaneMouseHandler();
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#installKeyboardActions()
	 */
	protected void installKeyboardActions(){
		ActionMap map = new ActionMapUIResource();
		map.put("scrollTabsForwardAction", new ScrollTabsForwardAction());
		map.put("scrollTabsBackwardAction", new ScrollTabsBackwardAction());

		SwingUtilities.replaceUIActionMap(tabPane, map);
		tabScroller.scrollForwardButton.setAction(map.get("scrollTabsForwardAction"));
		tabScroller.scrollBackwardButton.setAction(map.get("scrollTabsBackwardAction"));
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#uninstallKeyboardActions()
	 */
	protected void uninstallKeyboardActions(){
		SwingUtilities.replaceUIActionMap(tabPane, null);
	}

	
// -------------------- UI Rendering----------------------//

	/* (non-Javadoc)
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paint(java.awt.Graphics, javax.swing.JComponent)
	 */
	public void paint(Graphics g, JComponent c){
		int tc = tabPane.getTabCount();
		if(tabCount != tc) tabCount = tc;
		ensureCurrentLayout();
		paintContentBorder(g, tabPane.getTabPlacement(), tabPane.getSelectedIndex());
	}

	/**
	 * Paint the Tab include the close widget
	 * 
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paintTab(java.awt.Graphics, int, java.awt.Rectangle[], int, java.awt.Rectangle, java.awt.Rectangle)
	 */
	protected void paintTab(Graphics g, int tabPlacement, Rectangle[] rects,
					int tabIndex, Rectangle iconRect, Rectangle textRect){
		Rectangle tabRect = rects[tabIndex];
		int selectedIndex = tabPane.getSelectedIndex();
		boolean isSelected = selectedIndex == tabIndex;
		boolean isOver = overTabIndex == tabIndex;
		Graphics2D g2 = null;
		Shape save = null;
		boolean cropShape = false;
		int cropx = 0;
		int cropy = 0;

		if(g instanceof Graphics2D){
			g2 = (Graphics2D)g;
			Rectangle viewRect = tabScroller.viewport.getViewRect();
			int cropline = viewRect.x + viewRect.width;

			if((tabRect.x < cropline) && (tabRect.x + tabRect.width > cropline)){
				cropx = cropline - 1;
				cropy = tabRect.y;
				save = g2.getClip();
				g2.clipRect(tabRect.x, tabRect.y, tabRect.width, tabRect.height);
				cropShape = true;
			}
		}

		paintTabBackground(g, tabPlacement, tabIndex, tabRect.x, tabRect.y, tabRect.width, tabRect.height, isSelected);
		paintTabBorder(g, tabPlacement, tabIndex, tabRect.x, tabRect.y, tabRect.width, tabRect.height, isSelected);

		String title = tabPane.getTitleAt(tabIndex);
		Font font = tabPane.getFont();
		FontMetrics metrics = g.getFontMetrics(font);

		layoutLabel(tabPlacement, metrics, tabIndex, title, tabRect, iconRect, textRect, isSelected);
		paintText(g, tabPlacement, font, metrics, tabIndex, title, textRect, isSelected);

		// paint widget
		if(cropShape){
			g.setColor(shadow);
			g.drawLine(cropx, cropy, cropx, cropy + rects[tabIndex].height);
			g2.setClip(save);
		}else{
			if(tabIndex > 0){
				int dx = tabRect.x + tabRect.width - 21;
				int dy = ((tabRect.y + tabRect.height) / 2 - 6) + 1;

				if((closeWidgetStatus == ACTIVE && isOver) || isSelected){
					g.drawImage(imActive, dx, dy + 1, null);
				}else{
					g.drawImage(imInactive, dx, dy + 1, null);
				}
			}
		}
	}

	/** 
	 * Paint the label text. Use Blue for the active text
	 * 
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paintText(java.awt.Graphics, int, java.awt.Font, java.awt.FontMetrics, int, java.lang.String, java.awt.Rectangle, boolean)
	 */
	protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, 
					int tabIndex, String title,	Rectangle textRect, boolean isSelected){
		g.setFont(font);
		if(tabPane.isEnabled() && tabPane.isEnabledAt(tabIndex)){
			if(isSelected){
				g.setColor(Color.blue);
			}else{
				g.setColor(tabPane.getForegroundAt(tabIndex));
			}
			BasicGraphicsUtils.drawStringUnderlineCharAt(g, title, -1, textRect.x, textRect.y + metrics.getAscent());
		}
	}


	/**
	 * If tabPane doesn't have a peer yet, the validate() will silently fail.
	 * Handle by forcing a layout if tabPane is still invalid (bug 4237677).
	 */
	private void ensureCurrentLayout(){
		if(!tabPane.isValid()) tabPane.validate();
		
		if(!tabPane.isValid()){
			TabbedPaneLayout layout = (TabbedPaneLayout)tabPane.getLayout();
			layout.calculateLayoutInfo();
		}
	}

	/**
	 * @param x co-ordinate
	 * @param y co-ordinate
	 * @return current mouse over tab
	 */
	private int getTabAtLocation(int x, int y){
		ensureCurrentLayout();

		for(int i = 0; i < tabPane.getTabCount(); i++){
			if(rects[i].contains(x, y))	return i;
		}
		return -1;
	}

	/**
	 * Returns the index of the tab closest to the passed in location, note that
	 * the returned tab may not contain the location x,y.
	 *
	 * @param x coordinate
	 * @param y coordinate
	 * @return index of closest tab
	 */
	private int getClosestTab(int x, int y){
		int min = 0;
		int tabCount = Math.min(rects.length, tabPane.getTabCount());
		int max = tabCount;

		while(min != max){
			int current = (max + min) / 2;
			int minLoc = rects[current].x;
			int maxLoc = minLoc + rects[current].width;

			if(x < minLoc){
				max = current;
				if(min == max) return Math.max(0, current - 1);
			}else{
				if(x >= maxLoc){
					min = current;
					if(max - min <= 1) return Math.max(current + 1, tabCount - 1);
				}else{
					return current;
				}
			}
		}
		return min;
	}

//------------------Inner classes---------------------//
	
	/**
	 * installed action, scroll forward through tabs (scroll button) 
	 * 
	 * @author: Aaron Ceglar 
	 */
	private static class ScrollTabsForwardAction extends AbstractAction{

		public void actionPerformed(ActionEvent e){
			Object src = e.getSource();
			if(src instanceof ScrollableTabButton){
				JTabbedPane pane = (JTabbedPane)((ScrollableTabButton)src).getParent();
				((CloseTabPaneUI)pane.getUI()).tabScroller.scrollForward();
			}
		}
	}

	/**
	 * installed action, scroll backward through tabs (scroll button) 
	 * 
	 * @author: Aaron Ceglar 
	 */
	private static class ScrollTabsBackwardAction extends AbstractAction{

		public void actionPerformed(ActionEvent e){
			Object src = e.getSource();
			if(src instanceof ScrollableTabButton){
				JTabbedPane pane = (JTabbedPane)((ScrollableTabButton)src).getParent();
				((CloseTabPaneUI)pane.getUI()).tabScroller.scrollBackward();
			}
		}
	}

	/**
	 * Create a tab pane layout including scroll components (if required) 
	 *
	 * @author: Aaron Ceglar 
	 */
	private class TabbedPaneScrollLayout extends TabbedPaneLayout{

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicTabbedPaneUI$TabbedPaneLayout#preferredTabAreaHeight(int, int)
		 */
		protected int preferredTabAreaHeight(int tabPlacement, int width){
			return calculateMaxTabHeight(tabPlacement);
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicTabbedPaneUI$TabbedPaneLayout#preferredTabAreaWidth(int, int)
		 */
		protected int preferredTabAreaWidth(int tabPlacement, int height){
			return calculateMaxTabWidth(tabPlacement);
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicTabbedPaneUI$TabbedPaneLayout#layoutContainer(java.awt.Container)
		 */
		public void layoutContainer(Container parent){
			int tabCount = tabPane.getTabCount();
			Insets insets = tabPane.getInsets();
			int selectedIndex = tabPane.getSelectedIndex();

			calculateLayoutInfo();
			if(selectedIndex < 0) return;
			Component selectedComponent = tabPane.getComponentAt(selectedIndex);
			if(selectedComponent != null) setVisibleComponent(selectedComponent);

			int tx, ty, tw, th; // tab area bounds
			int cx, cy, cw, ch; // content area bounds
			Insets cInsets = getContentBorderInsets(SwingConstants.TOP);
			Rectangle bounds = tabPane.getBounds();
			int numChildren = tabPane.getComponentCount();

			if(numChildren > 0){
				// calculate tab area bounds
				tw = bounds.width - insets.left - insets.right;
				th = calculateTabAreaHeight(SwingConstants.TOP, runCount, maxTabHeight);
				tx = insets.left;
				ty = insets.top;

				// calculate content area bounds
				cx = tx + cInsets.left;
				cy = ty + th + cInsets.top;
				cw = bounds.width - insets.left - insets.right - cInsets.left - cInsets.right;
				ch = bounds.height - insets.top - insets.bottom - th - cInsets.top - cInsets.bottom;

				int totalTabWidth = rects[tabCount - 1].x + rects[tabCount - 1].width;

				for(int i = 0; i < numChildren; i++){
					Component child = tabPane.getComponent(i);

					if(child instanceof ScrollableTabViewport){
						Rectangle viewRect = ((JViewport)child).getViewRect();
						int vw = tw;
						if(totalTabWidth > tw){
							vw = Math.max(tw - 36, 36); // Need to allow space for scrollbuttons
							// Scroll at end, ensure scroll offset aligns with a tab
							if(totalTabWidth - viewRect.x <= vw) vw = totalTabWidth - viewRect.x;
						}
						child.setBounds(tx, ty, vw, th);

					}else if(child instanceof ScrollableTabButton){
						ScrollableTabButton scrollbutton = (ScrollableTabButton)child;
						Dimension bsize = scrollbutton.getPreferredSize();
						int bx = 0;
						int by = 0;
						boolean visible = false;

						if(totalTabWidth > tw){
							int dir = scrollbutton.scrollsForward() ? EAST : WEST;
							scrollbutton.setDirection(dir);
							visible = true;
							bx = dir == EAST ? bounds.width - insets.left - bsize.width : bounds.width - insets.left - 2 * bsize.width;
							by = ty + th - bsize.height;
						}
						child.setVisible(visible);
						if(visible)	child.setBounds(bx, by, bsize.width, bsize.height);

					}else{
						child.setBounds(cx, cy, cw, ch);
					}
				}
			}
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicTabbedPaneUI$TabbedPaneLayout#calculateTabRects(int, int)
		 */
		protected void calculateTabRects(int tabPlacement, int tabCount){
			maxTabHeight = calculateMaxTabHeight(tabPlacement);
			runCount = 0;
			selectedRun = -1;
			if(tabCount == 0) return;

			// Run through tabs and lay them out in a single run
			selectedRun = 0;
			runCount = 1;
			Insets tabAreaInsets = getTabAreaInsets(tabPlacement);
			int totalWidth = 0;

			for(int i = 0; i < tabCount; i++){
				Rectangle rect = rects[i];

				if(i > 0){
					rect.x = rects[i - 1].x + rects[i - 1].width - 1;
				}else{
					tabRuns[0] = 0;
					maxTabWidth = 0;
					rect.x = tabAreaInsets.left - 2;
				}

				rect.width = calculateTabWidth(tabPlacement, i, getFontMetrics());
				totalWidth = rect.x + rect.width;
				maxTabWidth = Math.max(maxTabWidth, rect.width);
				rect.y = tabAreaInsets.top;
				rect.height = maxTabHeight;
			}
			tabScroller.tabPanel.setPreferredSize(new Dimension(totalWidth, maxTabHeight));
		}
	}

	
	/**
	 * Construct supporting widgets for scrolling functionality.
	 *  
	 * @author: Aaron Ceglar 
	 */
	private class ScrollableTabSupport implements ChangeListener{
		public ScrollableTabViewport viewport;
		public ScrollableTabPanel tabPanel;
		public ScrollableTabButton scrollForwardButton;
		public ScrollableTabButton scrollBackwardButton;
		public int leadingTabIndex;
		private Point tabViewPosition = new Point(0, 0);

		/**
		 * Instantiate required scroll widgets
		 */
		public ScrollableTabSupport(){
			viewport = new ScrollableTabViewport();
			tabPanel = new ScrollableTabPanel();
			viewport.setView(tabPanel);
			viewport.addChangeListener(this);

			scrollForwardButton = createScrollableTabButton(EAST);
			scrollBackwardButton = createScrollableTabButton(WEST);
		}

		/**
		 * move to next tab if one exists
		 */
		public void scrollForward(){
			Rectangle vRect = viewport.getViewRect();
			if(vRect.width >= viewport.getViewSize().width - vRect.x) return; // no room left to scroll
			setLeadingTabIndex(leadingTabIndex + 1);
		}

		/**
		 * move to previous tab  
		 */
		public void scrollBackward(){
			if(leadingTabIndex == 0)return; // no room left to scroll
			setLeadingTabIndex(leadingTabIndex - 1);
		}

		/**
		 * Adjust first presented tab index, given viewport position
		 * 
		 * @param index : current leading tab index
		 */
		public void setLeadingTabIndex(int index){
			leadingTabIndex = index;
			Dimension viewSize = viewport.getViewSize();
			Rectangle viewRect = viewport.getViewRect();
			tabViewPosition.x = leadingTabIndex == 0 ? 0 : rects[leadingTabIndex].x;

			if((viewSize.width - tabViewPosition.x) < viewRect.width){
				// scroll at end, adjust viewport size to ensure view remains aligned on a tab boundary
				Dimension extentSize = new Dimension(viewSize.width - tabViewPosition.x, viewRect.height);
				viewport.setExtentSize(extentSize);
			}

			viewport.setViewPosition(tabViewPosition);
		}

		/* (non-Javadoc)
		 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
		 */
		public void stateChanged(ChangeEvent e){
			JViewport viewport = (JViewport)e.getSource();
			Rectangle vpRect = viewport.getBounds();
			Rectangle viewRect = viewport.getViewRect();
			int tabCount = tabPane.getTabCount();
			leadingTabIndex = getClosestTab(viewRect.x, viewRect.y);

			if(rects[leadingTabIndex].x < viewRect.x)	leadingTabIndex++;

			tabPane.repaint(vpRect.x, vpRect.y + vpRect.height, vpRect.width, getContentBorderInsets(SwingConstants.TOP).top);
			scrollBackwardButton.setEnabled(viewRect.x > 0);
			scrollForwardButton.setEnabled(leadingTabIndex < tabCount - 1 && viewport.getViewSize().width - viewRect.x > viewRect.width);
		}
	}

	/**
	 * Used to control the method of scrolling the viewport contents.
	 * BLIT_SCROLL_MODE : Use graphics.copyArea to implement scrolling.  
	 * @author: Aaron Ceglar 
	 */
	private class ScrollableTabViewport extends JViewport implements UIResource{
		public ScrollableTabViewport(){
			super();
			setScrollMode(BLIT_SCROLL_MODE);
		}
	}

	/**
	 * Paint the tab area upon call to paint component 
	 * 
	 * @author: Aaron Ceglar 
	 */
	private class ScrollableTabPanel extends JPanel implements UIResource{
		public ScrollableTabPanel(){
			setLayout(null);
		}

		public void paintComponent(Graphics g){
			super.paintComponent(g);
			CloseTabPaneUI.this.paintTabArea(g, tabPane.getTabPlacement(), tabPane.getSelectedIndex());
		}
	}

	/**
	 * Paint the scroll buttons 
	 *
	 * @author: Aaron Ceglar 
	 */
	protected class ScrollableTabButton extends BasicArrowButton implements	UIResource, SwingConstants{
		public ScrollableTabButton(int direction){
			super(direction, UIManager.getColor("TabbedPane.selected"), UIManager.getColor("TabbedPane.shadow"), UIManager.getColor("TabbedPane.darkShadow"), UIManager.getColor("TabbedPane.highlight"));
			setRolloverEnabled(true);
		}

		/**
		 * Can the tab pane scroll forward.
         * @return t/f
		 */
		public boolean scrollsForward(){
			return direction == EAST || direction == SOUTH;
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicArrowButton#getPreferredSize()
		 */
		public Dimension getPreferredSize(){
			return new Dimension(16, calculateMaxTabHeight(0));
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicArrowButton#paint(java.awt.Graphics)
		 */
		public void paint(Graphics g){
			int size;
			int w = getSize().width;
			int h = getSize().height;
			Color origColor = g.getColor();
			boolean isPressed = getModel().isPressed();
			boolean isRollOver = getModel().isRollover();
			boolean isEnabled = isEnabled();

			g.setColor(getBackground());
			g.fillRect(0, 0, w, h);

			g.setColor(shadow);
			// Using the background color set above
			if(direction == WEST){
				g.drawLine(0, 0, 0, h - 1); // left
				g.drawLine(w - 1, 0, w - 1, 0); // right
			}else{
				g.drawLine(w - 2, h - 1, w - 2, 0); // right
			}
			g.drawLine(0, 0, w - 2, 0); // top

			if(isRollOver){ // do highlights or shadows
				Color color1;
				Color color2;
				if(isPressed){
					color2 = Color.white;
					color1 = shadow;
				}else{
					color1 = Color.white;
					color2 = shadow;
				}

				g.setColor(color1);

				if(direction == WEST){
					g.drawLine(1, 1, 1, h - 1); // left
					g.drawLine(1, 1, w - 2, 1); // top
					g.setColor(color2);
					g.drawLine(w - 1, h - 1, w - 1, 1); // right
				}else{
					g.drawLine(0, 1, 0, h - 1);
					g.drawLine(0, 1, w - 3, 1); // top
					g.setColor(color2);
					g.drawLine(w - 3, h - 1, w - 3, 1); // right
				}
			}
			
			// If there's no room to draw arrow, bail
			if(h < 5 || w < 5){
				g.setColor(origColor);
				return;
			}

			if(isPressed)g.translate(1, 1);

			// Draw the arrow
			size = Math.min((h - 4) / 3, (w - 4) / 3);
			size = Math.max(size, 2);
			paintTriangle(g, (w - size) / 2, (h - size) / 2, size, direction, isEnabled);

			// Reset the Graphics back to it's original settings
			if(isPressed) g.translate(-1, -1);
			g.setColor(origColor);
		}
	}

	/**
	 * Ensure that the selected tab is always visible within the viewport.
	 * 
	 * @author: Aaron Ceglar 
	 */
	public class TabSelectionHandler implements ChangeListener{
		public void stateChanged(ChangeEvent e){
			JTabbedPane tabPane = (JTabbedPane)e.getSource();
			tabPane.revalidate();
			tabPane.repaint();

			if(tabPane.getTabLayoutPolicy() == JTabbedPane.SCROLL_TAB_LAYOUT){
				int index = tabPane.getSelectedIndex();
				if(index < rects.length && index != -1){
					tabScroller.tabPanel.scrollRectToVisible(rects[index]);
				}
			}
		}
	}



	/**
	 * Control the state of the close widget during mouse movement. 
	 * 
	 * @author: Aaron Ceglar 
	 */
	class CloseTabPaneMouseHandler extends MouseHandler{
		public CloseTabPaneMouseHandler(){
			super();
		}

		public void mousePressed(MouseEvent e){
			if(closeWidgetStatus == OVER){
				closeWidgetStatus = PRESSED;
				tabScroller.tabPanel.repaint();
				return;
			}
		}

		public void mouseClicked(MouseEvent e){
			super.mousePressed(e);
		}

		public void mouseReleased(MouseEvent e){
			if(overTabIndex == -1) return;
			if(closeWidgetStatus == ACTIVE){
				tabScroller.tabPanel.repaint();
				if(overTabIndex > 0) tabPane.remove(overTabIndex);
			}
		}

		public void mouseExited(MouseEvent e){
			overTabIndex = -1;
			tabScroller.tabPanel.repaint();
		}
	}

	/**
	 * Are comments really necessary here ?
	 * 
	 * @author: Aaron Ceglar 
	 */
	class MyMouseMotionListener implements MouseMotionListener{

		public void mouseMoved(MouseEvent e){
			setTabIcons(e.getX(), e.getY());
		}

		public void mouseDragged(MouseEvent e){
		}
	}

}