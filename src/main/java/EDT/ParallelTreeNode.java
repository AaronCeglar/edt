package EDT;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.TreeSet;

import javax.swing.tree.TreeNode;

/**
 * The base component of the parallel episode tree.
 * All parallel episodes are stored within this structure.
 * Consists of methods for adding and maintinence.
 * 
 * @author: Aaron Ceglar 
 */
public class ParallelTreeNode extends AbstractTreeNode{
	protected int symbol = 0;   // event identifier

	/**
	 * Construct new ParallelTreeNode with symbol
	 * 
	 * @param sym symbol
	 * @calledBy AnalysisEngine 
	 */
	public ParallelTreeNode(int sym){
		super();
		symbol = sym;
		if(sym == -1) valid = true; // root node
	}

	/**
	 * Construct new ParallelTreeNode with symbol and inital event instance
	 * 
	 * @param sym : symbol
	 * @param window : window that event instance belongs to
	 * @param date : actual date of event
	 * @calledBy AnalysisEngine
	 */
	public ParallelTreeNode(int sym, int window, long date){
		super();
		symbol = sym;
		appendTemporalInstance(window, date);
	}

/*=================Analysis Methods===============*/	

	/**
	 * Create a new episode given this node. 
	 * 
	 * @param sDate : start date
	 * @param eDate : end date
	 * @param node : episode node
	 * @return new Episode instance
	 * @see EDT.AbstractTreeNode#createNewEpisodeData(long, long, EDT.AbstractTreeNode)
	 * @calledBy AbstractTreeNode
	 */
	protected Episode createNewEpisodeData(long s, long e, AbstractTreeNode node){
		return new Episode(s, e, node, ((ParallelTreeNode)node).getPathSymbols());
	}

	/**
	 * Find if a node has a child with the symbol parameter, if so append a new
	 * temporal instance and return true, else return false.   
	 * 
	 * @param symbol
	 * @param window
	 * @param timestamp
	 * @return if the child is found
	 * @calledBy AnalysisEngine
	 */
	public boolean hasChild(int symbol, int window, long timestamp){
		Enumeration enumChildren = children();

		while(enumChildren.hasMoreElements()){
			ParallelTreeNode currentChild = (ParallelTreeNode)enumChildren.nextElement();
			if(currentChild.symbolEquals(symbol)){
				currentChild.appendTemporalInstance(window, timestamp);
				return true;
			}
		}
		return false;
	}

	/**
	 * Add a child to a given node, returning the new node. 
	 * If it already exists then returen the matching node.  
	 * 
	 * @param symbol
	 * @return the relevant node 
	 * @calledBy AnalysisEngine
	 */
	public ParallelTreeNode addChild(int symbol){
		Enumeration enumChildren = children();
		while(enumChildren.hasMoreElements()){
			ParallelTreeNode currentChild = (ParallelTreeNode)enumChildren.nextElement();
			if(currentChild.symbolEquals(symbol)) return currentChild;
		}
		ParallelTreeNode newNode = (new ParallelTreeNode(symbol));
		add(newNode);
		return newNode;
	}
	
	/**
	 * Construct episode hashcode
	 * 
	 * @see java.lang.Object#hashCode()
	 * @calledBy AnalysisEngine
	 */
	public int hashCode(){
		int code = 1;
		for(Integer symbol: getPathSymbols()){
			code *= symbol;
		}
		return code;
	}

	/**
	 * Does this episode contain any elements of its comparator node. 
	 * Don't include root in comparison.
	 * 
	 * @param o comparison object
	 * @return
	 * @calledBy AnalysisEngine
	 */
	public boolean contains(Object o){
		if(!(o instanceof ParallelTreeNode))return false;
	
		TreeNode[] compPath = ((ParallelTreeNode)o).getPath();
		TreeNode[] path = getPath();

		for(int depth = 1; depth < path.length; depth++){
			for(int compDepth = 1; compDepth < compPath.length; compDepth++){
				if(((ParallelTreeNode)path[depth]).symbol == ((ParallelTreeNode)compPath[compDepth]).symbol){
					return true;
				}
			}
		}
		return false;
	}
	
/*=================Presentation Methods===============*/	
	
	/**
	 * Get the details of the parallel episode associated with this node in order to print
	 * it out in a list context. The contents are
	 * 	- occurance count
	 * 	- associated event symbols  : path symbols		
	 * Don't include root.
	 * 
	 * @see EDT.AbstractTreeNode#contentsToPrintList()
	 * @calledBy AnalysisResults ReportTextDetail
	 */
	public Integer[] contentsToPrintList(){
		Integer[] list = new Integer[getLevel() + 1];
		list[0] = count;
		TreeNode[] path = getPath();
		for(int depth = 1; depth < path.length; depth++){
			list[depth] = ((ParallelTreeNode)path[depth]).symbol;
		}
		return list;
	}

	/**
	 * Get the details of the parallel episode associated with this node in order to print
	 * it out in a tree context. The contents are
	 * 	- occurance count
	 * 	- associated event symbol		
	 * 
	 * @see EDT.AbstractTreeNode#contentsToPrintTree()
	 * @calledBy AnalysisResults
	 */
	public Integer[] contentsToPrintTree(){
		Integer[] info = {count, symbol};
		return info;
	}

	/**
	 * Get the set of event identifiers that comprise the associated episode.
	 * 
	 * @see EDT.AbstractTreeNode#getEvents()
	 * @calledBy AnalysisResults
	 */
	public Integer[] getEvents(){
		ArrayList<Integer> list = new ArrayList<Integer>();
		TreeNode[] path = getPath();
		for(int depth = 1; depth < path.length; depth++)
			list.add(((ParallelTreeNode)path[depth]).symbol);
		return list.toArray(new Integer[0]);
	}

	/**
	 * Get the set of artifacts associated with this episode
	 * 
	 * @return artifact set
	 * @calledBy AnalysisResults HybridTreeNode
	 */
	public Integer[] getPathArtifacts(){
		TreeSet<Integer> set = new TreeSet<Integer>();
		//no getting root as first symbol
		
		TreeNode[] path = getPath();
		for(int depth = 1; depth < path.length; depth++){
			for(Integer sym : Store.getEventArtifacts(((ParallelTreeNode)path[depth]).getSymbol())){
				set.add(sym);
			}
		}

		return set.toArray(new Integer[0]);
	}
	
/*=================Other Methods===============*/	
	
	/**
	 * Add a new temporal instance as an episodeInstance (super method) to an existing treeNode.
	 * Since this instance relates to an event an not an existing episode there is no date range
	 * the start and end date parameteres are the same.
	 * 
	 * @param window
	 * @param date
	 */
	private void appendTemporalInstance(int window, long date){
		count++;
		temporalData.add(new EpisodeInstance(date, date, window));
	}

	/**
	 * @return this nodes symbol
	 * @calledBy AbstractTreeNode
	 */
	protected int getSymbol(){
		return symbol;
	}

	/**
	 * @param sym : symbol
	 * @return does this node's sysmbol equal the parameter
	 */
	private boolean symbolEquals(int sym){
		return symbol == sym;
	}

	/**
	 * Print out a textual representation of the ParallelTreeNode 
	 * @see EDT.AbstractTreeNode#toString()
	 */
	public String toString(){
		return "parallel: " + symbol + " (" + count + " : " + valid + " : " + active + ") " + temporalData;
	}

	/**
	 * Get the list of symbols associated with this episode.
	 * Don't include Root.
	 * 
	 * @return
	 * @calledBy AnalysisEngine ReportTextDetail HybridTreeNode
	 */
	public ArrayList<Integer> getPathSymbols(){
		ArrayList<Integer> al = new ArrayList<Integer>();
		TreeNode[] path = getPath();
		for(int depth = 1; depth < path.length; depth++){
			ParallelTreeNode node = (ParallelTreeNode)path[depth];
			al.add(node.getSymbol());
		}
		return al;
	}

	/**
	 * @param node
     * @return equivalency
	 * @calledBy Episode HybridTreeNode
	 */
	public boolean equals(ParallelTreeNode node){
		if(this == node) return true;
		return false;
	}
}
