package EDT;

import java.util.*;

/**
 *  An event is the base unit of data upon which analysis is based. An event consists of a set
 *  of participants and a list of timestamps at which it occurred. 
 *  
 *  Each event has an identifier and a counter which relates to its number of occurances.
 *  
 *  Furthermore an event (depending upon user requirements) may require the set of participants
 *  to be ordered. tHerefore these are stored in a TreeSet structure which by default has a 
 *  constructor that overrides the natural ordering to ensure that the participants remain
 *  in the order specified by the dataset unless otherwise specified (ordered)  
 * 
 * @author: Aaron Ceglar 
 *
 */
public class Event{
	private int counter = 1; // counter
	private ArrayList<Integer> alRawIndexes = null; // List of associated raw entries 
	private int id = -1; // unique event identifier
	private TreeSet<Integer> tsActors = null; // actors (sorted according to comparator)
	private boolean isOrdered = false;

	
	/**
	 * default constructor (no ordering of the actors).
	 * 
	 * @calledBy InitialiEngine
	 */
	public Event(){
		alRawIndexes = new ArrayList<Integer>();
		tsActors = new TreeSet<Integer>(new Comparator<Integer>(){
			public int compare(Integer a, Integer b){
				if(a == b) return 0;
				return 1;
			}
		});
	}
	
	/**
	 * default constructor (no ordering of the actors), with raw index
	 * 
	 * @calledBy InitialiseEngine
	 */
	public Event(int rawIndex){
		alRawIndexes = new ArrayList<Integer>();
		alRawIndexes.add(rawIndex);
		tsActors = new TreeSet<Integer>(new Comparator<Integer>(){
			public int compare(Integer a, Integer b){
				if(a == b) return 0;
				return 1;
			}
		});
	}

	/**
	 * Create a sorted actor event
	 * 
	 * @param unsortedSet: set of actor identifiers 
	 * @param eventId  
	 * @param timeStamp
	 * @calledBy Store
	 */
	public Event(Collection<Integer> unsortedSet, int eventId, int timeStamp){
		alRawIndexes = new ArrayList<Integer>();
		alRawIndexes.add(timeStamp);
		isOrdered = true;
		tsActors = new TreeSet<Integer>(unsortedSet);
		id = eventId;
	}

	/**
	 * @return number of times event occurs
	 * @calledBy AbstractTreeNode
	 */
	public int getCount(){return counter;}

	/**
	 * @return get array of actors
	 * @calledBy AnalysisResults
	 * @calledBy ReportTextDetail
	 * @calledBy Store
	 */
	public Integer[] getActorArray(){return tsActors.toArray(new Integer[0]);}

	/**
	 * @calledBy Store
	 * @return the set of actors
	 */
	public TreeSet<Integer> getActors(){return tsActors;}
	
	/**
	 * @param id : the actor uniquer identifier
	 * @calledBy InitialiseEngine
	 */
	public void addActor(Integer id){tsActors.add(id);}

	/**
	 * @param i  eventIdentifier
	 * @calledBy InitialiseEngine
	 */
	public void setId(int i){id = i;}

	/**
	 * @calledBy InitialiseEngine
	 * @calledBy Store
	 * 
	 * @return event Identifier
	 */
	public int getId(){return id;}

	/**
	 * @calledBy Store
	 * @calledBy ReportEventVisualisation
	 * 
	 * @return list of raw Indexes
	 */
	public ArrayList<Integer> getRawIndexes(){return alRawIndexes;}

	/** 
	 * Compare if two events refer to the same event object.
	 * This is called in InitialiseEngine by the comparator within EventMap (HashMap)
	 * Therefore if they are the same use this same method to add this new events details
	 * to  the existing event (e).
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @calledBy InitialiseEngine
	 */
	public boolean equals(Object o){
		if(!(o instanceof Event))return false;
		Event e = (Event)o;
		if(e.isOrdered != isOrdered) return false;
		
		boolean found = tsActors.equals(e.tsActors);
		if(found){
			e.counter = e.counter + counter;
			e.alRawIndexes.addAll(alRawIndexes);
		}
		return found;
	}
	

	/**
	 * Prints representation of the event
	 *  - id
	 *  - counter
	 *  - is ordered
	 *  - list of actors
	 *  - list of associated raw indexes
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		StringBuffer sb = new StringBuffer(" " + id + " (" + counter + ")");
		if(isOrdered) sb.append("(ordered) [");

		Iterator iter = tsActors.iterator();
		while(iter.hasNext()){
			sb.append(Store.getArtifactSymbol((Integer)iter.next()));
			if(iter.hasNext())sb.append(", ");
		}
		
		sb.append("{");

		for(int i = 0; i < alRawIndexes.size(); i++){
			sb.append(alRawIndexes.get(i));
			if(i < alRawIndexes.size() - 1)	sb.append(",");
		}
		sb.append("}]");
		return sb.toString();
	}

	/**
	 * Generate hash code for event, used in event Map. 
	 * 
	 * @see java.lang.Object#hashCode()
	 * @calledBy InitialiseEngine
	 * 
	 */
	public int hashCode(){
		StringBuffer sb = new StringBuffer();
		Iterator iter = tsActors.iterator();
		while(iter.hasNext()){
			sb.append(iter.next().toString() + ":");
		}
		return sb.toString().hashCode();
	}

}
