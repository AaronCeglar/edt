package EDT;

/**
 * An episode is comprised of multiple events occuring within close proximity.
 * An Episode represents a distillation of knowledge from the tree structures from which they are created.
 * Each episode therefore consists of a link to the tree node from which it was created, the date range 
 * over which it occurs and the events which participate within it.   
 *
 * @author: Aaron Ceglar 
 */
public class Episode extends Object{
	private long start = 0;
	private long end = 0;
	private Object symbol = null;
	private AbstractTreeNode node = null;

	/**
	 * Constructor.
	 * 
	 * @param sDate
	 * @param eDate
	 * @param p
	 * @param sym
	 * @calledBy AbstractTreeNode HybridTreeNode ParallelTreeNode
	 */
	public Episode(long sDate, long eDate, AbstractTreeNode p, Object sym){
		start = sDate;
		end = eDate;
		symbol = sym;
		node = p;
	}

	/**
	 * @return the node associated with this episode  
	 * @calledBy AnalysisEngine
	 */
	public AbstractTreeNode getNode(){
		return node;
	}

	/**
	 * @return the endDate associated with this episode
	 * @calledBy AnalysisEngine
	 */
	public long getEnd(){
		return end;
	}

	/**
	 * @return the startDate associated with this episode
	 * @calledBy AnalysisEngine
	 */
	public long getStart(){
		return start;
	}


	/**
	 * @return the symbol associated with this episode
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return symbol.toString();
	}
	
	/**
	 * @param o : the comparison object
	 * @return true if this episode contains the object, else false
	 * @calledBy 
	 *//*
	public boolean contains(Object o){
		if(node instanceof ParallelTreeNode){
			if(!(o instanceof Integer))	return false;
			for(TreeNode n : node.getPath()){
				if(((ParallelTreeNode)n).symbol ==(Integer)o) return true;
			}
			return false;
		}else{ // hybrid
			if(!(o instanceof ParallelTreeNode))return false;
			for(TreeNode n : node.getPath()){
				if(((HybridTreeNode)n).getSymbol().containsCommonEvent(o)) return true;
			}
			return false;
		}
	}*/

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @return equivalency
	 * @calledBy AnalysisEngine AnalysisResults ReportEventVisualisation
	 */
	public boolean equals(Object o){
		if(o == this) return true;
		if(!(o instanceof Episode)) return false;

		Episode ed = (Episode)o;
		if(ed.node.getDepth() != node.getDepth()) return false;

		if(node instanceof HybridTreeNode){
			if(((ParallelTreeNode)symbol).equals((ParallelTreeNode)ed.symbol))	return true;
			if(((HybridTreeNode)node).equals((HybridTreeNode)ed.node)) return true;
		}else{ // instance of parallel tree node
			if((Integer)symbol == (Integer)ed.symbol) return true;
			if(((ParallelTreeNode)node).equals((ParallelTreeNode)ed.node))	return true;
		}
		return false;
	}
}
