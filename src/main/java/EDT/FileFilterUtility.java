package EDT;

import java.io.File;
import java.util.Hashtable;
import java.util.Enumeration;
import javax.swing.filechooser.*;

/**
 * Enables the user specification of filters upon file extension for use in FileChoosers  
 * 
 * @calledBy AnalysisResults, InitialiseEngine, InputSourceDialog
 * @calledBy ReportEventVisualisation, ReportTextDetail
 * 
 * @author: Denise DeVries 
 */
public class FileFilterUtility extends FileFilter {
	private Hashtable<String,FileFilterUtility> filters = null;
	private String description = null;
	private String fullDescription = null;
	private boolean useExtensionsInDescription = true;

	/**
	 * Creates a file filter. If no filters are added, then all files are accepted.
	 * 
	 * @calledBy ReportEventVisualisation
	 */
	public FileFilterUtility() {
		this.filters = new Hashtable<String,FileFilterUtility>();
	}

	/**
	 * Creates a file filter that accepts the given file type.
	 * Example: new ExampleFileFilter("jpg", "JPEG Image Images");
	 *
	 * Note that the "." before the extension is not needed. If
	 * provided, it will be ignored.
	 * 
	 * @calledBy AnalysisResults, InitialiseEngine, ReportEventVisualisation, ReportTextDetail
	 */
	public FileFilterUtility(String extension, String description) {
		this();
		if(extension!=null) addExtension(extension);
		if(description!=null) setDescription(description);
	}

	/**
	 * Creates a file filter from the given string array and description.
	 * Example: new ExampleFileFilter(String {"gif", "jpg"}, "Gif and JPG Images");
	 */
	public FileFilterUtility(String[] filters, String description) {
		this();
		for(String extension: filters) addExtension(extension);
		if(description!=null) setDescription(description);
	}

	/**
	 * Return true if this file should be shown in the directory pane, false if it shouldn't.
	 */
	public boolean accept(File f) {
		if(f != null) {
			if(f.isDirectory())	return true;
			if(getExtension(f) != null && filters.get(getExtension(f)) != null) return true;
		}
		return false;
	}

	/**
	 * Return the extension portion of the file's name.
	 */
	private String getExtension(File f) {
		if(f != null) {
			String filename = f.getName();
			int i = filename.lastIndexOf('.');
			if(i>0 && i<filename.length()-1) {
				return filename.substring(i+1).toLowerCase();
			}
		}
		return null;
	}

	/**
	 * Adds a filetype "dot" extension to filter against.
	 *
	 * For example: the following code will create a filter that filters
	 * out all files except those that end in ".jpg" and ".tif":
	 *
	 *   ExampleFileFilter filter = new ExampleFileFilter();
	 *   filter.addExtension("jpg");
	 *   filter.addExtension("tif");
	 *
	 * Note that the "." before the extension is not needed and will be ignored.
	 * 
	 * @calledBy ReportEventVisualisation
	 */
	public void addExtension(String extension) {
		if(filters == null) {
			filters = new Hashtable<String,FileFilterUtility>(5);
		}
		filters.put(extension.toLowerCase(), this);
		fullDescription = null;
	}


	/**
	 * Returns the human readable description of this filter. For
	 * example: "JPEG and GIF Image Files (*.jpg, *.gif)"
	 *
	 * @see setDescription
	 * @see setExtensionListInDescription
	 * @see isExtensionListInDescription
	 * @see FileFilter#getDescription
	 */
	public String getDescription() {
		if(fullDescription == null) {
			if(description == null || isExtensionListInDescription()) {
				fullDescription = description==null ? "(" : description + " (";
				// build the description from the extension list
				Enumeration extensions = filters.keys();
				if(extensions != null) {
					fullDescription += "." + (String) extensions.nextElement();
					while (extensions.hasMoreElements()) {
						fullDescription += ", ." + (String) extensions.nextElement();
					}
				}
				fullDescription += ")";
			} else {
				fullDescription = description;
			}
		}
		return fullDescription;
	}

	/**
	 * Sets the human readable description of this filter. For
	 * example: filter.setDescription("Gif and JPG Images");
	 * 
	 * @calledBy ReportEventVisualisation
	 */
	public void setDescription(String description) {
		this.description = description;
		fullDescription = null;
	}

	/**
	 * Determines whether the extension list (.jpg, .gif, etc) should
	 * show up in the human readable description.
	 *
	 * Only relevent if a description was provided in the constructor
	 * or using setDescription();
	 */
	public void setExtensionListInDescription(boolean b) {
		useExtensionsInDescription = b;
		fullDescription = null;
	}

	/**
	 * Returns whether the extension list (.jpg, .gif, etc) should
	 * show up in the human readable description.
	 *
	 * Only relevent if a description was provided in the constructor
	 * or using setDescription();
	 */
	public boolean isExtensionListInDescription() {
		return useExtensionsInDescription;
	}
}
