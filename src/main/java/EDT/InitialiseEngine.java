package EDT;

import java.awt.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.plaf.basic.BasicTableHeaderUI;

// specific classes used to avoid class ambiguities
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.*;

/**
 * The initialise engine presents the raw dataset in a sorted tabular format and prepares 
 * the for analysis by populating the required data structures and offering the user a selection
 * of heuristic parameters.     
 * 
 * @author: Aaron Ceglar
 */
public class InitialiseEngine extends JPanel implements ActionListener{
	//---Control Panel widgets-----
	private JPanel pControl = null;

	private JSpinner spSupport = null;  // heuristic filter

	private JSpinner spTreeDepth = null;  // analysis extent constraint
	
	private JSpinner spLift = null; // heuristic filter
	private JRadioButton rbLift = null;
	
	private JComboBox cbGranularity = null; // analysis window extent: granularity 
	private JSpinner spPeriod = null; // analysis window extent: quantity (of granularity)
	
	private JRadioButton rbActorOrder = null;  // order by actors within an event
	private JRadioButton rbWindowOrder = null; // order the events within a window

	private JScrollPane spFilterActor = null;
	private JList liFilterActor = null; // filter by specific actors (inclusion)
	private DefaultListModel dlmFilterActor = null;
	
	private JButton bAnalyse = null; // trigger analysis
	
	//---- -Raw Data widgets---------
	private JTable tblRawData = null;  // presentation of raw data (first 1000 records only)
	private DefaultTableModel dtmRawData = null;

	//------Data Structures------------
	private int rawCount = 0; // numjber of raw records
	private int maxRawFeilds =0;  // number of raw feilds, used for raw table layout 
	private ArrayList<Record> alRawRecords = null; //facilitate multiple dataset addition
	private Record[] rawRecords = null; // array of raw records in temporal order (Store)

    private Integer actorSymbol = 0; // each actor is mapped to a unique id  
	private Hashtable<String, Integer> hmActorMap = null; // mapping of actor - id
	private ArrayList<String> alActorMap = null; // ordered list of mapping (index)
	
	private int eventCounter = 0; // each event is mapped to a unique id
	private HashMap<Event, Integer> hmEventMap = null; // mapping of actor - id 

    //--------- Tab pane----------------
	private CloseTabPane ctpMain = null;  // closed tab pane
	private int ctpCounter = 1; // tab pane counter 
	

	/**
	 * Initialise the storage structures and then populate them and the display widgets
	 * through a call to build Raw.
	 * 
	 * @calledBy EpisodeDetection
	 */
	public InitialiseEngine(){
		initialiseVariables();
		buildRaw();
	}

	/**
	 *  Append to the existing dataset by not re-initialising the data structures 
	 *  before calling buildRaw. 
	 *  
	 * @calledBy EpisodeDetection
	 */
	public void addDataset(){
		buildRaw();
	}

	/**
	 * Initialise data structures required for analysis.
	 */
	private void initialiseVariables(){
		eventCounter = 0;
		actorSymbol = 0;
		rawCount = 0;
		maxRawFeilds = 0;

		rawRecords = null;
		alRawRecords = new ArrayList<Record>(10000); 
		Store.initialiseInputList();

		hmActorMap = new Hashtable<String, Integer>();
		alActorMap = new ArrayList<String>(1000);

		hmEventMap = new HashMap<Event, Integer>();
	}

	/**
	 * Main population method.
	 * Removes existing widgets and rebuilds with the new raw data.
	 */
	private void buildRaw(){
		removeAll();  // remove existing widgets
			
		// Parse the data source. Throws parse exception if the current date format is invalid.
		try{
			if(Store.getInputType() == Constants.FILE){
				processDataFile();
			}else{
				processDataBase();
			}
		}catch(ParseException p){
			System.out.println("invalid date format selected");
			return;
		}
		populateGlobalDataStores(); // populate Store 
		buildTableRaw(); // build raw display table
		buildGUI(); // build widgets
	}

	
	/**
	 * populate Store. 
	 * 
	 * Three levels of storage
	 *  - raw records: The array of raw records
	 *  - artifactList: Each record has a set of artifacts (actors) the artifact list
	 *  	maintains a mapping of artifact_int -> artifact_string. 
	 * 	-  eventMap: a hashmap of unique events, discretised by participating artifacts
	 * 		each event maintains alist of timestamps at which it occurs.
	 *  - eventList: a sorted array of events (by id/counter) serves as an index. 
	 * 
	 */
	private void populateGlobalDataStores(){
		
		// sort rawRecords into temporal order and store
		rawRecords = alRawRecords.toArray(new Record[0]);
		Arrays.sort(rawRecords, new Comparator<Record>(){
			public int compare(Record a, Record b){
				return a.timeStamp - b.timeStamp > 0 ? 1 :-1;
			}
		});
		Store.setRawRecords(rawRecords);

		Store.setArtifactList(alActorMap.toArray(new String[0]));
		
		// sort event map by eventId and store as list
		Event[] eventList = hmEventMap.keySet().toArray(new Event[0]);
		Arrays.sort(eventList, new Comparator<Event>(){
			public int compare(Event a, Event b){
				return a.getId() - b.getId();
			}
		});

		Store.setEventCounter(eventCounter); // number of events
		Store.setEventMap(hmEventMap);
		Store.setEventList(eventList);
	}

	/**
	 * Construct interface widgets
	 */
	private void buildGUI(){
		pControl = new JPanel();
		pControl.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		pControl.setLayout(gridBag);
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.weightx = 1;
		c.insets = new Insets(3, 3, 3, 3); 

		//------Data source widgets----------
		JLabel lbFile = new JLabel("Input Data");
		lbFile.setForeground(Color.BLUE);
		lbFile.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(lbFile, c);
		pControl.add(lbFile);

		//construct fileName panel, can handle multiple data sources
		JPanel pFileName = buildFileNamePanel();  
		c.anchor = GridBagConstraints.CENTER;
		gridBag.setConstraints(pFileName, c);
		pControl.add(pFileName);

		pControl.add(Store.buildSeparator(c, gridBag));

		//------Heuristic widgets----------		
		JLabel lbHeuristic = new JLabel("Heuristic");
		lbHeuristic.setForeground(Color.BLUE);
		lbHeuristic.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbHeuristic, c);
		pControl.add(lbHeuristic);

		JLabel lbSupport = new JLabel("Support");
		c.anchor = GridBagConstraints.EAST;
		c.gridwidth = 3;
		gridBag.setConstraints(lbSupport, c);
		pControl.add(lbSupport);

		spSupport = new JSpinner(new SpinnerNumberModel(9, 2, 100, 1));
		spSupport.setBorder(BorderFactory.createLineBorder(new Color(120, 150, 190)));
		spSupport.setPreferredSize(new Dimension(40, 22));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(spSupport, c);
		pControl.add(spSupport);

		JLabel lbTreeDepth = new JLabel("Max. Episode Length");
		c.gridwidth = 3;
		gridBag.setConstraints(lbTreeDepth, c);
		pControl.add(lbTreeDepth);

		spTreeDepth = new JSpinner(new SpinnerNumberModel(4, 2, 7, 1));
		spTreeDepth.setBorder(BorderFactory.createLineBorder(new Color(120, 150, 190)));
		spTreeDepth.setPreferredSize(new Dimension(40, 22));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(spTreeDepth, c);
		pControl.add(spTreeDepth);

		JLabel lbLift = new JLabel("Lift");
		c.gridwidth = 3;
		gridBag.setConstraints(lbLift, c);
		pControl.add(lbLift);

		rbLift = new JRadioButton();
		rbLift.addActionListener(this);
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(rbLift, c);
		pControl.add(rbLift);

		spLift = new JSpinner(new SpinnerNumberModel(15.0, 0.4, 50.0, 0.2));
		spLift.setBorder(BorderFactory.createLineBorder(new Color(120, 150, 190)));
		spLift.setPreferredSize(new Dimension(40, 22));
		spLift.setEnabled(false);
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.HORIZONTAL;
		gridBag.setConstraints(spLift, c);
		pControl.add(spLift);

		pControl.add(Store.buildSeparator(c, gridBag));

		//------Temporal extent widgets----------
		JLabel lbWindowExtent = new JLabel("Temporal Extent");
		lbWindowExtent.setForeground(Color.BLUE);
		lbWindowExtent.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbWindowExtent, c);
		pControl.add(lbWindowExtent);

		JLabel lbGranularity = new JLabel("Granularity");
		c.anchor = GridBagConstraints.EAST;
		c.fill = GridBagConstraints.NONE;
		c.gridwidth = 1;
		gridBag.setConstraints(lbGranularity, c);
		pControl.add(lbGranularity);

		cbGranularity = new JComboBox(Constants.tempGranularities);
		cbGranularity.setEditable(false);
		cbGranularity.setSelectedIndex(3);
		cbGranularity.addActionListener(this);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(cbGranularity, c);
		pControl.add(cbGranularity);

		JLabel lbPeriod = new JLabel("Period");
		c.gridwidth = 3;
		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(lbPeriod, c);
		pControl.add(lbPeriod);

		spPeriod = new JSpinner(new SpinnerNumberModel(2, 1, 100, 1));
		spPeriod.setBorder(BorderFactory.createLineBorder(new Color(120, 150, 190)));
		spPeriod.setPreferredSize(new Dimension(20, 22));
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(spPeriod, c);
		pControl.add(spPeriod);

		pControl.add(Store.buildSeparator(c, gridBag));
		
		//------Ordering widgets----------
		JLabel lbOrdering = new JLabel("Ordering");
		lbOrdering.setForeground(Color.BLUE);
		lbOrdering.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbOrdering, c);
		pControl.add(lbOrdering);

		JLabel lbEventOrdering = new JLabel("Event By Actors");
		c.anchor = GridBagConstraints.EAST;
		c.fill = GridBagConstraints.NONE;
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(lbEventOrdering, c);
		pControl.add(lbEventOrdering);

		rbActorOrder = new JRadioButton();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(rbActorOrder, c);
		pControl.add(rbActorOrder);

		JLabel lbWinOrdering = new JLabel("Window By Events");
		c.anchor = GridBagConstraints.EAST;
		c.fill = GridBagConstraints.NONE;
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(lbWinOrdering, c);
		pControl.add(lbWinOrdering);

		rbWindowOrder = new JRadioButton();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(rbWindowOrder, c);
		pControl.add(rbWindowOrder);

		pControl.add(Store.buildSeparator(c, gridBag));

		//------Ordering widgets----------
		JLabel lbFilter = new JLabel("Filter by Actor");
		lbFilter.setForeground(Color.BLUE);
		lbFilter.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbFilter, c);
		pControl.add(lbFilter);

		dlmFilterActor = new DefaultListModel();
		String[] st = Store.getArtifactSymbols();
		dlmFilterActor.addElement("none");
		for(int i = 0; i < st.length; i++){
			dlmFilterActor.addElement(st[i]);
		}
		liFilterActor = new JList(dlmFilterActor);
		liFilterActor.setSelectedIndex(0);

		spFilterActor = new JScrollPane(liFilterActor, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spFilterActor.setPreferredSize(new Dimension(150, 100));

		c.weightx = 0;
		c.anchor = GridBagConstraints.EAST;
		gridBag.setConstraints(spFilterActor, c);
		pControl.add(spFilterActor);

		pControl.add(Store.buildSeparator(c, gridBag));

		//------Analysis widget----------		
		bAnalyse = new JButton("Analyse");
		bAnalyse.addActionListener(this);
		gridBag.setConstraints(bAnalyse, c);
		pControl.add(bAnalyse);

		JPanel pPad = new JPanel();
		c.weighty = 1;
		gridBag.setConstraints(pPad, c);
		pControl.add(pPad);

		//----------------
		ctpMain = new CloseTabPane();
		ctpMain.addTab("Raw Data", buildRawDisplay());

		setLayout(new BorderLayout());
		add(pControl, BorderLayout.WEST);
		add(ctpMain, BorderLayout.CENTER);
	}

	/**
	 * Construct A panel which contains labels of the data sources loaded into EDT.
	 * @return  data source panel.
	 */
	private JPanel buildFileNamePanel(){
		ArrayList<String> names = Store.getInputList();
		JPanel pDataSources = new JPanel(new GridLayout(names.size(), 1));

		for(String name : names){
			JLabel lbSourceName = new JLabel(name.toUpperCase());
			lbSourceName.setFont(new Font("SansSerif", Font.PLAIN, 12));
			pDataSources.add(lbSourceName);
		}
		return pDataSources;
	}

	/**
	 * Construct a panel containing the table of raw data embedded within a scrollpane 
	 * @return  raw table panel
	 */
	private JPanel buildRawDisplay(){
		JPanel pRawData = new JPanel(new BorderLayout());
		pRawData.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		JScrollPane spTable = new JScrollPane(tblRawData);
		spTable.setBackground(Color.WHITE);
		spTable.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		pRawData.add(spTable);
		return pRawData;
	}

	/* 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae){
		// launch analysis engine. Present results as a new tab.
		if(ae.getSource() == bAnalyse){
			// build actor filter.
			int[] filter = liFilterActor.getSelectedIndices();
			if(liFilterActor.getMinSelectionIndex() == 0) filter = null;
			
			double lift = rbLift.isSelected() ? ((Double)spLift.getValue()).doubleValue() : 0.0;

			// launch analysis
			AnalysisEngine aEngine = new AnalysisEngine(((Integer)spSupport.getValue()).intValue(), 
							lift, ((Integer)spTreeDepth.getValue()).intValue(), 
							cbGranularity.getSelectedIndex(), ((Integer)spPeriod.getValue()).intValue(),
							rbActorOrder.isSelected(), rbWindowOrder.isSelected(), filter);

			ctpMain.addTab("Analysis Results " + (ctpCounter++), aEngine.analyseDataset());
			ctpMain.setSelectedIndex(ctpMain.getTabCount() - 1); // make new tab active
			revalidate();
		}
		// toggle the lift metric
		if(ae.getSource() == rbLift){
			spLift.setEnabled(rbLift.isSelected());
		}	
	}

	/**
	 * Print the contents of the active tab.
	 * 
	 * @calledBy Episode Detection
	 */
	public void printActiveTab(){
		if(ctpMain.getSelectedIndex() == 0){
			printRawToFile();
		}else{
			((AnalysisResults)ctpMain.getSelectedComponent()).printToFile();
		}
	}
	
	/**
	 * Construct a raw table report and print to PDF file.
	 */
	private void printRawToFile(){
		//--------Report header & footer-----------
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		Phrase p = new Phrase("Episode Detection Tool Report", new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 16));

		HeaderFooter header = new HeaderFooter(p, false);
		header.setBorder(0);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		HeaderFooter footer = new HeaderFooter(new Phrase("Page "), new Phrase(": (" + sdf.format(cal.getTime()) + ")"));
		footer.setAlignment(Element.ALIGN_CENTER);
		footer.setBorder(0);

		try{
			//----Get the .pdf file name---- 
			JFileChooser fcFile = new JFileChooser(System.getProperty("user.dir"));
			fcFile.setDialogType(JFileChooser.SAVE_DIALOG);
			fcFile.setDialogTitle("Select File to Save as");
			fcFile.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			fcFile.setFileFilter(new FileFilterUtility("pdf", "Adobe PDF Files: "));
			if(fcFile.showOpenDialog(Store.getFrame()) != JFileChooser.APPROVE_OPTION) return;

			//----create PDF writer instance-------
			PdfWriter.getInstance(document, new FileOutputStream(fcFile.getSelectedFile().getPath()));
			document.setHeader(header);
			document.setFooter(footer);
			document.addTitle("Title");
			document.open();

			//-----Construct Report table-----
			Table table = new Table(tblRawData.getColumnCount());
			table.setBorderWidth(2);
			table.setBorderColor(Color.BLACK);
			table.setPadding(2);

			//-----Table headers-------
			com.lowagie.text.Font f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 11);
			for(int i = 0; i < tblRawData.getColumnCount(); i++){
				Cell cell = null;
				if(i == 0){
					cell = new Cell(new Phrase("Time Stamp", f));
				}else{
					cell = new Cell(new Phrase("Actor " + i, f));
				}
				cell.setHeader(true);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setBorderWidthBottom(2);
				table.addCell(cell);
			}
			table.endHeaders();

			//-----Table content------
			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 10);
			Vector vec = dtmRawData.getDataVector();
			for(int i = 0; i < tblRawData.getRowCount(); i++){
				for(int j = 0; j < tblRawData.getColumnCount(); j++){
					table.addCell(new Phrase((String)((Vector)vec.elementAt(i)).elementAt(j), f));
				}
			}
			document.add(table);

		}catch(DocumentException de){
			System.err.println(de.getMessage());
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		document.close();
	}

	/**
	 * Construct the raw table model from the populated data structures.
	 * Handles records of different length by adding empty columns to the end.
	 * 
	 * @param cols Number of columns required in table
	 */
	private void buildTableRaw(){
		dtmRawData = new DefaultTableModel();
		dtmRawData.setColumnCount(maxRawFeilds);
		Object[] row = new String[maxRawFeilds];

		// iterate over raw data, for each record construct a table row 
		for(int i = 0; i < Store.getRawLength(); i++){
			int element = 0;
			row[element++] = Store.getCurrentDateFormat().format(new Date(Store.getRawRecordTimeStamp(i)));
			Integer[] sym = Store.getEventArtifacts(Store.getRawRecordEvent(i));

			for(int k = 0; k < maxRawFeilds - 1; k++){
				if(k < sym.length){
					row[element++] = Store.getArtifactSymbol(sym[k]);
				}else{
					row[element++] = ""; //empty column for that row
				}
			}
			dtmRawData.addRow(row);
			if(i > 999)	break;  // only present the first thousand redcords
		}

		String[] s = new String[maxRawFeilds];
		int actorNumber = 1;
		for(int i = 0; i < maxRawFeilds; i++){
			if(i != Store.getTimeStampColumn() - 1){
				s[i] = "Actor " + actorNumber++;
			}else{
				if(i == Store.getTimeStampColumn() - 1)	s[i] = "TimeStamp";
			}
		}
		
		dtmRawData.setColumnIdentifiers(s);
		tblRawData = new JTable(dtmRawData);
		tblRawData.setBackground(Color.WHITE);
		tblRawData.setEnabled(false);
		tblRawData.getTableHeader().setReorderingAllowed(false);
		tblRawData.getTableHeader().setEnabled(false);

		tblRawData.getTableHeader().setUI(new RawTableHeaderUI());
	}

	/**
	 * Parse the (database) dataset, populate:
	 * 	- artifact map
	 * 	- event map
	 * 	- array of temporally ordered records   
	 * 
	 * @throws ParseException Invalid date format: should never be thrown
	 */
	private void processDataBase() throws ParseException{
		ArrayList<String> dbData = Store.getDbData(); // dataset to parse 
		String[] trans = null; // individual record
		
		// iterate over dataset: populate artifact and event structures
		for(int i = 0; i < dbData.size(); i++){
			trans = dbData.get(i).split(Constants.columnRegex);
			Event event = new Event(rawCount++);

			// add each actor to artifact map 
			for(int j = 0; j < trans.length; j++){
				if(j == Store.getTimeStampColumn() - 1)	continue;

				Integer artifactId = hmActorMap.get(trans[j]);
				if(artifactId == null){
					alActorMap.add(trans[j]);
					artifactId = actorSymbol;
					hmActorMap.put(trans[j], actorSymbol++);
				}
				event.addActor(artifactId);
			}

			// add constructed event to event map.
			Integer currentEventId = hmEventMap.get(event);
			if(currentEventId == null){
				event.setId(eventCounter);
				hmEventMap.put(event, eventCounter);
				currentEventId = eventCounter++;
			}else{
				event.setId(currentEventId);
			}

			// create new record object 
			Date d = Store.getCurrentDateFormat().parse(trans[0]);
			alRawRecords.add(new Record(d.getTime(), currentEventId));
		}
		maxRawFeilds = trans.length;
	}

	/**
	 * Parse the (file) dataset, populate:
	 * 	- artifact map
	 * 	- event map
	 * 	- array of temporally ordered records   
	 * 
	 * @return number of dataset feilds 	
	 * @throws ParseException Invalid date format: should never be thrown
	 */
	private void processDataFile() throws ParseException{
		String line = null;
		String[] trans = null;

		try{
			BufferedReader in = new BufferedReader(new FileReader(Store.getFInput()));
			while((line = in.readLine()) != null){
				trans = line.split(Constants.columnRegex);
				if(trans.length > maxRawFeilds) maxRawFeilds = trans.length;
				
				Event event = new Event(rawCount++);
				for(int j = 0; j < trans.length; j++){
					if(j == Store.getTimeStampColumn() - 1)	continue;
					
					Integer artifactId = hmActorMap.get(trans[j]);
					if(artifactId == null){
						alActorMap.add(trans[j]);
						artifactId = actorSymbol;
						hmActorMap.put(trans[j], actorSymbol++);
					}
					event.addActor(artifactId);
				}
	
				// create event level symbol
				Integer currentEventId = hmEventMap.get(event);
				if(currentEventId == null){
					event.setId(eventCounter);
					hmEventMap.put(event, eventCounter);
					currentEventId = eventCounter++;
				}else{
					event.setId(currentEventId);
				}
				
				//	 create new record object 
				Date d = Store.getCurrentDateFormat().parse(trans[0]);
				alRawRecords.add(new Record(d.getTime(), currentEventId));	
			}
			in.close();
		}catch(FileNotFoundException e){
			System.out.println("error: file not found");
		}catch(IOException e){
			System.out.println("error: error reading file");
		}
	}

/*------------Inner Classes-----------------------*/


	/**
	 * Extended the BasicHeader to fix a inconsistent drawing bug, which resulted in 
	 * the raw table header being painted during mouse over the header co-ordinates
	 * when another (analysis results) tab was active. That being said the behaviour now 
	 * (26-2-08) appears stable when this UI is removed, however it has been left for the
	 * present.
	 * 
	 * extension typically simplifies the super's methods to only the essentials. 
	 * 
	 * @author: Aaron Ceglar 
	 */
	class RawTableHeaderUI extends BasicTableHeaderUI{
		
		public RawTableHeaderUI(){
			super();
		}

		/* 
		 * @see javax.swing.plaf.basic.BasicTableHeaderUI#paint(java.awt.Graphics, javax.swing.JComponent)
		 */
		public void paint(Graphics g, JComponent c){
			if(header.getColumnModel().getColumnCount() <= 0) return;
			
			// set header clip area	
			Rectangle clip = g.getClipBounds();
			int cMin = header.columnAtPoint(clip.getLocation());
			int cMax = header.columnAtPoint(new Point(clip.x + clip.width - 1, clip.y));

			TableColumnModel cm = header.getColumnModel();
			if(cMax == -1) cMax = cm.getColumnCount() - 1;

			int columnWidth;
			Rectangle cellRect = header.getHeaderRect(cMin);
			TableColumn aColumn;

			//paint the column header
			for(int column = cMin; column <= cMax; column++){
				aColumn = cm.getColumn(column);
				columnWidth = aColumn.getWidth();
				cellRect.width = columnWidth;
				paintCell(g, cellRect, column);
				cellRect.x += columnWidth;
			}
			rendererPane.removeAll();
		}

		/**
		 * @param columnIndex index of column header to be rendered
		 * @return component used to draw the header cell
		 */
		private Component getHeaderRenderer(int columnIndex){
			TableColumn aColumn = header.getColumnModel().getColumn(columnIndex);
			TableCellRenderer renderer = new RawHeaderCellRenderer();
			return renderer.getTableCellRendererComponent(header.getTable(), aColumn.getHeaderValue(),
							                              false, false, -1, columnIndex);
		}

		/**
		 * Paint the header cell
		 * 
		 * @param g : Graphics object
		 * @param cr : Header column clip area
		 * @param columnIndex : Index of column to be rendered 
		 */
		private void paintCell(Graphics g, Rectangle cr, int columnIndex){
			rendererPane.paintComponent(g, getHeaderRenderer(columnIndex), 
							            header, cr.x, cr.y, cr.width, cr.height, true);
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicTableHeaderUI#getMinimumSize(javax.swing.JComponent)
		 */
		public Dimension getMinimumSize(JComponent c){
			long width = 0;
			Enumeration enumeration = header.getColumnModel().getColumns();
			while(enumeration.hasMoreElements()){
				TableColumn aColumn = (TableColumn)enumeration.nextElement();
				width = width + aColumn.getMinWidth();
			}
			return createHeaderSize(width);
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicTableHeaderUI#getPreferredSize(javax.swing.JComponent)
		 */
		public Dimension getPreferredSize(JComponent c){
			long width = 0;
			Enumeration enumeration = header.getColumnModel().getColumns();
			while(enumeration.hasMoreElements()){
				TableColumn aColumn = (TableColumn)enumeration.nextElement();
				width = width + aColumn.getPreferredWidth();
			}
			return createHeaderSize(width);
		}

		/* (non-Javadoc)
		 * @see javax.swing.plaf.basic.BasicTableHeaderUI#getMaximumSize(javax.swing.JComponent)
		 */
		public Dimension getMaximumSize(JComponent c){
			long width = 0;
			Enumeration enumeration = header.getColumnModel().getColumns();
			while(enumeration.hasMoreElements()){
				TableColumn aColumn = (TableColumn)enumeration.nextElement();
				width = width + aColumn.getMaxWidth();
			}
			return createHeaderSize(width);
		}

		/**
		 * Force header height of 30
		 * @param width : Of header 
		 * @return the Dimension of the header 
		 */
		private Dimension createHeaderSize(long width){
			if(width > Integer.MAX_VALUE) width = Integer.MAX_VALUE;
			return new Dimension((int)width, 30);
		}
		
		/**
		 * Customise the painting of the raw table header cells
		 */
		class RawHeaderCellRenderer implements TableCellRenderer{

			public RawHeaderCellRenderer(){
				super();
				setOpaque(true);
			}

			public JComponent getTableCellRendererComponent(JTable table,
							Object value, boolean isSelected, boolean hasFocus,
							int row, int column){

				JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
				p.setBackground(Color.WHITE);
				p.setBorder(BorderFactory.createLineBorder(Color.white, 2));
				p.setBackground(new Color(220, 220, 220));

				JLabel lab = new JLabel((String)value);
				lab.setEnabled(true);
				lab.setFont(new Font("SansSerif", Font.PLAIN, 12));
				lab.setForeground(Color.BLUE);
				lab.getInsets(new Insets(5, 5, 5, 5));
				p.add(lab);
				return p;
			}
		}
	}
	

}
