package EDT;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;

import javax.swing.*;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author: Aaron Ceglar 
 *
 */
public class ReportTextDetail extends JFrame implements ActionListener{
	private JPanel pMain = null;
	private AbstractTreeNode node = null;
	private JList lResults = null;
	private JButton btnShow = null;
	private JButton btnPrint = null;
	private JButton btnClose = null;

	private long windowLength = 0;

	private boolean showingAll = true;
	private DefaultListModel dlm = null;

	/**
	 * Constructor.
	 * 
	 * @param tn : episode upon which the presentation is based.
	 * @param wl : window length.
	 * @param oa : are actors ordered.
	 * @calledBy AnalysisResults
	 */
	public ReportTextDetail(AbstractTreeNode tn, long wl){
		// Establish look and feel.
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		java.awt.Image ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/edt.gif"));
		setIconImage((new ImageIcon(ic)).getImage());
		
		node = tn;
		windowLength = wl;
		setTitle("Detail Text Presentation");

		pMain = new JPanel(new BorderLayout(5, 5));
		pMain.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
		pMain.add(buildTitlePanel(), BorderLayout.NORTH);
		pMain.add(buildDisplayPane(), BorderLayout.CENTER);
		pMain.add(buildControlPane(), BorderLayout.SOUTH);

		setContentPane(pMain);
		setBounds(50, 50, 400, 500);
		setMinimumSize(new Dimension(400, 500));
		setResizable(true);
		setVisible(true);
	}

	/**
	 * Constuct the title panel. 
	 * Contains the episode (list format) from which this detail report was called. 
	 * 
	 * @return the constructed panel.
	 */
	private JPanel buildTitlePanel(){
		JPanel pTitle = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pTitle.setBackground(Color.white);
		Integer nodeSymbols[] = node.contentsToPrintList();
		pTitle.add(new JLabel(nodeSymbols[0].toString())); // count

		StringBuffer sb = new StringBuffer();
		for(int j = 1; j < nodeSymbols.length; j++){
			if(j != 1) sb.append("  ");
			if(nodeSymbols[j] == -1){
				// Hybrid episode sequence separator, add constructed string and clear buffer. 
				pTitle.add(new JLabel(sb.toString()));
				sb = new StringBuffer();
				java.awt.Image ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/rightArrow.gif"));
				pTitle.add(new JLabel(new ImageIcon(ic)));
			}else{
				// Get the artifact symbols associated with the event(node)symbol.
				Integer[] tr = Store.getEvent(nodeSymbols[j]).getActorArray();
				sb.append("[");
				for(int h = 0; h < tr.length; h++){
					if(h != 0) sb.append(", ");
					sb.append(Store.getArtifactSymbols()[tr[h]]);
				}
				sb.append("]");
			}
		}
		pTitle.add(new JLabel(sb.toString()));
		return pTitle;
	}

	/**
	 * Construct the Control panel.
	 * 	- Show targets : toggles display between all window contents and only those specified within the episode.
	 * 	- Print : print the detailed report to Pdf.
	 *  - Close : close the presentation.
	 * 
	 * @return the constructed panel.
	 */
	private JPanel buildControlPane(){
		JPanel pControl = new JPanel();

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		pControl.setLayout(gridBag);
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(5, 5, 5, 5); 
		c.gridwidth = 1;

		btnShow = new JButton("Show Targets");
		btnShow.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnShow.addActionListener(this);
		gridBag.setConstraints(btnShow, c);
		pControl.add(btnShow);

		btnPrint = new JButton("Print");
		btnPrint.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnPrint.addActionListener(this);
		gridBag.setConstraints(btnPrint, c);
		pControl.add(btnPrint);

		JPanel pPad = new JPanel();
		c.weightx = 1;
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(pPad, c);
		pControl.add(pPad);

		btnClose = new JButton("Close");
		btnClose.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnClose.addActionListener(this);
		c.anchor = GridBagConstraints.EAST;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(btnClose, c);
		pControl.add(btnClose);

		return pControl;
	}

	/**
	 * Construct the detail panel,, which consists of a list within a scrollPane.
	 * 
	 * @return the constructed panel. 
	 */
	private JScrollPane buildDisplayPane(){
		dlm = new DefaultListModel();
		if(node instanceof ParallelTreeNode){
			buildParallelTextDetails(dlm);
		}else{
			buildHybridTextDetails(dlm);
		}

		lResults = new JList(dlm);
		lResults.setBackground(Color.WHITE);
		lResults.setEnabled(true);
		lResults.setCellRenderer(new tdrCellRenderer());

		JScrollPane spList = new JScrollPane(lResults, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spList.setBackground(Color.WHITE);
		spList.setBorder(BorderFactory.createLoweredBevelBorder());
		return spList;
	}

	/**
	 * Construct the parallel episode details to be presented, through population of the model parameter.
	 * For each temporal event associated with an episode, display the raw records that fall within the pertinent 
	 * analysis window. The raw records that participate within the episode are coloured to distinguish them.  
	 * 
	 * @param dlm : the constructed model.
	 */
	private void buildParallelTextDetails(DefaultListModel dlm){
		// Get list of timestamp ids associated with the events in a pattern.
		ColourTreeSet episodeEvents = new ColourTreeSet(); // set of colours used within presentation
		ArrayList<Integer> path = ((ParallelTreeNode)node).getPathSymbols();
		for(int i = 0; i < path.size(); i++){
			for(Integer event: Store.getEventTimeStamps(path.get(i))){
				episodeEvents.add(new EventColour(event, i + 1)); 
			}	
		}

		// Get the temporal data associated with a episode (node). This provides window information.
		long[][] temporalData = node.getTemporalData();

		// For each instance of temporal data
		for(int index = 0; index < temporalData.length; index++){
			int rawIndex = (int)temporalData[index][0];
			long timeStamp = Store.getRawRecordTimeStamp(rawIndex);

			// Get first event for start of window date.
			while(rawIndex > 0 && Store.getRawRecordTimeStamp(rawIndex - 1) == timeStamp){
				rawIndex--;
				if(rawIndex == 0) break;
			}

			// Add new window title to presentation data model.
			dlm.addElement("Window: " + rawIndex + " (" + Store.getCurrentDateFormat().format(new Date(temporalData[index][1])) +
						   ") (" + Store.getCurrentDateFormat().format(new Date(timeStamp + windowLength)) + ")\n");
						
			// Iterate over the raw records within the valid temporal window.
			for(; rawIndex < Store.getRawLength(); rawIndex++){
				// If raw element outside valid temporal window then get next window. 
				if(Store.getRawRecordTimeStamp(rawIndex) > (timeStamp + windowLength)) break;

				// See if the raw record "event" indexed participates in the episode. If so then colour it accordingly.
				// If not "showing all" then only those raw events participating within the episode are displayed. 
				EventColour ce = episodeEvents.contains(rawIndex);
				if(showingAll){
					dlm.addElement(ce);
				}else{
					if(ce.colourIndex != 0) dlm.addElement(ce);
				}
			}
		}
	}

	/**
	 * Construct the parallel episode details to be presented, through population of the model parameter.
	 * For each temporal event associated with an episode, display the raw records that fall within the pertinent 
	 * analysis window. The raw records that participate within the episode are coloured to distinguish them.  
	 * 
	 * @param dlm : the constructed model.
	 */
	private void buildHybridTextDetails(DefaultListModel dlm){
		// Get list of timestamp ids associated with the events in a pattern.
		ColourTreeSet episodeEvents = new ColourTreeSet();
		ArrayList<ParallelTreeNode> path = ((HybridTreeNode)node).getPathSymbols();
		for(int i = 0; i < path.size(); i++){
			for(Integer node: path.get(i).getPathSymbols()){
				for(Integer event : Store.getEventTimeStamps(node)){
					episodeEvents.add(new EventColour(event, i + 1)); 
				}
			}
		}
		
		// Get the temporal data associated with a episode (node). This provides window information.
		long[][] temporalData = node.getTemporalData();
		
		// For each instance of temporal data.
		for(int index = 0; index < temporalData.length; index++){
			int rawIndex = (int)temporalData[index][0];
			long timeStamp = Store.getRawRecordTimeStamp(rawIndex);
			
			// Get first event for start of window date.
			while(Store.getRawRecordTimeStamp(rawIndex - 1) == timeStamp){
				rawIndex--;
				if(rawIndex == 0) break;
			}
			// Add new window title to presentation data model.
			dlm.addElement("Window: " + rawIndex + " (" + Store.getCurrentDateFormat().format(new Date(temporalData[index][1])) + 
							") (" + Store.getCurrentDateFormat().format(new Date(timeStamp + windowLength)) + ")\n");

			// Iterate over the raw records within the valid temporal window.
			for(; rawIndex < Store.getRawLength(); rawIndex++){
				// If raw element outside valid temporal window then get next window. 
				if(Store.getRawRecordTimeStamp(rawIndex) > timeStamp + windowLength) break;
				
				// See if the raw record "event" indexed participates in the episode. If so then colour it accordingly.
				// If not "showing all" then only those raw events participating within the episode are displayed. 
				EventColour ce = episodeEvents.contains(rawIndex);
				if(showingAll){
					dlm.addElement(ce);
				}else{
					if(ce.colourIndex != 0)
						dlm.addElement(ce);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae){
		// Toggle between showing all events and only those pertaining to the episode.
		// Causes rebuild of main presentation panel.
		if(ae.getSource() == btnShow){
			if(showingAll){
				btnShow.setText("Show All");
			}else{
				btnShow.setText("Show Targets");
			}
			showingAll = !showingAll;
			dlm.clear();
			if(node instanceof ParallelTreeNode){
				buildParallelTextDetails(dlm);
			}else{
				buildHybridTextDetails(dlm);
			}
			pMain.revalidate();

		}
		//Print report contents
		if(ae.getSource() == btnPrint)
			printToFile();

		// Close the report.
		if(ae.getSource() == btnClose){
			this.dispose();
		}
	}

	/**
	 * Print the report to a *.pdf file.
	 */
	private void printToFile(){
		// Construct document object.
		Document document = new Document(PageSize.A4, 20, 20, 50, 50);
		Phrase p = new Phrase("Episode Detection Tool Detail Report", new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 16));

		HeaderFooter header = new HeaderFooter(p, false);
		header.setBorder(0);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		HeaderFooter footer = new HeaderFooter(new Phrase("Page "), new Phrase(": (" + sdf.format(cal.getTime()) + ")"));
		footer.setAlignment(Element.ALIGN_CENTER);
		footer.setBorder(0);
		
		// Instigate file chooser to select file name to save to. 
		try{
			JFileChooser fcFile = new JFileChooser(System.getProperty("user.dir"));
			fcFile.setDialogType(JFileChooser.SAVE_DIALOG);
			fcFile.setDialogTitle("Select File to Save as");
			fcFile.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			fcFile.setFileFilter(new FileFilterUtility("pdf", "Adobe PDF Files: "));

			if(fcFile.showOpenDialog(Store.getFrame()) != JFileChooser.APPROVE_OPTION)return;

			PdfWriter.getInstance(document, new FileOutputStream(fcFile.getSelectedFile().getPath()));
			document.setHeader(header);
			document.setFooter(footer);
			document.addTitle("Title");
			document.open();

			// Print the title out.
			Integer nodeSymbols[] = node.contentsToPrintList();
			com.lowagie.text.Font f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 12, com.lowagie.text.Font.BOLD);
			Phrase ph = new Phrase("Episode:", f);

			for(int j = 1; j < nodeSymbols.length; j++){
				if(nodeSymbols[j] == -1){
					try{
						String s = System.getProperty("user.dir") + "\\images\\rightArrow.gif";
						ph.add(new Chunk(Image.getInstance(s), 5, -3));
						ph.add(new Phrase("  "));
					}catch(IOException e){
						System.out.println("TextDetailReport: PrintToFile : Image \"images\rightArrow.gif\" not found error");
						
					}
				}else{
					Integer[] tr = Store.getEvent(nodeSymbols[j]).getActorArray();
					StringBuffer sb = new StringBuffer(" [");
					for(int h = 0; h < tr.length; h++){
						if(h != 0)
							sb.append(", ");
						sb.append(Store.getArtifactSymbols()[tr[h]]);
					}
					sb.append("]");
					ph.add(new Chunk(sb.toString()));
				}
			}
			Paragraph para = new Paragraph(ph);
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("Occurance Count : " + nodeSymbols[0].toString(), f));
			para.setSpacingAfter(20);
			document.add(para);

			// Populate details.
			if(node instanceof ParallelTreeNode){
				printParallelTextDetails(document);
			}else{
				printHybridTextDetails(document);
			}
		}catch(DocumentException de){
			System.out.println("TextDetailReport: PrintToFile : Document exception error");
		}catch(FileNotFoundException e){
			System.out.println("TextDetailReport: PrintToFile : File not found error");
			
		}
		document.close();
	}

	/**
	 * @param document : the main document to which these details are appended.
	 * @throws DocumentException : invalid document construction
	 */
	private void printParallelTextDetails(Document document) throws DocumentException{
		// Get list of timestamp ids associated with the events in a pattern.
		ArrayList<Integer> path = ((ParallelTreeNode)node).getPathSymbols();
		ColourTreeSet episodeEvents = new ColourTreeSet();
		for(int i = 0; i < path.size(); i++){
			ArrayList<Integer> ets = Store.getEventTimeStamps(path.get(i));
			for(int j = 0; j < ets.size(); j++)
				episodeEvents.add(new EventColour(ets.get(j), i + 1));
		}

		// For each temporal pattern.
		long[][] temporalData = node.getTemporalData();
		for(int index = 0; index < temporalData.length; index++){
			int rawIndex = (int)temporalData[index][0];
			long timeStamp = Store.getRawRecordTimeStamp(rawIndex);

			// Get first event for start of window date.
			while(rawIndex > 0 && Store.getRawRecordTimeStamp(rawIndex - 1) == timeStamp){
			    rawIndex--;
				if(rawIndex == 0) break;
			}	

			//Construct window title.
			com.lowagie.text.Font f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14);
			Phrase ph = new Phrase("Window: (" + Store.getCurrentDateFormat().format(new Date(temporalData[index][1])) + " ... " + Store.getCurrentDateFormat().format(new Date(timeStamp + windowLength)) + ")", f);

			Paragraph para = new Paragraph(ph);
			para.setSpacingAfter(15);
			para.setIndentationLeft(20);
			para.add(Chunk.NEWLINE);

			// Iterate over window append event as required. 
			for(; rawIndex < Store.getRawLength(); rawIndex++){
				if(Store.getRawRecordTimeStamp(rawIndex) > timeStamp + windowLength) break;
				EventColour ce = episodeEvents.contains(rawIndex);

				if(!showingAll && ce.colourIndex == 0) continue;
				StringBuffer sb = new StringBuffer("  " + Store.getCurrentDateFormat().format(new Date(Store.getRawRecordTimeStamp(ce.index))) + " ");
		
				Integer[] tr = Store.getEventArtifacts(Store.getRawRecordEvent(ce.index));
				for(int h = 0; h < tr.length; h++){
					if(h != 0) sb.append(", ");
					sb.append(Store.getArtifactSymbols()[tr[h]]);
				}

				if(ce.colourIndex != 0){
					f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 11, com.lowagie.text.Font.BOLD);
				}else{
					f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 11, com.lowagie.text.Font.NORMAL, new Color(120, 120, 120));
				}
				para.add(new Phrase(sb.toString(), f));
				para.add(Chunk.NEWLINE);
			}
			document.add(para);
		}
	}

	/**
	 * @param document
	 * @throws DocumentException
	 */
	private void printHybridTextDetails(Document document) throws DocumentException{
		// Get list of timestamp ids associated with the events in a pattern.
		ColourTreeSet episodeEvents = new ColourTreeSet();
		ArrayList<ParallelTreeNode> path = ((HybridTreeNode)node).getPathSymbols();
		for(int i = 0; i < path.size(); i++){
			ArrayList<Integer> node = path.get(i).getPathSymbols();
			for(int j = 0; j < node.size(); j++){
				ArrayList<Integer> event = Store.getEventTimeStamps(node.get(j));
				for(int k = 0; k < event.size(); k++)
					episodeEvents.add(new EventColour(event.get(k), i + 1));
			}
		}
		
		// For each temporal pattern.
		long[][] temporalData = node.getTemporalData();
		for(int index = 0; index < temporalData.length; index++){
			int rawIndex = (int)temporalData[index][0];
			long timeStamp = Store.getRawRecordTimeStamp(rawIndex);
			
			// Get first event for start of window date.
			while(Store.getRawRecordTimeStamp(rawIndex - 1) == timeStamp){
				 rawIndex--;
				if(rawIndex == 0) break;
			}	
			
			//Construct window title.
			com.lowagie.text.Font f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 12, com.lowagie.text.Font.BOLD);
			Phrase ph = new Phrase("Window: (" + Store.getCurrentDateFormat().format(new Date(temporalData[index][1])) + " ... " + Store.getCurrentDateFormat().format(new Date(timeStamp + windowLength)) + ")", f);

			Paragraph para = new Paragraph(ph);
			para.setSpacingAfter(15);
			para.setIndentationLeft(20);
			para.add(Chunk.NEWLINE);
			
			// Iterate over window append event as required. 
			for(; rawIndex < Store.getRawLength(); rawIndex++){
				long ts = Store.getRawRecordTimeStamp(rawIndex);
				if(ts > timeStamp + windowLength) break;

				EventColour ce = episodeEvents.contains(rawIndex);
				if(!showingAll && ce.colourIndex == 0) continue;
				StringBuffer sb = new StringBuffer("  " + Store.getCurrentDateFormat().format(new Date(Store.getRawRecordTimeStamp(ce.index))) + " ");
				Integer[] tr = Store.getEventArtifacts(Store.getRawRecordEvent(ce.index));

				for(int h = 0; h < tr.length; h++){
					if(h != 0) sb.append(", ");
					sb.append(Store.getArtifactSymbols()[tr[h]]);
				}
				if(ce.colourIndex != 0){
					f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 11, com.lowagie.text.Font.BOLD);
				}else{
					f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 11);
				}
				para.add(new Phrase(sb.toString(), f));
				para.add(Chunk.NEWLINE);
			}
			document.add(para);
		}
	}
	
	
//--------------------Inner classes----------------------
	
	/**
	 * Ensures that each unique event within the episode is assigned a different colour.
	 * While no participatory events are coloured black.
	 * 
	 * @author: Aaron Ceglar 
	 */
	class EventColour extends Object{
		protected int index = 0; // event index
		protected int colourIndex = 0; // colour of event

		/**
		 * Constructor.
		 * 
		 * @param i : event index.
		 * @param c : colour assigned.
		 */
		public EventColour(int i, int c){
			index = i;
			colourIndex = c;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString(){
			return index + " " + colourIndex;
		}

		/* (non-Javadoc)
		 * If the event has a colour assigned return true.
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object o){
			if(index == ((EventColour)o).index) return true;
			return false;
		}
	}

	
	/**
	 * Sorts the event colours for quick access based upon event index.
	 * 
	 * @author: Aaron Ceglar 
	 */
	class ColourTreeSet extends TreeSet<EventColour>{

		/**
		 * Constructor.
		 */
		public ColourTreeSet(){
			super(new Comparator<EventColour>(){
				public int compare(EventColour a, EventColour b){
					return a.index - b.index;
				}
			});
		}

		/**
		 * Iterate over the set and return the assigned event colour is one exists else return colour 0 (maps to BLACK, see constant class).   
		 * 
		 * @param j : the raw index of an event.
		 * @return
		 */
		public EventColour contains(int j){
			for(EventColour s: this){
				if(s.index == j) return s;
				if(s.index > j)	break;
			}
			return new EventColour(j, 0);
		}
	}

	
	/**
	 * Customise the rendering of each list component.
	 * 
	 * @author: Aaron Ceglar 
	 */
	class tdrCellRenderer extends JPanel implements ListCellRenderer{
		/**
		 * Constructor. 
		 */
		public tdrCellRenderer(){
			super();
			setOpaque(true);
		}

		/* (non-Javadoc)
		 * Customise the rendering.
		 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
		 */
		public JComponent getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus){

			JPanel present = new JPanel(new FlowLayout(FlowLayout.LEFT));
			present.setBackground(Color.white);

			if(value instanceof String){
				JLabel lString = new JLabel((String)value);
				lString.setFont(new Font("Tacoma", Font.BOLD, 12));
				present.add(lString);
			}else{
				EventColour ce = (EventColour)value;
				present.add(new JLabel("  " + Store.getCurrentDateFormat().format(new Date(Store.getRawRecordTimeStamp(ce.index)))));

				Integer[] tr = Store.getEventArtifacts(Store.getRawRecordEvent(ce.index));
				StringBuffer sb = new StringBuffer();
				for(int h = 0; h < tr.length; h++){
					if(h != 0) sb.append(", ");
					sb.append(Store.getArtifactSymbols()[tr[h]]);
				}

				JLabel lab1 = new JLabel(sb.toString());
				lab1.setForeground(Constants.colourMap[ce.colourIndex]);
				present.add(lab1);
			}
			return present;
		}
	}
}
