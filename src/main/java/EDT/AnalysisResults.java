package EDT;

import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.*;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Generates the analysis report interface from which more detailed reports can be launched.
 * 
 * @author: Aaron Ceglar 
 */
public class AnalysisResults extends JPanel implements ActionListener, ListSelectionListener{

	private JPanel pDisplay = null; // Display of episodes in either a list or tree presentation.
	private JPanel pControl = null; // Presentation of analysis parameters used and widgets for control of result interface.
	private JPanel pFilter = null; // Dynamic panel populated with the current set of presentation filters

	private int granularity = 0; // Temporal granularity.
	private int period = 0; // Temporal periodicy.
	private double lift = 0.0; // Lift heuristic.
	private int support = 0; // Support heuristic.
	private int treeDepth = 0; // Depth of tree construction during analysis.
	private boolean orderActors = false; // Actors ordered for analysis.
	private boolean orderEvents = false; // Events ordered within windows for analysis.
	private int[] artifactFilter = null; // The set of artifact filters.
	private Integer[] reportFilter = null; // The set of report filters.
	private ArrayList<Integer> reportFilterMapping = null; //Contains mapping from actor symbol to actor name. 

	private JScrollPane spHy = null;
	private JScrollPane spPara = null;
	
	private AbstractTreeNode detailNode  = null; //Declare global, as passed from inner class to other class
	
	private ParallelTreeNode dmtnParallelRoot = null; // Root of the parallel episode tree.
	private HybridTreeNode dmtnHybridRoot = null; // Root of the hybrid episode tree.

	private JList liFilterActor = null; // List of potential report filters.
	private JButton bPresentType = null; // Change between presentation type.
	private boolean showSets = true; // Current presentation type (list, tree).

	/**
	 * Constructor.
	 * 
	 * @param s : support heuristic
	 * @param l : lift heuristic
	 * @param d : tree depth constraint
	 * @param g : temporal granularity
	 * @param p : temporal period
	 * @param a : are actorsordered
	 * @param af : actor filter
	 * @param par : parallel tree root node 
	 * @param hy : hybrid tree root node
	 * @calledBy AnalysisEngine
	 */
	public AnalysisResults(int s, double l, int d, int g, int p, boolean a, boolean e, int[] af, ParallelTreeNode par, HybridTreeNode hy){
		super(new BorderLayout());
		support = s;
		lift = l;
		treeDepth = d;
		granularity = g;
		period = p;
		orderActors = a;
		orderEvents = e;
		artifactFilter = af;
		dmtnParallelRoot = par;
		dmtnHybridRoot = hy;
		
		reportFilter = new Integer[0];
		buildGUI();
	}

	/**
	 * Construct widgets
	 */
	private void buildGUI(){
		pControl = new JPanel();
		pControl.setBorder(new CompoundBorder(BorderFactory.createLoweredBevelBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		pControl.setLayout(gridBag);
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(3, 3, 3, 3); 
		c.gridwidth = GridBagConstraints.REMAINDER;

		//------------Analysis Parameter labels---------------
		JLabel lParameters = new JLabel("Parameters");
		lParameters.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lParameters.setForeground(Color.BLUE);
		gridBag.setConstraints(lParameters, c);
		pControl.add(lParameters);

		JLabel lPGran = new JLabel("Granularity: " + Constants.tempGranularities[granularity]);
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lPGran, c);
		pControl.add(lPGran);

		JLabel lPPeriod = new JLabel("Period: " + period);
		gridBag.setConstraints(lPPeriod, c);
		pControl.add(lPPeriod);

		JLabel lPSupport = new JLabel("Minimum Support: " + support);
		gridBag.setConstraints(lPSupport, c);
		pControl.add(lPSupport);

		JLabel lPTreeDepth = new JLabel("Max. Tree Depth: " + treeDepth);
		gridBag.setConstraints(lPTreeDepth, c);
		pControl.add(lPTreeDepth);

		if(lift > 0.0){
			JLabel lPLift = new JLabel("Minimum Lift: " + lift);
			gridBag.setConstraints(lPLift, c);
			pControl.add(lPLift);
		}

		if(orderActors){
			pControl.add(Store.buildSeparator(c, gridBag));
			JLabel lOrder = new JLabel("Order Event");
			c.anchor = GridBagConstraints.WEST;
			lOrder.setFont(new Font("SansSerif", Font.PLAIN, 12));
			lOrder.setForeground(Color.BLUE);
			gridBag.setConstraints(lOrder, c);
			pControl.add(lOrder);
		}

		if(orderEvents){
			if(!orderActors) pControl.add(Store.buildSeparator(c, gridBag));
			JLabel lWOrder = new JLabel("Order Window");
			c.anchor = GridBagConstraints.WEST;
			lWOrder.setFont(new Font("SansSerif", Font.PLAIN, 12));
			lWOrder.setForeground(Color.BLUE);
			gridBag.setConstraints(lWOrder, c);
			pControl.add(lWOrder);
		}

		pFilter = new JPanel();
		if(artifactFilter.length > 0) buildFilter();  // construct filter panel
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(pFilter, c);
		pControl.add(pFilter);

		pControl.add(Store.buildSeparator(c, gridBag));

		//----------Filter result display----------
		JLabel lFilter = new JLabel("Filter by Actor");
		lFilter.setForeground(Color.BLUE);
		lFilter.setFont(new Font("SansSerif", Font.PLAIN, 12));
		c.anchor = GridBagConstraints.WEST;
		c.weightx = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(lFilter, c);
		pControl.add(lFilter);

		DefaultListModel dlmFilterActor = new DefaultListModel();
		dlmFilterActor.addElement("all");
		
		reportFilterMapping = new ArrayList<Integer>();
		reportFilterMapping.add(-1);
		
		//Construct result filter listing, remove all artifact symbols upon which analysis has been filtered.   
		String[] st = Store.getArtifactSymbols();
		for(int i = 0; i < st.length; i++){
			boolean found = false;
			for(int h = 0; h < artifactFilter.length; h++){
				if(artifactFilter[h] - 1 == i){
					found = true;
					break;
				}
			}
			if(!found){
				reportFilterMapping.add(i);
				dlmFilterActor.addElement(st[i]);
			}
		}

		liFilterActor = new JList(dlmFilterActor);
		liFilterActor.setSelectedIndex(0);
		liFilterActor.addListSelectionListener(this);

		JScrollPane spFilterActor = new JScrollPane(liFilterActor, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
						                                           ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spFilterActor.setPreferredSize(new Dimension(100, 100));
		c.weightx = 0;
		c.anchor = GridBagConstraints.EAST;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(spFilterActor, c);
		pControl.add(spFilterActor);

		pControl.add(Store.buildSeparator(c, gridBag));
 
		//--------display type widget(list,tree)--------
		bPresentType = new JButton("show Tree");
		bPresentType.addActionListener(this);
		c.fill = GridBagConstraints.HORIZONTAL;
		gridBag.setConstraints(bPresentType, c);
		pControl.add(bPresentType);

		JPanel pPad = new JPanel();
		c.weighty = 1;
		gridBag.setConstraints(pPad, c);
		pControl.add(pPad);

		add(pControl, BorderLayout.WEST);
		pDisplay = buildListPresentation();
		add(pDisplay, BorderLayout.CENTER);
		Store.setStatusText("present results");
	}

	/* (non-Javadoc)
	 * Handles modifications to selection of actor filtering. 
	 * Instigates reconstruction of display, given the new filter constraints.  
	 * 
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent event){
		if(event.getValueIsAdjusting())	return;

		// build filter list
		ArrayList<Integer> tempFilter = new ArrayList<Integer>();
		for(int index : liFilterActor.getSelectedIndices()){
			if(index == 0)	continue;
			tempFilter.add(reportFilterMapping.get(index));
		}
		tempFilter.trimToSize();
		reportFilter = tempFilter.toArray(new Integer[0]);

		// rebuild the filter widget
		buildFilter();

		// rebuild the display (filter constraint handled upon rendering)
		remove(pDisplay);
		if(showSets){
			pDisplay = buildListPresentation();
		}else{
			pDisplay = BuildTreePresentation();
		}
		add(pDisplay, BorderLayout.CENTER);
		revalidate();
	}

	/** 
	 * Construct the filter constraint display panel. Comprised of both:
	 * 	- analysis filter elements.
	 * 	- report filter elements.
	 */
	private void buildFilter(){
		pFilter.removeAll();
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		pFilter.setLayout(gridBag);
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(3, 3, 3, 3); 
		c.gridwidth = GridBagConstraints.REMAINDER;

		pFilter.add(Store.buildSeparator(c, gridBag));

		JLabel lPartFilter = new JLabel("Filters");
		lPartFilter.setForeground(Color.BLUE);
		lPartFilter.setFont(new Font("SansSerif", Font.PLAIN, 12));
		gridBag.setConstraints(lPartFilter, c);
		pFilter.add(lPartFilter);

		c.anchor = GridBagConstraints.EAST;
		for(int i = 0; i < artifactFilter.length; i++){
			JLabel lArt = new JLabel(Store.getArtifactSymbol(artifactFilter[i] - 1));
			gridBag.setConstraints(lArt, c);
			pFilter.add(lArt);
		}

		for(int i = 0; i < reportFilter.length; i++){
			JLabel lRep = new JLabel(Store.getArtifactSymbol(reportFilter[i]));
			gridBag.setConstraints(lRep, c);
			pFilter.add(lRep);
		}
		pControl.revalidate();
		
		// Filter the episode tree nodes to control presentation 
		filterParallelTree(dmtnParallelRoot); 
		filterHybridTree(dmtnHybridRoot);
	}

	/**
	 * Print the shown report to *.pdf file
	 * 
	 * @calledBy InitialiseEngine
	 */
	public void printToFile(){
		// Construct document object.
		Document document = new Document(PageSize.A4, 20, 20, 50, 50);
		Phrase p = new Phrase("Episode Detection Tool Report", new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 16));
		HeaderFooter header = new HeaderFooter(p, false);
		header.setBorder(0);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		HeaderFooter footer = new HeaderFooter(new Phrase("Page "), new Phrase(": (" + sdf.format(cal.getTime()) + ")"));
		footer.setAlignment(Element.ALIGN_CENTER);
		footer.setBorder(0);

		// Instigate file chooser to select file name to save to. 
		try{
			JFileChooser fcFile = new JFileChooser(System.getProperty("user.dir"));
			fcFile.setDialogType(JFileChooser.SAVE_DIALOG);
			fcFile.setDialogTitle("Select File to Save as");
			fcFile.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			fcFile.setFileFilter(new FileFilterUtility("pdf", "Adobe PDF Files: "));

			if(fcFile.showOpenDialog(Store.getFrame()) != JFileChooser.APPROVE_OPTION)return;

			PdfWriter.getInstance(document, new FileOutputStream(fcFile.getSelectedFile().getPath()));
			document.setHeader(header);
			document.setFooter(footer);
			document.addTitle("Title");
			document.open();

			com.lowagie.text.Font f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14, com.lowagie.text.Font.BOLD);
			Paragraph para = new Paragraph(new Phrase("Data Source", f));
			para.setSpacingAfter(20);
			para.add(Chunk.NEWLINE);

			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 12);

			for(String name : Store.getInputList()){
				para.add(new Phrase("  " + name.toUpperCase(), f));
				para.add(Chunk.NEWLINE);
			}
			document.add(para);

			//Print analysis parameters
			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14, com.lowagie.text.Font.BOLD);
			para = new Paragraph(new Phrase("Parameters", f));
			para.setSpacingAfter(20);
			para.add(Chunk.NEWLINE);

			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 12);
			para.add(new Phrase("  Granularity : " + Constants.tempGranularities[granularity], f));
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("  Period: " + period, f));
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("  Minimum Support: " + support, f));
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("  Max. Tree Depth: " + treeDepth, f));
			para.add(Chunk.NEWLINE);
			
			if(lift > 0.0){
				para.add(new Phrase("  Minimum Lift: " + lift, f));
				para.add(Chunk.NEWLINE);
			}
			document.add(para);

			if(orderActors){
				f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14, com.lowagie.text.Font.BOLD);
				para = new Paragraph(new Phrase("  Order By Events", f));
				para.add(Chunk.NEWLINE);
				document.add(para);
			}

			if(orderEvents){
				f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14, com.lowagie.text.Font.BOLD);
				para = new Paragraph(new Phrase("  Order By Window", f));
				para.add(Chunk.NEWLINE);
				document.add(para);
			}

			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14, com.lowagie.text.Font.BOLD);
			para = new Paragraph(new Phrase("Filters", f));
			para.setSpacingAfter(20);
			para.add(Chunk.NEWLINE);

			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 12);
			for(int i = 0; i < artifactFilter.length; i++){
				para.add(new Phrase("  " + Store.getArtifactSymbol(artifactFilter[i] - 1), f));
				para.add(Chunk.NEWLINE);
			}
			for(int i = 0; i < reportFilter.length; i++){
				para.add(new Phrase("  " + Store.getArtifactSymbol(reportFilter[i]), f));
				para.add(Chunk.NEWLINE);
			}
			document.add(para);

			//Print parallel episodes
			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14, com.lowagie.text.Font.BOLD);
			para = new Paragraph(new Phrase("Parallel Episodes", f));
			document.add(para);

			edtListModel model = (edtListModel)((JList)(spPara.getViewport().getView())).getModel();
			for(int i = 0; i < model.getSize(); i++){
				document.add(printToFile(model.getElementAt(i)));
			}

			// Print hybrid episodes
			f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 14, com.lowagie.text.Font.BOLD);
			para = new Paragraph(new Phrase("Hybrid Episodes", f));
			para.setSpacingBefore(20);
			document.add(para);
			model = (edtListModel)((JList)(spHy.getViewport().getView())).getModel();
			for(int i = 0; i < model.getSize(); i++){
				document.add(printToFile(model.getElementAt(i)));
			}

		}catch(DocumentException de){
			System.err.println(de.getMessage());
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		document.close();
	}

	/**
	 * Print an episode line. 
	 * 
	 * @param obj : the episode to print
	 * @return The printable representation of the episode  
	 */
	private Paragraph printToFile(Object obj){
		com.lowagie.text.Font f = new com.lowagie.text.Font(com.lowagie.text.Font.HELVETICA, 11);
		Paragraph para = new Paragraph(new Phrase("  ", f));

		if(!(obj instanceof AbstractTreeNode)) return null;
		
		// Get contents of episode to print from associated tree node.
		Integer nodeSymbols[] = ((AbstractTreeNode)obj).contentsToPrintList();
		
		para.add(new Phrase("count: " + nodeSymbols[0].toString()));
		if(lift > 0.0){
			double l = ((AbstractTreeNode)obj).getLift();
			if(l > 0.0)	para.add(new Phrase("(" + Store.getDoubleFormat().format(l) + ")"));
		}

		Phrase ph = new Phrase();
		for(int j = 1; j < nodeSymbols.length; j++){
			if(nodeSymbols[j] == -1){
				// hybrid episode sequence marker, print arrow 		
				try{
					com.lowagie.text.Image ic = com.lowagie.text.Image.getInstance(ClassLoader.getSystemResource("images/rightArrow.gif"));
					ph.add(new Chunk(ic, 5, -3));
					ph.add(new Phrase("  "));
				}catch(IOException e){
					System.out.println("AnalysisResults : PrintToFile : file \"images/rightArrow.gif\" not found");
				}catch(BadElementException e){
					System.out.println("AnalysisResults : PrintToFile : Bad element exception");
				}
			}else{
				//print event representation
				Integer[] tr = Store.getEvent(nodeSymbols[j]).getActorArray();
				StringBuffer sb = new StringBuffer(" [");
					
				for(int h = 0; h < tr.length; h++){
					if(h != 0) sb.append(", ");
					sb.append(Store.getArtifactSymbols()[tr[h]]);
				}
				sb.append("]");
				ph.add(new Chunk(sb.toString()));
			}
		}
		para.add(ph);
		return para;
	}

	/**
	 * Filter the parallel episode tree given a set of actor constraints for display purposes.
	 * The filtration of tree and list representations must be done separatly as a node which
	 * becomes invalid (invisible) within a list presentation, might still be required within a tree
	 * presentation.   
	 * 
	 * @param root : root node of tree
	 */
	private void filterParallelTree(ParallelTreeNode root){
		// Set visiblity of each node within tree display. 
		// Bottom up test as given a visible child its ancestors must all be visible.
		Enumeration en = root.postorderEnumeration();
		while(en.hasMoreElements()){
			ParallelTreeNode node = (ParallelTreeNode)en.nextElement();
			node.setVisibleInTree(false);

			if(!node.isLeaf()){
		  		if(node.hasTreeVisibleChild())node.setVisibleInTree(true);
		  		continue;
		  	}
		  	node.setVisibleInTree(isNodeVisible(node.getPathArtifacts()));
		}
		// Set visiblity of each node within list display. 
		// Top down test as given a valid and visible parent all descendents (supersets) must be visible.
		en = root.preorderEnumeration();
		while(en.hasMoreElements()){
			ParallelTreeNode node = (ParallelTreeNode)en.nextElement();
			node.setVisibleInList(false);
			if(node.isRoot()) continue;
			if(node.isParentVisibleInList()){
				node.setVisibleInList(true);
				continue;
			}else{
			    node.setVisibleInList(isNodeVisible(node.getPathArtifacts()));
			}
		}
	}

	/**
	 * Filter the hybrid episode tree given a set of actor constraints for display purposes.
	 * The filtration of tree and list representations must be done separatly as a node which
	 * becomes invalid (invisible) within a list presentation, might still be required within a tree
	 * presentation.   
	 * 
	 * @param root : root node of tree
	 */
	private void filterHybridTree(HybridTreeNode root){
		// Set visiblity of each node within tree display. 
		// Bottom up test as given a visible child its ancestors must all be visible.
		Enumeration en = root.postorderEnumeration();
		while(en.hasMoreElements()){
			HybridTreeNode node = (HybridTreeNode)en.nextElement();
			node.setVisibleInTree(false);
			if(!node.isLeaf()){
		  		if(node.hasTreeVisibleChild())node.setVisibleInTree(true);
		  		continue;
		  	}
		  	if(node.isRoot())continue;
		  	node.setVisibleInTree(isNodeVisible(node.getPathArtifacts()));

		}
		// Set visiblity of each node within list display. 
		// Top down test as given a valid and visible parent all descendents (supersets) must be visible.
		en = root.preorderEnumeration();
		while(en.hasMoreElements()){
			HybridTreeNode node = (HybridTreeNode)en.nextElement();
			node.setVisibleInList(false);
			if(node.isRoot()) continue;
			if(node.isParentVisibleInList()){
				node.setVisibleInList(true);
				continue;
			}else{
			    node.setVisibleInList(isNodeVisible(node.getPathArtifacts()));
			}
		}
	}

	/**
	 * Does the episode relating to the node contain an artifact in the filter list. 
	 * @param eventSymbols : the event symbols pertaining to a node.
	 * @return True if no filter list artifacts are found (hence visible) else return false. 
	 */
	private boolean isNodeVisible(Integer[] eventSymbols){
		
		// this  will never return false as the filtering is done during analysis and is hence alsways true 
		if(artifactFilter.length > 0){
			for(int a = 0; a < artifactFilter.length; a++){
				for(int e = 0; e < eventSymbols.length; e++){
					if(artifactFilter[a] - 1 == eventSymbols[e]) return false;
				}
			}
		}
		// all elements of the report filter must exist within the episode for it to be valid "AND" join 
		for(int a = 0; a < reportFilter.length; a++){
			boolean repElementExists = false;
			for(int e = 0; e < eventSymbols.length; e++){
				if(reportFilter[a] == eventSymbols[e]){
					repElementExists = true;
					break;
				}
			}
			if(!repElementExists) return false;
		}
		return true;
	}
	
	/**
	 * Populate the episode "list" display panel.
	 * @return the display panel
	 */
	private JPanel buildListPresentation(){
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 5, 5, 5); // t,l,b,r
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.weightx = 1;
		c.anchor = GridBagConstraints.WEST;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;

		JPanel pTextual = new JPanel();
		pTextual.setBorder(new CompoundBorder(BorderFactory.createLoweredBevelBorder(), BorderFactory.createEmptyBorder(7, 7, 7, 7)));
		pTextual.setLayout(gridBag);
		
		//---------Parallel episode display-------------
		spPara = buildListPanel(dmtnParallelRoot);
		if(spPara != null){
			JLabel lpara = new JLabel("Parallel Episode Sets", SwingConstants.LEFT);
			gridBag.setConstraints(lpara, c);
			pTextual.add(lpara);
			
			c.fill = GridBagConstraints.BOTH;
			c.weighty = 1;
			gridBag.setConstraints(spPara, c);
			pTextual.add(spPara);
		}

		//------------Hybrid episode display-------------
		spHy = buildListPanel(dmtnHybridRoot);
		if(spHy != null){
			JLabel lhy = new JLabel("Hybrid Episode Sets", SwingConstants.LEFT);
			c.weighty = 0;
			c.fill = GridBagConstraints.NONE;
			gridBag.setConstraints(lhy, c);
			pTextual.add(lhy);

			c.fill = GridBagConstraints.BOTH;
			c.weighty = 1;
			gridBag.setConstraints(spHy, c);
			pTextual.add(spHy);
		}
		return pTextual;
	}

	/**
	 * Construct contents of episode display pane given a episode tree structure (Parallel or hybrid).  
	 * 
	 * @param root : root of tree to be displayed.
	 * @return the populated pane.
	 */
	private JScrollPane buildListPanel(AbstractTreeNode root){
		// Sort display into descending frequency order
		TreeSet<AbstractTreeNode> sortedPTree = new TreeSet<AbstractTreeNode>(new Comparator<AbstractTreeNode>(){
			public int compare(AbstractTreeNode a, AbstractTreeNode b){
				if(b.count - a.count > 0) return 1;
				if(b.count - a.count < 0) return -1;
				if(b.getLevel() > a.getLevel()) return -1;
				return 1;
			}
		});

		Enumeration en = root.breadthFirstEnumeration();
		while(en.hasMoreElements()){
			AbstractTreeNode node = (AbstractTreeNode)en.nextElement();
			//This refers to the level of the tree at which nodes should be displayed.
			//Parallel tree all nodes below the root should be shown.
			//Hybrid tree all nodes below the first level should be shown (level 1 replicates the parallel tree). 
			if(root instanceof ParallelTreeNode){
				if(node.getLevel() > 0) sortedPTree.add(node);
			}else{
				if(node.getLevel() > 1) sortedPTree.add(node);
			}
		}

		if(sortedPTree.size() < 1)return null;
		
		//Populate pane 
		edtListModel dlm = new edtListModel();
		for(AbstractTreeNode node : sortedPTree){
			dlm.addElement(node);
		}
		
		JList lResults = new JList(dlm);
		lResults.setBackground(Color.WHITE);
		lResults.setEnabled(true);
		lResults.setCellRenderer(new edtListCellRenderer());
		lResults.addMouseListener(new edtMouseListener(lResults));
		JScrollPane spList = new JScrollPane(lResults, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spList.setBackground(Color.WHITE);
		spList.setBorder(BorderFactory.createLoweredBevelBorder());
		return spList;
	}

	/**
	 * Build the episode "tree" display.
	 * 
	 * @return the display panel
	 */
	private JPanel BuildTreePresentation(){
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(5, 5, 5, 5); // t,l,b,r
		c.weightx = 0;
		c.weighty = 0;
		c.anchor = GridBagConstraints.WEST;

		JPanel pTree = new JPanel();
		pTree.setBorder(new CompoundBorder(BorderFactory.createLoweredBevelBorder(), BorderFactory.createEmptyBorder(7, 7, 7, 7)));
		pTree.setLayout(gridBag);

		// Establish framework for display.
		JScrollPane spPara = buildTreePanel(dmtnParallelRoot);
		JScrollPane spHy = buildTreePanel(dmtnHybridRoot);

		if(spPara != null){
			JLabel lpara = new JLabel("Parallel Episode Tree", SwingConstants.LEFT);
			c.gridwidth = GridBagConstraints.RELATIVE;
			if(spHy == null)
				c.gridwidth = GridBagConstraints.REMAINDER;
			gridBag.setConstraints(lpara, c);
			pTree.add(lpara);
		}

		if(spHy != null){
			JLabel lhy = new JLabel("Hybrid Episode Tree", SwingConstants.LEFT);
			c.gridwidth = GridBagConstraints.REMAINDER;
			gridBag.setConstraints(lhy, c);
			pTree.add(lhy);
		}

		c.weightx = 1;
		c.weighty = 1;

		if(spPara != null){
			c.fill = GridBagConstraints.BOTH;
			c.gridwidth = GridBagConstraints.RELATIVE;
			if(spHy == null)
				c.gridwidth = GridBagConstraints.REMAINDER;
			c.weighty = 1;
			gridBag.setConstraints(spPara, c);
			pTree.add(spPara);
		}

		if(spHy != null){
			c.gridwidth = GridBagConstraints.REMAINDER;
			gridBag.setConstraints(spHy, c);
			pTree.add(spHy);
		}
		return pTree;
	}

	/**
	 * Construct contents of episode display pane given a episode tree structure (Parallel or hybrid).
	 *   
	 * @param root : root of tree to present.
	 * @return the constructed pane.
	 */
	private JScrollPane buildTreePanel(AbstractTreeNode root){
		JTree jt = null;
		//Construct underlying tree model.
		if(root instanceof ParallelTreeNode){
			jt = new JTree(new edtParallelTreeModel(root));
		}else{
			jt = new JTree(new edtHybridTreeModel(root));
		}
		if(jt.getModel().getChildCount(root) < 1) return null;

		jt.setVisible(true);
		jt.setRootVisible(false);
		jt.setCellRenderer(new edtTreeCellRenderer());
		jt.setToggleClickCount(1);
		jt.addMouseListener(new edtMouseListener(jt));

		// Initialise tree presentation. Expand tree presentation to the level such that 20 rows are presented.   
		int level = 1;
		while(true){
			if(jt.getRowCount() > 20 || level++ >= 3) break;
			for(int i = jt.getRowCount(); i >= 0; i--){
				if(!jt.isExpanded(i)){
					jt.expandRow(i);
				}
			}
		}

		JScrollPane spList = new JScrollPane(jt, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spList.setBackground(Color.WHITE);
		spList.setBorder(BorderFactory.createLoweredBevelBorder());
		return spList;
	}

	/* (non-Javadoc)
	 * Toggle between tree and list views.
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent arg0){
		remove(pDisplay);
		if(showSets){
			pDisplay = BuildTreePresentation();
			add(pDisplay, BorderLayout.CENTER);
			bPresentType.setText("Show Sets");
		}else{
			pDisplay = buildListPresentation();
			add(pDisplay, BorderLayout.CENTER);
			bPresentType.setText("Show Tree");
		}
		revalidate();
		showSets = !(showSets);
	}

//---------Inner Classes-------------

	/**
	 * Customises the rendering of components in the list presentation.
	 * 
	 * @author: Aaron Ceglar 
	 */
	class edtListCellRenderer extends JPanel implements ListCellRenderer{
		public edtListCellRenderer(){
			super();
			setOpaque(true);
		}

		/* (non-Javadoc)
		 * Constructs a render object to present an episode within the list display
		 * 
		 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
		 */
		public JComponent getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus){
			// Ensure value is a subclass of AbstractTreeNode. 
			if(!(value instanceof AbstractTreeNode)){
				add(new JLabel(value.toString()));
				System.out.println("renderer: not valid tree node");
				return this;
			}

			// Initialise presentation component.
			JPanel pPresent = new JPanel(new FlowLayout(FlowLayout.LEFT));
			pPresent.setBackground(Color.white);
			Integer nodeContents[] = ((AbstractTreeNode)value).contentsToPrintList();
			pPresent.add(new JLabel(nodeContents[0].toString())); // count

			if(lift > 0.0){
				double actualLift = ((AbstractTreeNode)value).getLift();
				if(actualLift > 0.0) pPresent.add(new JLabel("(" + Store.getDoubleFormat().format(actualLift) + ")"));
			}

			StringBuffer sb = new StringBuffer();
			for(int j = 1; j < nodeContents.length; j++){
				if(j != 1) sb.append("  ");

				if(nodeContents[j] == -1){	// Hybrid episode sequence separator.
					JLabel lab = new JLabel(sb.toString());
					lab.setForeground(isSelected ? Color.blue : Color.black);
					if(!((AbstractTreeNode)value).isVisibleInList()) lab.setForeground(isSelected ? Color.blue : Color.orange);
					pPresent.add((lab));
					sb = new StringBuffer();
					java.awt.Image ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/rightArrow.gif"));
					pPresent.add(new JLabel(new ImageIcon(ic)));
				}else{
					// get the artifact symbols associated with the event (node) symbol
					Integer[] tr = Store.getEventArtifacts(nodeContents[j]);
					sb.append("[");
					for(int h = 0; h < tr.length; h++){
						if(h != 0) sb.append(", ");
						sb.append(Store.getArtifactSymbols()[tr[h]]);
					}
					sb.append("]");
				}
			}
			JLabel lab = new JLabel(sb.toString());
			lab.setForeground(isSelected ? Color.blue : Color.black);
			pPresent.add((lab));
			return pPresent;
		}
	}

	/**
	 * Customises the rendering of components in the tree presentation.
	 * 
	 * @author: Aaron Ceglar 
	 */
	class edtTreeCellRenderer extends DefaultTreeCellRenderer{
		public edtTreeCellRenderer(){
			super();
			setOpaque(true);
		}

		/* (non-Javadoc)
		 * Constructs a render object to present an episode within the list display
		 * 
		 * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
		 */
		public Component getTreeCellRendererComponent(JTree tree, Object value,	boolean selected, boolean expanded, 
						                              boolean leaf, int row, boolean hasFocus){
			// Ensure value is a subclass of AbstractTreeNode. 
			if(!(value instanceof AbstractTreeNode)){
				add(new JLabel(value.toString()));
				System.out.println("renderer: not parallel tree node");
				return this;
			}
			
			// Construct presentation component
			AbstractTreeNode node = (AbstractTreeNode)value;
			Integer[] nodeSymbol = node.contentsToPrintTree();
			
			StringBuffer sb = new StringBuffer(nodeSymbol[0] + "   ");
			if(lift > 0){
				double actualLift = ((AbstractTreeNode)value).getLift();
				if(actualLift > 0.0)	sb.append("(" + Store.getDoubleFormat().format(actualLift) + ") ");
			}
			for(int j = 1; j < nodeSymbol.length; j++){
				if(j != 1) sb.append("  ");
				Integer[] tr = Store.getEventArtifacts(nodeSymbol[j]);
				sb.append("[");
				for(int h = 0; h < tr.length; h++){
					if(h != 0)	sb.append(", ");
					sb.append(Store.getArtifactSymbols()[tr[h]]);
				}
				sb.append("]");
			}

			java.awt.Image ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/blueball.gif"));
			if(expanded)
				ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/greenball.gif"));
			if(leaf)
				ic = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/redball.gif"));

			JLabel lPresent = new JLabel(sb.toString(), new ImageIcon(ic), SwingConstants.LEFT);
			lPresent.setBackground(Color.white);
			lPresent.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
			lPresent.setForeground(selected ? Color.blue : Color.black);

			if(!tree.isSelectionEmpty()){
				AbstractTreeNode selectedNode = (AbstractTreeNode)tree.getLastSelectedPathComponent();
				if(selectedNode.isNodeDescendant(node))	lPresent.setForeground(Color.red);
				if(selectedNode.isNodeAncestor(node)) lPresent.setForeground(new Color(60, 140, 60));
			}
			if(selected) lPresent.setForeground(Color.blue);
			return lPresent;
		}
	}

	/**
	 * This list model esxtension ensures that only "visible" tree nodes are considered as valid.
	 * In order to maintain an immutabler tree structure the model's access methods have been 
	 * overwritten, such that only valid "visible" nodes are accessed.  
	 *
	 * @author: Aaron Ceglar 
	 */
	class edtListModel extends AbstractListModel{
		private ArrayList<AbstractTreeNode> delegate = new ArrayList<AbstractTreeNode>(100);

		/**
		 * Constructor
		 */
		public edtListModel(){
			super();
		}

		/* (non-Javadoc)
		 * Get the number of visible nodes in the model.
		 * @see javax.swing.ListModel#getSize()
		 */
		public int getSize(){
			int count = 0;
			for(AbstractTreeNode node : delegate){
				if(node.isVisibleInList)count++;
			}
			return count;
		}

		
		/* (non-Javadoc)
		 * Get the visible episode specified by "index" from the model.
		 * @see javax.swing.ListModel#getElementAt(int)
		 */
		public Object getElementAt(int index){
			int validCount = 0;
			for(AbstractTreeNode node : delegate){
				if(!node.isVisibleInList) continue;
				if(validCount == index)	return node;
				validCount++;
			}
			return null;
		}

		/**
		 * Append a node to the model. 
		 * @param node
		 */
		public void addElement(AbstractTreeNode node){
			delegate.add(node);
		}
	}

	/**
	 * This parallel tree model esxtension ensures that only "visible" tree nodes are considered as valid. 
	 * In order to maintain an immutabler tree structure the model's access methods have been 
	 * overwritten, such that only valid "visible" nodes are accessed.  
	 *
	 * @author: Aaron Ceglar 
	 */
	class edtParallelTreeModel extends DefaultTreeModel{

		/**
		 * @param root: root of the presented tree
		 */
		public edtParallelTreeModel(TreeNode root){
			super(root);
		}

		/* (non-Javadoc)
		 * Return the number of visible child nodes.
		 * @see javax.swing.tree.DefaultTreeModel#getChildCount(java.lang.Object)
		 */
		public int getChildCount(Object n){
			int newCount = 0;
			Enumeration en = ((AbstractTreeNode)n).children();
			while(en.hasMoreElements()){
				AbstractTreeNode child = (AbstractTreeNode)en.nextElement();
				if(child.isVisibleInTree)newCount++;
			}
			return newCount;
		}

		/* (non-Javadoc)
		 * Return the index of the specified child within the parent list.
		 * Return -1 if the node (child) requested is not visible, else return the count constrained to visible children only.
		 *  
		 * @see javax.swing.tree.DefaultTreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
		 */
		public int getIndexOfChild(Object parentNode, Object childNode){
			AbstractTreeNode child = (AbstractTreeNode)childNode;
			if(!child.isVisibleInTree) return -1;

			int newCount = 0;
			Enumeration en = ((AbstractTreeNode)parentNode).children();
			while(en.hasMoreElements()){
				AbstractTreeNode child2 = (AbstractTreeNode)en.nextElement();
				if(child.equals(child2))break;
				if(child2.isVisibleInTree) newCount++;
			}
			return newCount;
		}

		/* (non-Javadoc)
		 * Get the child specified at the visible index.
		 * 
		 * @see javax.swing.tree.DefaultTreeModel#getChild(java.lang.Object, int)
		 */
		public Object getChild(Object parentNode, int index){
			AbstractTreeNode child = null;

			int newCount = -1;
			Enumeration en = ((AbstractTreeNode)parentNode).children();
			while(en.hasMoreElements()){
				child = (AbstractTreeNode)en.nextElement();
				if(child.isVisibleInTree) newCount++;
				if(newCount == index) break;
			}
			return child;
		}
	}

	/**
	 * This hybrid tree model esxtension ensures that only "visible" tree nodes are considered as valid. 
	 * In order to maintain an immutabler tree structure the model's access methods have been 
	 * overwritten, such that only valid "visible" nodes are accessed.  
	 *
	 * @author: Aaron Ceglar 
	 */
	class edtHybridTreeModel extends DefaultTreeModel{

		/**
		 * @param root
		 */
		public edtHybridTreeModel(TreeNode root){
			super(root);
		}

		/* (non-Javadoc)
		 * Return the number of visible child nodes.
		 * Don't display level 1 leaf nodes.
		 * 
		 * @see javax.swing.tree.DefaultTreeModel#getChildCount(java.lang.Object)
		 */
		public int getChildCount(Object n){
			int newCount = 0;
			Enumeration en = ((AbstractTreeNode)n).children();
			while(en.hasMoreElements()){
				AbstractTreeNode child = (AbstractTreeNode)en.nextElement();
				if(child.getLevel() == 1 && child.isLeaf())	continue;
				if(child.isVisibleInTree) newCount++;
			}
			return newCount;
		}

		/* (non-Javadoc)
		 * Return the index of the specified child within the parent list.
		 * Return -1 if the node (child) requested is not visible, else return the count constrained to visible children only.
		 * Don't display level 1 leaf nodes.
		 *
		 * @see javax.swing.tree.DefaultTreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
		 */
		public int getIndexOfChild(Object p, Object c){
			AbstractTreeNode child = (AbstractTreeNode)c;
			if(!child.isVisibleInTree)return -1;

			int newCount = 0;
			Enumeration en = ((AbstractTreeNode)p).children();
			while(en.hasMoreElements()){
				AbstractTreeNode child2 = (AbstractTreeNode)en.nextElement();
				if(child.equals(child2)) break;
				if(child.getLevel() == 1 && child.isLeaf())	continue;
				if(child2.isVisibleInTree) newCount++;
			}
			return newCount;
		}

		/* (non-Javadoc)
		 * Get the child specified at the visible index.
		 * Don't display level 1 leaf nodes. 
		 * 
		 * @see javax.swing.tree.DefaultTreeModel#getChild(java.lang.Object, int)
		 */
		public Object getChild(Object p, int index){
			AbstractTreeNode child = null;

			int newCount = -1;
			Enumeration en = ((AbstractTreeNode)p).children();
			while(en.hasMoreElements()){
				child = (AbstractTreeNode)en.nextElement();
				if(child.getLevel() == 1 && child.isLeaf())	continue;
				if(child.isVisibleInTree) newCount++;
				if(newCount == index) break;
			}
			return child;
		}
	}

	/**
	 * Picks up on clicks over either the list or tree displays.
	 * Constrcucts a popup menu with launches for detailed report and event level visualisation. 
	 * 
	 * @author: Aaron Ceglar 
	 */
	class edtMouseListener extends MouseAdapter{
		private JList listParent = null;
		private JTree treeParent = null;
		private boolean isTree = true;

		/**
		 * Constructor.
		 * 
		 * @param comp : The component that was clicked
		 */
		public edtMouseListener(JComponent comp){
			super();
			if(comp instanceof JTree){
				treeParent = (JTree)comp;
			}else{
				isTree = false;
				listParent = (JList)comp;
			}
		}

		/* (non-Javadoc)
		 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
		 */
		public void mousePressed(MouseEvent e){
			this.handleMouseEvent(e);
		}

		/* (non-Javadoc)
		 * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
		 */
		public void mouseReleased(MouseEvent e){
			this.handleMouseEvent(e);
		}

		/**
		 * Construct popup menu for selected episode and display. Enables launch of detailed reports
		 * 
		 * @param e : the MouseEvent
		 */
		private void handleMouseEvent(MouseEvent e){
			if(!e.isPopupTrigger())	return;

			// Get the node that was clicked upon, depending upon display type. 
			if(isTree){
				TreePath tp = treeParent.getPathForLocation((int)e.getPoint().getX(), (int)e.getPoint().getY());
				detailNode = (AbstractTreeNode)tp.getLastPathComponent();
			}else{
				detailNode = (AbstractTreeNode)listParent.getModel().getElementAt(listParent.locationToIndex(e.getPoint()));
			}

			// Construct a popup menu.
			JPopupMenu popupMenu = new JPopupMenu("Details");
			
			//Selection launches ReportTextDetail.
			JMenuItem miTextDetails = new JMenuItem("Text Report");
			miTextDetails.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					new ReportTextDetail(detailNode, Constants.tempGranMillis[granularity] * period);
				}
			});
			popupMenu.add(miTextDetails);

			// Populate with choices of events from selected episode for report Event Visualisation. 
			for(final Integer event:detailNode.getEvents()){
				Integer[] artifacts = Store.getEventArtifacts(event);
				final StringBuffer eventTitle = new StringBuffer();
				
				for(int h = 0; h < artifacts.length; h++){
					if(h != 0) eventTitle.append(", ");
					eventTitle.append(Store.getArtifactSymbol(artifacts[h]));
				}

				JMenuItem miCircularVis = new JMenuItem("Visualisation for " + eventTitle);
				miCircularVis.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						new ReportEventVisualisation(event, eventTitle.toString());
					}
				});
				popupMenu.add(miCircularVis);
			}
		

			if(isTree){
				popupMenu.show(treeParent, e.getX(), e.getY());
			}else{
				popupMenu.show(listParent, e.getX(), e.getY());
			}
		}
	}

}
