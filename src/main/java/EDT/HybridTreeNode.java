package EDT;

import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.tree.TreeNode;

/**
 * The base component of the hybrid episode tree.
 * All hybrid episodes are stored within this structure.
 * Consists of methods for adding and maintinence.
 * 
 * @author: Aaron Ceglar 
 */
public class HybridTreeNode extends AbstractTreeNode{
	protected ParallelTreeNode symbol = null;

	/**
	 * Base constructor. 
	 * 
	 * @param p : parallelTreeNode to be associated with this hybrid node.
	 * @calledBy AnalysisEngine
	 */
	public HybridTreeNode(ParallelTreeNode p){
		super();
		symbol = p;
	}

	/**
	 * Constructor with initial ParallelTreeNode and its temporal data.
	 * Used in construction of first level of tree. 
	 * 
	 * @param p : parallel episode
	 * @param td : temporal data
	 * @calledBy AnalysisEngine
	 */
	public HybridTreeNode(ParallelTreeNode p, TreeSet<EpisodeInstance> td){
		this(p);
		temporalData = new TreeSet<EpisodeInstance>(td);
		valid = true;
		count = p.count;
	}
	
/*=================Analysis Methods===============*/	
	
	/**
	 * Get the details of the hybrid episode associated with this node in order to print
	 * it out in a list context. The contents are :
	 * 	- occurance count
	 * 	- associated event symbols : path symbols.
	 *  - spacer (-1) to indicate the boundary between parallel episodes. 	
	 * Don't include root.
	 * 
	 * @see EDT.AbstractTreeNode#contentsToPrintList()
	 * @calledBy AnalysisResults ReportTextDetail
	 */
	public Integer[] contentsToPrintList(){
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(count);

		TreeNode[] path = getPath();
		for(int depth = 1; depth < path.length; depth++){
			TreeNode[] innerPath = (((HybridTreeNode)path[depth]).symbol).getPath();
			for(int innerDepth = 1; innerDepth < innerPath.length; innerDepth++){
				list.add(((ParallelTreeNode)innerPath[innerDepth]).symbol);
			}
			if(depth < path.length - 1) list.add(-1);
		}
		return list.toArray(new Integer[0]);
	}

	/**
	 * Get the details of the hybrid episode associated with this node in order to print
	 * it out in a tree context. The contents are
	 * 	- occurance count
	 * 	- associated parallel episode symbol	
	 * 
	 * @see EDT.AbstractTreeNode#contentsToPrintTree()
	 * @calledBy AnalysisResults
	 */
	public Integer[] contentsToPrintTree(){
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(count);
		TreeNode[] path = symbol.getPath();
		for(int depth = 1; depth < path.length; depth++)
			list.add(((ParallelTreeNode)path[depth]).symbol);
		return list.toArray(new Integer[0]);
	}

	/**
	 * Get the set of event identifiers that comprise the associated episode.
	 * 
	 * @see EDT.AbstractTreeNode#getEvents()
	 * @calledBy AnalysisResults
	 */
	public Integer[] getEvents(){
		ArrayList<Integer> list = new ArrayList<Integer>();

		TreeNode[] path = getPath();
		for(int i = 1; i < path.length; i++){
			TreeNode[] node = (((HybridTreeNode)path[i]).symbol).getPath();
			for(int j = 1; j < node.length; j++){
				list.add(((ParallelTreeNode)node[j]).symbol);
			}
		}
		return list.toArray(new Integer[0]);
	}
	
	/**
	 * Test if the two hybrid episodes are equivalent.
	 * 
	 * @param h : comparison hybrid episode
	 * @return equivalency
	 * @calledBy Episode
	 */
	public boolean equals(HybridTreeNode comp){
		if (getLevel() != comp.getLevel()) return false;
		
		HybridTreeNode[] compPath = (HybridTreeNode[])comp.getPath();
		HybridTreeNode[] path = (HybridTreeNode[])getPath();
		for(int depth = 1; depth < path.length; depth++){
			for(int compDepth = 1; compDepth < compPath.length; compDepth++){
				if(!path[depth].getSymbol().equals(compPath[compDepth].getSymbol()))
					return false;
			}
		}
		return true;
	}

	/**
	 * Does the last parallel episode of this hybrid episode contain a common event.  
	 * 
	 * @param comp : the comparison node
	 * @return does the last parallel episode contain a common event. 
	 * @calledBy AnalysisEngine
	 */
	public boolean lastContains(AbstractTreeNode comp){
		
		TreeNode[] compPath = ((ParallelTreeNode)comp).getPath();
		TreeNode[] path = symbol.getPath();

		for(int depth = 1; depth < path.length; depth++){
			for(int compDepth = 1; compDepth < compPath.length; compDepth++){
				if(((ParallelTreeNode)path[depth]).symbol == ((ParallelTreeNode)compPath[compDepth]).symbol)
					return true;
			}
		}
		return false;
	}
	
/*=================Presentation Methods===============*/	
	
	/**
	 * @return episode symbol
	 * @calledBy AbstractTreeNode AnalysisEngine Episode
	 */
	public ParallelTreeNode getSymbol(){
		return symbol;
	}

	/**
	 * Test if the symbols are equivalent.
	 * 
	 * @param sym : comparison symbol
	 * @return is equivalent
	 * @calledBy AnalysisEngine
	 */
	public boolean symbolEquals(ParallelTreeNode sym){
		return symbol.equals(sym);
	}
	
	/**
	 * Get the list of episode component symbols.
	 * 
	 * @return path symbols list 
	 * @calledBy ReportTextDetail
	 */
	public ArrayList<ParallelTreeNode> getPathSymbols(){
		ArrayList<ParallelTreeNode> list = new ArrayList<ParallelTreeNode>();
		TreeNode[] nodes = getPath();
		for(int depth = 1; depth < nodes.length; depth++){
			HybridTreeNode node = (HybridTreeNode)nodes[depth];
			list.add(node.getSymbol());
		}
		return list;
	}

	/**
	 * Get the set of artifacts associated with this episode.
	 * 
	 * @return artifact set
	 * @calledBy AnalysisResults
	 */
	public Integer[] getPathArtifacts(){
		TreeSet<Integer> set = new TreeSet<Integer>();
		TreeNode[] path = getPath();
		for(int depth = 1; depth < path.length; depth++){
			Integer[] pas = ((HybridTreeNode)path[depth]).getSymbol().getPathArtifacts();
			for(Integer j: pas){
				set.add(j);
			}
		}
		return set.toArray(new Integer[0]);
	}
	
/*=================Other Methods===============*/	

	/**
	 * Print out a textual representation of the ParallelTreeNode. 
	 * 
	 * @see EDT.AbstractTreeNode#toString()
	 */
	public String toString(){
		if(symbol == null)	return "";
		return "hybrid: " + symbol.getPathSymbols() + " (" + count + " : " + valid + " : " + active + ") " + temporalData;
	}

	/**
	 * Create a new episode given this node. 
	 * 
	 * @param sDate : start date
	 * @param eDate : end date
	 * @param node : episode node
	 * @return new Episode instance
	 * @see EDT.AbstractTreeNode#createNewEpisodeData(long, long, EDT.AbstractTreeNode)
	 * @calledBy AbstractTreeNode
	 */
	protected Episode createNewEpisodeData(long sDate, long eDate, AbstractTreeNode node){
		return new Episode(sDate, eDate, node, ((HybridTreeNode)node).getPathSymbols());
	}
}
