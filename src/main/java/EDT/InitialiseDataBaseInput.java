package EDT;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 * Enables the specification of database feilds for analysis.
 * At present this is limited to essentially three data feilds give the selection of a table from a database
 * 	- Date feild
 * 	- To (initiator) feild
 * 	- From (reciever) feild
 * 
 * @author: Aaron Ceglar Denise DeVries 
 */
public class InitialiseDataBaseInput extends JPanel implements ActionListener{
	private DatabaseConnection db = null; // Database connection object
	private JComboBox cbName = null; // list of database tables
	private JComboBox cbDate = null; // list of database table columns, Date selection
	private JComboBox cbFrom = null; // list of database table columns, From selection
	private JComboBox cbTo = null; // list of database table columns, To selection

	private int timeStampColumn = 1;
	private InputSourceDialog parent = null;

	/**
	 * Constructor. Launch GUI.
	 * 
	 * @param path : Database path
	 * @param owner : parent dialog
	 * @calledBy InputSourceDialog
	 */
	public InitialiseDataBaseInput(String path, InputSourceDialog owner){
		super();
		parent = owner;
		parent.setOkButton(false);
		db = new DatabaseConnection(path);
		buildGUI();
		setVisible(true);
	}

	/**
	 * Construct widgets
	 */
	private void buildGUI(){
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Initialise Parameters", 
				  TitledBorder.CENTER, TitledBorder.TOP, new Font("Times", Font.PLAIN, 12), Color.BLUE));

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		setLayout(gridBag);
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 5, 5, 5); 

		//-------------Table widget-----------------
		JLabel lbFileDb = new JLabel("Select TABLE ");
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbFileDb, c);
		add(lbFileDb);
		

		cbName = new JComboBox();
		cbName.addItem("UNSPECIFIED");
		for(String tblName : db.getCatalogue()){
			cbName.addItem(tblName);
		}
	
		cbName.setSelectedIndex(0);
		cbName.addActionListener(this);
		c.gridwidth = GridBagConstraints.REMAINDER;

		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(cbName, c);
		add(cbName);

		//-------------Date widget-----------
		JLabel lbEvDate = new JLabel("Select DATE column ");
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbEvDate, c);
		add(lbEvDate);

		cbDate = new JComboBox();
		cbDate.addItem("UNSPECIFIED");
		cbDate.addActionListener(this);
		cbDate.setSelectedIndex(0);
		c.weighty = 2;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(cbDate, c);
		add(cbDate);

		//----------From (reciever) widget---------- 
		JLabel lbEvFrom = new JLabel("Select FROM column ");
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbEvFrom, c);
		add(lbEvFrom);
		cbFrom = new JComboBox();
		cbFrom.addItem("UNSPECIFIED");
		cbFrom.setSelectedIndex(0);
		cbFrom.addActionListener(this);
		c.weighty = 4;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(cbFrom, c);
		add(cbFrom);

		//---------To (initiator) widget----------------
		JLabel lbEvTo = new JLabel("Select TO column ");
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(lbEvTo, c);
		add(lbEvTo);
		cbTo = new JComboBox();
		cbTo.addItem("UNSPECIFIED");
		cbTo.setSelectedIndex(0);
		cbTo.addActionListener(this);
		c.weighty = 5;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.NONE;
		gridBag.setConstraints(cbTo, c);
		add(cbTo);

		//----------Pad Panel----------------
		JPanel pPad = new JPanel();
		c.weighty = 1;
		gridBag.setConstraints(pPad, c);
		add(pPad);
		return;
	}

	/**
	 * Populate the widget lists with available database columns once the database table has been selected. 
	 * 
	 * @param table
	 * @throws SQLException
	 */
	private void populateWidgetLists(String table) throws SQLException{
		cbDate.removeAllItems();
		cbDate.addItem("UNSPECIFIED");
		cbFrom.removeAllItems();
		cbFrom.addItem("UNSPECIFIED");
		cbTo.removeAllItems();
		cbTo.addItem("UNSPECIFIED");
		
		for(String [] names : db.getSchema(table)){
			if(names[1].equals("DATETIME")){
				cbDate.addItem(names[0]);
			}else{
				cbFrom.addItem(names[0]);
				cbTo.addItem(names[0]);
			}
		}
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae){
		// re build widget lists if table seleccted
		if(ae.getSource() == cbName){
			String table = cbName.getSelectedItem().toString();
			try{
				populateWidgetLists(table);
			}catch(SQLException e){
				System.out.println("Invalid Table Name");
			}
		}
		//if all feilds selected enable the parent dialog Ok button
		parent.setOkButton(false);
		if((cbName.getSelectedIndex() != 0) && (cbDate.getSelectedIndex() != 0) &&
		   (cbFrom.getSelectedIndex() != 0) && (cbTo.getSelectedIndex() != 0)){
			parent.setOkButton(true);
		}

	}

	/**
	 * Populate the EDT DB Data store and set the timestamp column 
	 * Triggered upon parent dialog close.
	 *  
	 * @calledBy InputSourceDialog
	 */
	public void dispose(){
		Store.setDbData(db.alQuery("Select " + cbDate.getSelectedItem().toString()+", " + cbFrom.getSelectedItem().toString()+
			            ", "+cbTo.getSelectedItem().toString() + " from " + cbName.getSelectedItem().toString()+
			            " order by " + cbDate.getSelectedItem().toString()));
		Store.setTimeStampColumn(timeStampColumn);
		db.dbClose();
	}

}
