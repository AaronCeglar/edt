package EDT;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Enables the selection of a date Format from a validated list of available date formats.
 * Furthermore allows the user to create new formats and delete old ones.
 * 
 *  The listof valid date formats is persistent and stored in an associated .df file.
 * 
 * @author: Aaron Ceglar
 */

public class DateFormatDialog extends JDialog implements ActionListener, ListSelectionListener{
	private JPanel pMain = null; // Main dialog canvas.

	private JList lFormats = null; // List of valid formats.
	private DefaultListModel lmFormats = null; // Underlying model of valid formats.
	private JScrollPane spFormats = null;  // Scroll Pane enclosing the valid format list.	

	// List of invalid formats according to candidate string. Used to maintain entire list by 
	// ensuring that the invalid as well as valid formats are written back to Store.
	private ArrayList<SimpleDateFormat> alInvalidFormats = null;  

	private JButton bOk = null;
	private JButton bCancel = null;

	private String sCandidate = null; // the candidate String.
	
	private String sDateRep = null; // the date representation of the candidate string.
	private JLabel lbDateRep = null; // JLabel encapsulating

	private Date date = null;  //Date object

	private JTextField tfEdit = null; // Edit feild
	
	private int selectedIndex = 0; //currently selected valid format element.
	private boolean isDeleting = false;  // activity flag: deleting valid format 

	/**
	 * Populate valid Formats List.
	 * Keep track of invalid formats to maintain entire listing.
	 * 
	 * @param p parent component
	 * @param d candidate string
	 * @calledBy ValidateInput
	 */
	public DateFormatDialog(JDialog p, String candidate){
		super(p, "Date Formatter", true);
		sCandidate = candidate;

		ArrayList<SimpleDateFormat> alFormats = Store.getDateFormats();
		alInvalidFormats = new ArrayList<SimpleDateFormat>();
		lmFormats = new DefaultListModel();

		for(SimpleDateFormat f: alFormats){
			try{
				date = f.parse(candidate);
				sDateRep = f.format(date);
				lmFormats.addElement(f);
			}catch(ParseException e){
				alInvalidFormats.add(f);
			}
		}
		pMain = new JPanel();
		buildGUI();
	}

	/**
	 * Construct dialog widgets. Comprised of: 
	 *  - displasy of candidate string and its date interpretation
	 *  - listing of available valid formats
	 *  - edit box
	 */
	private void buildGUI(){
		pMain.removeAll();
		pMain.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		pMain.setLayout(gridBag);
		c.insets = new Insets(3, 3, 3, 3); 
		c.weightx = 0;
		c.weighty = 0;
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;

		//-------Candidate Widgets--------
		JLabel lbCandidate = new JLabel("Candidate String:");
		gridBag.setConstraints(lbCandidate, c);
		lbCandidate.setHorizontalAlignment(SwingConstants.RIGHT);
		pMain.add(lbCandidate);

		JLabel lbOriginalDate = new JLabel(sCandidate);
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(lbOriginalDate, c);
		pMain.add(lbOriginalDate);

		//-------Date Interpretation--------
		JLabel lbInterpretation = new JLabel("Date Interpretation: ");
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.anchor = GridBagConstraints.EAST;
		lbInterpretation.setHorizontalAlignment(SwingConstants.RIGHT);
		gridBag.setConstraints(lbInterpretation, c);
		pMain.add(lbInterpretation);

		lbDateRep = new JLabel(sDateRep);
		lbDateRep.setBackground(Color.WHITE);
		lbDateRep.setPreferredSize(new Dimension(120, 20));
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(lbDateRep, c);
		pMain.add(lbDateRep);

		//-------Valid Format Listing--------
		JLabel lbValidFormats = new JLabel("Valid Formats:");
		lbValidFormats.setForeground(Color.BLUE);
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.HORIZONTAL;
		gridBag.setConstraints(lbValidFormats, c);
		pMain.add(lbValidFormats);

		lFormats = new JList(lmFormats);
		lFormats.setCellRenderer(new ValidFormatsCellRenderer());
		lFormats.addKeyListener(new listKeyListener());
		lFormats.addListSelectionListener(this);
		lFormats.setSelectedIndex(0);
		lFormats.setFont(new Font("Tahoma", Font.PLAIN, 11));

		spFormats = new JScrollPane(lFormats, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spFormats.setPreferredSize(new Dimension(150, 100));
		c.weighty = 1;
		gridBag.setConstraints(spFormats, c);
		pMain.add(spFormats);

		//-------Edit Box--------
		JLabel lbEdit = new JLabel("Edit: ");
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		c.weighty = 0;
		gridBag.setConstraints(lbEdit, c);
		pMain.add(lbEdit);

		tfEdit = new JTextField("");
		tfEdit.setEditable(true);
		tfEdit.addKeyListener(new ValidateEditEntryKeyListener());
		tfEdit.setBackground(Color.WHITE);
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(tfEdit, c);
		pMain.add(tfEdit);

		//-------Dialog buttons--------
		JPanel p = new JPanel();

		bOk = new JButton("Ok");
		bOk.addActionListener(this);
		p.add(bOk);

		bCancel = new JButton("Cancel");
		bCancel.addActionListener(this);
		p.add(bCancel);

		c.weighty = 0;
		c.anchor = GridBagConstraints.EAST;
		c.fill = GridBagConstraints.NONE;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(p, c);
		pMain.add(p);

		//---------------
		add(pMain);
		Rectangle rec = getParent().getBounds();
		pack();
		Rectangle newRec = this.getBounds();
		setLocation(((int)rec.getCenterX()) - (newRec.width / 2),
					((int)rec.getCenterY()) - (newRec.height / 2));
		addWindowListener(new WindowListenerDFD());
		setResizable(false);
		setVisible(true);
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae){
		// store the dateFormats, set the current date format in Store
		if(ae.getSource() == bOk){
			ArrayList<SimpleDateFormat> sdf = alInvalidFormats;
			for(int i = 0; i < lmFormats.size(); i++){
				sdf.add((SimpleDateFormat)lmFormats.get(i));
			}
			Store.setDateFormats(sdf);
			Store.setCurrentDateFormat((SimpleDateFormat)lmFormats.get(lFormats.getSelectedIndex()));
			dispose();
		}
		// Dispose of the dialog without changing the Stored dateFormats.
		if(ae.getSource() == bCancel){
			dispose();
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent arg0){
		// If deleting a format then do nothing.
		if(selectedIndex == lFormats.getSelectedIndex() || isDeleting) return;   
		
		// If selection changed update widgets 
		selectedIndex = lFormats.getSelectedIndex();
		SimpleDateFormat f = (SimpleDateFormat)lFormats.getSelectedValue();
		try{
			date = f.parse(sCandidate);
			sDateRep = f.format(date);
			lbDateRep.setText(f.format(date));
			pMain.revalidate();
		}catch(ParseException e){}
		
	}

/*------------Inner Classes-----------------------*/

	/**
	 * Validate newly created date formats within the edit box
	 * @author: Aaron Ceglar
	 */
	class ValidateEditEntryKeyListener extends KeyAdapter{
		public ValidateEditEntryKeyListener(){
			super();
		}

		/* (non-Javadoc)
		 * Validate the edit entry.
		 * 
		 * @see java.awt.event.KeyAdapter#keyReleased(java.awt.event.KeyEvent)
		 */
		public void keyReleased(KeyEvent event){
			if(event.getKeyCode() != KeyEvent.VK_ENTER)	return;
			
			// validate the entry
			SimpleDateFormat sdf = new SimpleDateFormat();
			try{
				sdf.applyPattern(tfEdit.getText());
				sdf.parse(sCandidate);
			}catch(Exception e){
				System.out.println("invalid date format");
				tfEdit.setText("");
				return;
			}
			
			// if valid add to list of valid formats and select (update widgets).
			lmFormats.addElement(sdf);
			try{
				date = sdf.parse(sCandidate);
				sDateRep = sdf.format(date);
				lbDateRep.setText(sdf.format(date));
				pMain.revalidate();

			}catch(ParseException e){}
			tfEdit.setText("");
		}
	}

	/**
	 * Enable deletion of valid format entries.
	 * @author: Aaron Ceglar
	 */
	class listKeyListener extends KeyAdapter{
		public listKeyListener(){
			super();
		}

		/* (non-Javadoc)
		 * Delete selected dateFormat from within  the Valid Format List.
		 * set isDeleting to avoid cascading trigger 
		 * 
		 * @see java.awt.event.KeyAdapter#keyReleased(java.awt.event.KeyEvent)
		 */
		public void keyReleased(KeyEvent k){
			if(k.getKeyCode() != KeyEvent.VK_DELETE)return;
			isDeleting = true;
			lmFormats.removeElementAt(lFormats.getSelectedIndex());
			pMain.revalidate();
			isDeleting = false;
		}
	}

	/**
	 * Format the valid formats for presentation. 
	 * @author: Aaron Ceglar
	 */
	class ValidFormatsCellRenderer extends JLabel implements ListCellRenderer{
		public ValidFormatsCellRenderer(){
			super();
		}

		/*
		 * format is presented as a label, coloured blue upon selection.
		 * 
		 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList,
		 *      java.lang.Object, int, boolean, boolean)
		 */
		public JComponent getListCellRendererComponent(JList list, Object value, int index, 
						                               boolean isSelected, boolean cellHasFocus){
			JLabel j = new JLabel(((SimpleDateFormat)value).toPattern());
			if(isSelected) j.setForeground(Color.BLUE);
			return j;
		}
	}

	/**
	 * Dispose of dialog upon close
	 * @author: Aaron Ceglar 
	 */
	class WindowListenerDFD extends WindowAdapter{
		public void windowClosing(WindowEvent e){
			dispose();
		}
	}

}
